/*streamer.allowRead('all');
streamer.allowWrite('all');*/

Meteor.publish("getUserList", function () {
	return StudentDetails.find({},{username:0,userId:0})
});

Meteor.publish("getUserGroupList", function () {
	return Chat.find({});
});

Meteor.methods({
	getUserName: function (userId) {
	   //console.log('userId - '+userId)
	   return Meteor.users.find({"_id":userId}).fetch()
	},
	updateGroupList: function (groupData) {
	    Chat.insert(groupData);
	},
	getClientIp:function(){
		return clientIP = this.connection.clientAddress;
	},
	setMessage:function(msgObj){
		Messages.insert(msgObj);
	},
	getUserGroups:function(userId){
		groupList = []
		let groudData = Chat.find({}).fetch()
		
		if(groudData.length > 0){
			groudData.forEach(function(groupObj){
		 		studentArr = groupObj.students
		 		
		 		let isExist = false
		 		studentArr.forEach(function(studentData){
		 			if(studentData.userId == userId){
		 				isExist = true
		 			}
		 		})
		 		if(isExist){
		 			groupList.push(groupObj)
		 		}
	      	})
		}

		return groupList;
	}
})

/*YuukanOO/streamy*/

/**
 * Called upon a client connection, insert the user
 */
Streamy.onConnect(function(socket) {
  Clients.insert({
    'sid': Streamy.id(socket)
  });
});

/**
 * Upon disconnect, clear the client database
 */
Streamy.onDisconnect(function(socket) {
  Clients.remove({
    'sid': Streamy.id(socket)
  });

  // Inform the lobby
  Streamy.broadcast('__leave__', {
    'sid': Streamy.id(socket),
    'room': 'lobby'
  });
});

/**
 * When the nick is set by the client, update the collection accordingly
 */
Streamy.on('nick_set', function(data, from) {
  if(!data.handle)
    throw new Meteor.Error('Empty nick');

  Clients.update({
    'sid': Streamy.id(from)
  }, {
    $set: { 'nick': data.handle }
  });

  // Ack so the user can proceed to the rooms page
  Streamy.emit('nick_ack', { 'nick': data.handle }, from);

  // Inform the lobby
  Streamy.broadcast('__join__', {
    'sid': Streamy.id(from),
    'room': 'lobby'
  });
});

/**
 * Only publish clients with not empty nick
 */
Meteor.publish('clients', function() {
  return Clients.find({
    'nick': { $ne: null }
  });
});

/**
 * Publish rooms where the user appears
 * @param  {String} sid) Client id
 */
Meteor.publish('rooms', function(sid) {
  if(!sid){
  	return null;//this.error(new Meteor.Error('sid null'));
  }
    

  return Streamy.Rooms.allForSession(sid);
});