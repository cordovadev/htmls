/*Get Question Bank*/
Meteor.publish("getQuestionBank", function () {
	/*Get Questions for Only MSQ,TF,FIB,MTF*/
   return QuestionBank.find({ $and : [{$or : [{"type":"MCQ"},{"type":"TF"},{"type":"FIB"},{"type":"MTF"},{"type":"SA"},{"type":"LA"}]}]})
});
/*Get Question Bank*/
Meteor.publish("getQuizQuestionBank", function () {
	/*Get Questions for Only MSQ,TF,FIB,MTF*/
    return QuestionBank.find({ $and : [{$or : [{"type":"MCQ","practice_qn":false},
    											{"type":"TF","practice_qn":false},
    											{"type":"FIB","practice_qn":false},
    											{"type":"MTF","practice_qn":false}
    									]}
    						]})
}); 
/*Check Is there any notification for Quiz*/
Meteor.publish("checkAssessmentNotification", function (studentId,currentDate) {

	//questiondetails = []
	return Notification.find({"type":"quiz",studentId:studentId,date:currentDate,status:true},{limit:1})
    //return questiondetails
});

Meteor.methods({
	getQuizQuestion:function(questionId){
		totalQuestions = []
		questionsObj = Quiz.find({'_id':questionId}).fetch()
		for(var i=0;i<questionsObj[0].question.length;i++){

			eachQuestion = QuestionBank.find({'_id':questionsObj[0].question[i]._id}).fetch()
			totalQuestions.push(eachQuestion[0])
			
		}
		return totalQuestions
	},
	getAssignmentQuestion:function(getAssginmentQuestion){
		totalQuestions1 = []
		questionsObj1 = getAssginmentQuestion;
		console.log(questionsObj1)
		for(var i=0;i<questionsObj1.length;i++){

			eachQuestion1 = QuestionBank.find({'_id':questionsObj1[i]._id}).fetch()
			totalQuestions1.push(eachQuestion1[0])
			
		}
		return totalQuestions1
	},
		getStudentGroup:function(groupId){
		console.log(groupId)
	var	studentgroup1 = StudentGroup.find({'_id':questionsObj1[i]._id}).fetch()[0]
		return studentgroup1;
	},
	updateAssignment:function(answerObj,assignment_id,student_id){
		var StudentDetails1 = StudentDetails.find({userId:student_id}).fetch()[0]
						console.log(StudentDetails1);
		var answer=[]
	//	console.log(answerObj+"\n"+assignment_id)
		if(answer){
			//console.log("CAME 2 update CODE");
			//console.log("question id"+answerObj.question_id);
			
			
			Quiz.update({
     _id:assignment_id,
     "answer.studentId":StudentDetails1._id}, {
     $set: {
       "answer.$.status": "submited",
        "answer.$.question_id":answerObj.question_id,
         "answer.$.type":answerObj.type,
         "answer.$.data":answerObj.data,
         "answer.$.submitedDate":answerObj.submitedDate
     }
   })
			// ({'_id':id},{ $push: {"message.reply":replyArry}})

		}
		return 0;
	},
	getStudentId:function(student_id){
		var StudentDetails1 = StudentDetails.find({userId:student_id}).fetch()[0]
						console.log(StudentDetails1);
		
		return StudentDetails1;
	},
		updateAssignmentStatus:function(assignment_id,student_id,mark,review){
		//console.log(answerObj+"\n"+assignment_id)
		if(assignment_id){
			console.log("CAME 2 update CODE")
			Quiz.update({
     _id:assignment_id,
     "answer.studentId":student_id}, {
     $set: {
       "answer.$.status": "reviewd",
        "answer.$.mark":mark,
         "answer.$.review":review
     }
   })
			// ({'_id':id},{ $push: {"message.reply":replyArry}})

		}
		return 0;
	},
	updateDueDate:function(assignment_id,duedate){
		//console.log(answerObj+"\n"+assignment_id)
		if(assignment_id){
			console.log("CAME 2 update CODE")
			Quiz.update({
     _id:assignment_id}, {
     $set: {
       "duedate":duedate,
       "status" :"conducted"
     }
   })
			// ({'_id':id},{ $push: {"message.reply":replyArry}})

		}
		return 0;
	},

	getQuizTime:function(questionId){
		timeDeatils = {}
		questionsObj = Quiz.find({'_id':questionId}).fetch()
		return timeDeatils = questionsObj[0].time
	}
}) 