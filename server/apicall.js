Meteor.methods({
	callDictionaryApi: function (word) 
	{

		this.unblock();

	    // Construct the API URL
	    var apiUrl = "https://glosbe.com/gapi/translate?from=eng&dest=eng&format=json&phrase=" + word + "&pretty=true";
	    // query the API
	    return Meteor.http.call("GET", apiUrl);

	}
		
})