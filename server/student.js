Meteor.methods({

  registerGroupStudentsDetails: function(studentGroupInformation) {
    StudentGroup.insert(studentGroupInformation);    
    return "result";
  },
  updateGroupStudentsDetails: function(editStudentGroupDetailsId,groupName,obj,leaderId,leaderName) {
   StudentGroup.update(
   	{
   		_id:editStudentGroupDetailsId
   	},
   	{
   		$set:
   		{
   			"groupName": groupName,
        teacherId : this.userId,
   			"students":obj,
   		
   		}
   	});
    return "result";
  },
  getStudentGroupDetails: function() {
    return "result"
  },
   getStudentGroupDetailsToEdit: function(userId) {
    return StudentGroup.find({"_id" : userId}).fetch()
  },
  deletGroup: function(groupId) {
    console.log(groupId)
    StudentGroup.remove({ _id:groupId });
    return "result";
  },
   getStudentTarget: function(studentId) {
    return Target.find({"student_id" : studentId}).fetch()
  }

})

 Meteor.publish("getStudent", function() {
   return StudentDetails.find({teacherId:this.userId});
 });
  Meteor.publish("getStudentData", function() {
   return StudentDetails.find({userId:this.userId});
 });
 Meteor.publish("getStudentGroup", function() {
   return StudentGroup.find({teacherId:this.userId});
 });

 Meteor.publish("usersData", function () {
  if(this.userId){
    
    data = Meteor.users.find({_id:this.userId},{fields:{role:1,name:1,}}).fetch()
    console.log(this.userId)
    return Meteor.users.find({_id:this.userId},{fields:{role:1,endorse_session_curriculum:1,name:1}})
  }
})

/* Students.update({_id:self.studentData._id},{
          $set:{
            username: self.studentData.username,
            marks:{"maths":self.studentData.marks.maths,"english":self.studentData.marks.english,"science":self.studentData.marks.science,"history":self.studentData.marks.history},
            address:{"street":self.studentData.address.street,"city":self.studentData.address.city,"state":self.studentData.address.state,"country":$scope.selectedCountry}
          }
       */