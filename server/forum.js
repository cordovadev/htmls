/*Get Question Bank*/
Meteor.publish("getForumStudent", function (subjects,classId,batch) {
	/*Get forum Student*/
    return Forum.find({
    $and : [ {"user_id":this.userId},
        { $or : [ {"subject_id":{$in:subjects}},{"public":true}]}
          ]
    })
});
Meteor.publish("getForumTeacher", function (subject,classId,batch) {
  /*Get Forum  for Teacher*/
    return Forum.find({class_id:classId,batch_id:batch,subject_id:{$in:subject}})
});
Meteor.publish("getFaq", function () {
	/*Get Questions for Only MSQ,TF,FIB,MTF*/
    return Forum.find({faq:true})
});

// Meteor.publish('', function() {
//    console.log("Came @ Event Subscribe - privileges")
//    var self = this;
//    if (this.userId) {
//      let user = Meteor.users.findOne(this.userId, {
//        fields: {
//          role: 1,
//          endorse_class_batch_subject:1
//        }
//      });
//      let class_id = user.endorse_class_batch_subject.class_id
//      let batch_id = user.endorse_class_batch_subject.batch_id

//      //get privileges for this user
//      let privileges = Meteor.users.aggregate([{
//        $match: {
//          'endorse_class_batch_subject.class_id': class_id,
//          'endorse_class_batch_subject.batch_id': batch_id
//        }
//      }, {
//        $unwind: '$endorse_class_batch_subject'
//      }, {
//        $match: {
//          'endorse_class_batch_subject.class_id': class_id,
//          'endorse_class_batch_subject.batch_id': batch_id
//        }
//      }, {
//        $project: {
//          'endorse_class_batch_subject': 1
//        }
//      }]);

//      _.each(privileges, function(privilege) {
//        self.added('new_privileges', roleId, privilege);
//      });

//      self.ready();
//    }
//  });

// db.getCollection('users').aggregate([{
     //   $match: {
     //     'endorse_class_batch_subject.class_id': "123",
     //     'endorse_class_batch_subject.batch_id': "1234"
     //   }
     // }, {
     //   $unwind: '$endorse_class_batch_subject'
     // }, {
     //   $match: {
     //     'endorse_class_batch_subject.class_id': "123",
     //     'endorse_class_batch_subject.batch_id': "1234"
     //   }
     // },{
     //   $unwind: '$endorse_class_batch_subject.subjects'
     // },{ $match: {
     //     'endorse_class_batch_subject.subjects._id': "maths"}}, {
     //   $project: {
     //     'endorse_class_batch_subject': 1,"subjects":1
     //   }
     // }])


Meteor.methods({
	createQuestion: function (MsgObj) {
	    Forum.insert(MsgObj) 
	},
	updateDiscussion: function (id,MsgObj) {
	    var replyArry = []
	    fetchDiscussionDetails = Forum.find({'_id':id}).fetch()
	    if(fetchDiscussionDetails.length > 0){
	    	console.log("Came at fetchDiscussionDetails "+fetchDiscussionDetails)
	    	messageDetails = fetchDiscussionDetails[0].message
	    	if(messageDetails.reply){
	    		console.log("Came @  reply array")
	    		// replyArry.push(MsgObj)
	    		Forum.update({'_id':id},{ $push: {"message.reply":MsgObj}}) 
	    	}
	    	else{
	    		console.log("Came @ no reply array")
	    		// replyArry.push(MsgObj)
	    		Forum.update({'_id':id},{ $push: {"message.reply":MsgObj}}) 
	    	}
	    }
	},
	faqCorrectAns: function (messageData, index,checkstatus) {
		if(checkstatus == false){
          checkstatus = true
        }
        else{
          checkstatus = false 
        }
        console.log(checkstatus)
		var a= "message.reply."+index
		var s=a+".faqStatus"
		statusObj = {}
		statusObj[s] = checkstatus
		forumUpdate = Forum.update({_id:messageData},{$set:statusObj})
		if(forumUpdate){
			console.log("FAQ STATUS"+forumUpdate)
		}
	},
	forwardToFaq: function (forumMessage,faqStatus) {
		forumUpdate = Forum.update({_id:forumMessage},{$set:{faq:faqStatus}})
		if(forumUpdate){
			console.log("FAQ STATUS"+forumUpdate)
		}
	},
	showPublic: function (forumMessage,publicStatus) {
		forumUpdate = Forum.update({_id:forumMessage},{$set:{public:publicStatus}})
		if(forumUpdate){
			console.log("FAQ STATUS"+forumUpdate)
		}
	}
})