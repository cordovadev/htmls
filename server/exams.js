Meteor.methods({
	updateExamResult: function (marks) {
	    Exams.insert(marks) 
	},
	setNotificationStatus: function (notificationId) {
	    Notification.update(
	   	{
	   		_id:notificationId
	   	},
	   	{
	   		$set:
	   		{
	   			"status": false,
	   		}
	   	});
	}
})