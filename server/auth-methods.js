// Meteor.publish("usersData", function () {
//   if(this.userId){
//     return Meteor.users.find({_id:this.userId},{fields:{role:1}})
//   }
// })
 //Methods for authentication
 Meteor.methods({

   //Create user without password
   //@param email {String}
   createUserWithoutPassword: function(email) {

     userId = Accounts.createUser({
       email: email
     });
     
     //here just updates the user type
     Meteor.users.update(userId, {
       $set: {
         "username": email,
         "role": "student"
       }
     });

     //Changing the email tempalte
     Accounts.emailTemplates.enrollAccount.text = function(user, url) {

       //Changing the url
       let res = url.replace("#/enroll-account", "cordova/enroll-account");

       return "Welcome to edu-core:\n\n" + res;
     };
     //if user is created then send enrollment mail
     if (userId) {
       Accounts.sendEnrollmentEmail(userId);
       return 'Check your email to set your password!';
     }

   },

   //Verifies and sets the new user password
   verifyUserByTokenId: function(token, newPassword, confirmPassword) {

     //basic validations
     if (newPassword === null) {
       throw new Meteor.Error(403, 'New Password field is required');
     }
     if (confirmPassword === null) {
       throw new Meteor.Error(403, 'Confirm Password field is required');
     }
     if (confirmPassword !== newPassword) {
       throw new Meteor.Error(403, "Password doesn't match");
     }

     //find user by user token that we get from the link
     let user = Meteor.users.findOne({
       "services.password.reset.token": token
     });

     if (user === undefined) {
       throw new Meteor.Error(403, "Your verify token is invalid or has expired");
     }

     let email = user.services.password.reset.email;
     let userId = user._id;

     //update the user as verified
     Meteor.users.update({
       _id: user._id,
       'emails.address': email,
       'services.password.reset.token': token
     }, {
       $set: {
         'emails.$.verified': true
       }
     });

     //sets the new password for the user
     let res = Accounts.setPassword(userId, newPassword);

     return true;
   },

   //sends the reset password link to resets the new password
   sendPasswordResetLink: function(email) {

     if (email === "") {
       throw new Meteor.Error(403, 'Enter a valid email address');
     }
     //finds user by email
     let user = Meteor.users.findOne({
       "emails.address": email
     });

     if (user === undefined) {
       throw new Meteor.Error(403, 'User not found for this email');
     }

     let userId = user._id;

     //get host address
     let hostName = this.connection.httpHeaders.host
     //Changing the email tempalte
     
     Accounts.emailTemplates.siteName = "Cordova - Educore";
     Accounts.emailTemplates.from = "Cordova - Educore Admin <info@cordovacloud.com>";

     Accounts.emailTemplates.resetPassword.text = function(user, url) {

       let res = url.replace("localhost:3000", hostName);
       res = res.replace("#/reset-password", "cordova/reset-password");

       message = "Dear " + user.name + "\n\n"
       message += "We received a request to reset your Educore password.Click the link below to reset your password.\n\n"
       message += res + "\n\n"
       message += "If you have trouble clicking the password reset button, then copy and paste the URL below into your web browser:\n\n"
       message += res + "\n\n"
       message += "If you did not request a password reset, kindly ignore this Email.\n\n"
       message += "Sincerely,\n\nEducore support Team."

       return message;
     };

     //if user exists for that user sends the reset password link
     if (userId) {
       Accounts.sendResetPasswordEmail(userId, email);
       return 'Check your email to set your password!';
     }
     return 'Check your email to set your password!';

   },

   //Resets the password by token that will get from the link from the mail
   resetPasswordByToken: function(token, newPassword, confirmPassword) {

     //basic validations
     if (newPassword === null) {
       throw new Meteor.Error(403, 'New Password field is required');
     }
     if (confirmPassword === null) {
       throw new Meteor.Error(403, 'Confirm Password field is required');
     }
     if (confirmPassword !== newPassword) {
       throw new Meteor.Error(403, "Password doesn't match");
     }

     //finds user by token
     let user = Meteor.users.findOne({
       "services.password.reset.token": token
     });

     if (user === undefined) {
       throw new Meteor.Error(403, "Your password reset token is invalid or has expired");
     }

     let userId = user._id;

     //sets the new password
     Accounts.setPassword(userId, newPassword);
     return true;

   },

   //change password by user Id
   changePasswordByUserId: function(currentPassword, newPassword, confirmPassword, passwordObj) {

     //basic validations
     if (!currentPassword) {
       throw new Meteor.Error(403, 'Current Password field is required');
     }

     //get current user id
     let userId = Meteor.userId();

     let user = Meteor.user();

     // Check whether the current password matches the bcrypt'ed password in
     // the database user record.
     let result = Accounts._checkPassword(user, passwordObj);

     if (result.error)
       throw new Meteor.Error(403, 'Invalid Current Password');

     if (!newPassword) {
       throw new Meteor.Error(403, 'New Password field is required');
     }
     if (!confirmPassword) {
       throw new Meteor.Error(403, 'Confirm Password field is required');
     }
     if (confirmPassword !== newPassword) {
       throw new Meteor.Error(403, "Password doesn't match");
     }

     //sets the new password

     let res = Accounts.setPassword(userId, newPassword);

     return true;
   },

   //Authentication - user login
   authenticateUser: function(email, password) {

     check(email, String);
     check(password, String);

     /* Finds the user with the specified email.
     @param {String} email The email address to look for */

     if (!email)
       throw new Meteor.Error(403, 'Enter mandatory fields');

     let user = Accounts.findUserByEmail(email);

     if (!user)
       throw new Meteor.Error(403, 'Email or Password is not correct');

     // Check whether the provided password matches the bcrypt'ed password in
     // the database user record.
     let result = Accounts._checkPassword(user, password);

     if (result.error)
       throw new Meteor.Error(403, 'Email or Password is not correct');

     //creates new token for login
     let stampedLoginToken = Accounts._generateStampedLoginToken();

     //inserts newly created token to current user document
     Accounts._insertLoginToken(user._id, stampedLoginToken);

     res = {};
     res.token = stampedLoginToken.token;
     if (user.role === "teacher") {
       res.goTo = "questionList";
     } else if (user.role == "student") {
       res.goTo = "practiceTest";
     } else {
       res.goTo = "forum";
     }
     return res;
   },

   getEmailTemplate: function() {
     let res = Settings.findOne({
       _id: {
         $ne: ""
       }
     })
     return res;
   }

 });
