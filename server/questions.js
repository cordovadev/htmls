Meteor.methods({


	fetchQuestion: function (questions) {
		totalQuestions = []

		for(var i=0;i<questions.question.length;i++){
			let questionObj = questions.question[i]
			//Level 1
			console.log("Type : "+questionObj.type+" Level 1 - "+questionObj.level1)
			mcqQuestions = QuestionBank.find({type:questionObj.type,"level":1},{"limit":(questionObj.level1+1)}).fetch()
			console.log(mcqQuestions.length)
			shuffledData = shuffle(mcqQuestions)
			mcqLevel1Data =  shuffledData.slice(Math.max(shuffledData.length - questionObj.level1, 1))
			mcqLevel1Data.forEach(function(obj){
				totalQuestions.push(obj)
			})

			//Level 2
			console.log("Type : "+questionObj.type+" Level 2 - "+questionObj.level2)
			mcqQuestions = QuestionBank.find({type:questionObj.type,"level":2},{"limit":(questionObj.level2+1)}).fetch()
			console.log(mcqQuestions.length)
			shuffledData = shuffle(mcqQuestions)
			mcqLevel2Data =  shuffledData.slice(Math.max(shuffledData.length - questionObj.level2, 1))
			mcqLevel2Data.forEach(function(obj){
				totalQuestions.push(obj)
			})

			//Level 3
			console.log("Type : "+questionObj.type+" Level 3 - "+questionObj.level3)
			mcqQuestions = QuestionBank.find({type:questionObj.type,"level":3},{"limit":(questionObj.level3+1)}).fetch()
			console.log(mcqQuestions.length)
			shuffledData = shuffle(mcqQuestions)
			mcqLevel3Data =  shuffledData.slice(Math.max(shuffledData.length - questionObj.level3, 1))
			mcqLevel3Data.forEach(function(obj){
				totalQuestions.push(obj)
			})
		}
		return totalQuestions
	}
})




  function shuffle (array) {
    var m = array.length, t, i;
    while (m) {
      // Pick a remaining element…
      i = Math.floor(Math.random() * m--);
      // And swap it with the current element.
      t = array[m];
      array[m] = array[i];
      array[i] = t;
    }
    return array;
  }
