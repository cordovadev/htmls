Meteor.publish("getIbtPassage", function(subjectId) {
    return IbtPassage.find({
        "subject_id": subjectId
    })
});

Meteor.publish("getIbtQuestions", function(passageId) {
    return IbtQuestion.find({
        "passage_reference": passageId
    })
});

Meteor.methods({
    getibtQuestionsList: function(subjectId) {
        questionList = []
        let passageData = IbtPassage.find({
            "subject_id": subjectId
        }).fetch()

        if (passageData.length > 0) {
            passageData.forEach(function(passageObj) {
                passageRef = passageObj._id
                questionObj = IbtQuestion.find({
                    "passage_reference": passageRef
                }).fetch()
                if (questionObj.length > 0) {
                    questionObj.forEach(function(question) {
                        let customObj = {
                            'passage': passageObj.passage,
                            'passage_images': passageObj.images,
                            'passage_id': passageObj._id,
                            'passage_title': passageObj.title,
                            'question_id': question._id,
                            'question_images': question.images,
                            'question_type': question.type,
                            'question_qn_desc': question.qn_desc,
                            'question_subject_id': question.subject_id,
                            'question_strand': question.strand,
                            'question_mark': question.mark,
                            'question_choice': question.choice
                        }
                        questionList.push(customObj)
                    })
                }

            })
        }

        return questionList;
    }
})
