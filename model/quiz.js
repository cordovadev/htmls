Quiz = new Mongo.Collection('Questions');

Quiz.allow({
  insert: function(userId, doc) {
    return true;
  },
  update: function(userId, doc, fields, modifier) {
    return true;
  },
  remove: function(userId, doc) {
    return true;
  }

});

var Schemas ={};

quizSchemas = new SimpleSchema({
	title: {
        type: String,
        label: 'Title',
        max: 100,
        min:4
    },
    subTitle: {
        type: String,
        label: 'Sub Title',
        max: 100,
        min:4
    },
    description: {
        type: String,
        label: 'Description',
        max: 100,
        min:4
    },
    type: {
        type: String,
        label: 'Type',
        max: 50,
        min:4
    },
    fetch_from: {
        type: String,
        label: 'Fetch from source',
        max: 50,
        min:4
    },
    status: {
        type: String,
        label: 'status',
        max: 50,
        min:4
    },
    date:{
        type: Date,
        label: "date",
        max: 200,
        min :0,
        optional:false,
        blackbox : true
    },
    total_question: {
        type: Number,
        min: 1,
        label: 'Total no of Questions',
         optional :true,
        max: 100
    },
    question: {
        type: [Object],
    	label: 'Marks Details',
    	blackbox: true,
         optional :true
    },
    "question.$.type": {
        type: String,
        optional :true

    },
    "question.$.count": {
        type: Number,
        optional :true
    },
    "question.$.level1": {
        type: Number,
        optional :true
    },
    "question.$.level2": {
        type: Number,
        optional :true
    },
    "question.$.level3": {
        type: Number,
        optional :true
    },
    subject_id: {
        type: Number,
        label: 'Subject ID',
         optional :true
    },
    theme_id: {
        type: Number,
        label: 'Theme ID',
        optional :true
    },
    cross_subjects:{
        type: [String],
        label: 'Cross subjects',
        minCount: 1,
        optional :false
    },
    tags:{
        type: [String],
        label: 'Tag',
        minCount: 1,
        optional :false
    },
    strands:{
        type: [String],
        label: 'Strands',
        minCount: 1,
        optional :false
    },
    themes:{
        type: [String],
        label: 'Themes',
        minCount: 1,
        optional :false
    },
    teacherId:{
          type: String,
        optional :true
    },
    time:{
        type : Object,
        label: 'Time',
        blackbox: false
    },
    "time.hour": {
        type: String,
        min: 0,
        label: 'Hour',
        max: 12
    },
    "time.min": {
        type: String,
        min: 0,
        label: 'Minutes',
        max: 60
    },
    "time.sec": {
        type: String,
        min: 0,
        label: 'Seconds',
        max: 60
    },
    group :{
        type : [Object],
        optional : true,
        blackbox: true
    },
    studentvalue :{
        type : [Object],
        optional : true,
        blackbox: true
    },
    answer :{
        type :[Object],
        optional : true,
        blackbox: true
    },
    duedate:{
        type: Date,
        label: "duedate",
        max: 200,
        min :0,
        optional:true,
        blackbox : true
    }
});

//Quiz.attachSchema(Schemas.Quiz);



dictationSchema = new SimpleSchema({

      teacherId: {
        type: String,
        optional:false
    },
     date: {
        type: String,
        label: "Date",
        max: 200,
        min : 2,
        optional:false
    },
    type: {
        type: String,
        label: "type",
        max: 200,
        min : 2,
        optional:false
    },
    status:{
        type: String,
        label: "Status",
        max: 200,
        min :0,
        optional:true,
        blackbox:true
    },
    dictation_word:{
        type: String,
        label: "Diction",
        max: 200,
        min :0,
        optional:true,
    },
    mark: {
        type:String,
        label: "mark",
        max: 300,
        min :1,
        optional:false
    },
    Duration: {
        type:String,
        label: "Duration",
        optional:true
    },
     file: {
        type:String,
        label: "file",
        optional:true
    },
    subjectId: {
        type:String,
        label: "subjectId",
        optional:false
    },
    theme: {
        type:String,
        label: "theme",
        optional:false
    },
    answer:{
        type : [Object],
        optional : true,
        blackbox: true
    }
 

});
