Customer = new Mongo.Collection("customer");
ResetPassword = new Mongo.Collection('resetPassword');
ForgotPassword = new Mongo.Collection('forgotPassword');
Login = new Mongo.Collection('login');

Meteor.users.allow({
  insert: function(userId, doc) {
    return true;
  },
  update: function(userId, doc, fields, modifier) {
    return true;
  },
  remove: function(userId, doc) {
    return true;
  }
});


let Schemas = {};

Schemas.CustomerSchema = new SimpleSchema({
   phone: {
   		type: Number,
       label:"Phone Number",
       custom:function(){
        if(this.value <= 0){
            return "inValidPhone"
        }else{
          if(this.value){
           let pattern =  /^\+?([0-9]{2})\)?[-. ]?([0-9]{4})[-. ]?([0-9]{4})$/
            if(!pattern.test(this.value)){
              return "inValidPhone"
            }
          }
        }
       }
   },
   _id :{
      type: String
   },
   name:{
      type: String
   },
   username:{
      type: String
   },
   "address.line1": {
       type: String,
       label:"address.line1",
       max:150,
       custom:function(){
        if(this.value){
          let patternC = /^[a-zA-Z0-9-.#,:-\s]*$/ ;
          if(!patternC.test(this.value)){
            return "Invalid"
          }
        }
       }
   },
   "address.line2": {
       type: String,
       label:"address.line2",
       custom:function(){
        if(this.value){
          let patternC = /^[a-zA-Z0-9-.#,:-\s]*$/ ;
          if(!patternC.test(this.value)){
            return "Invalid"
          }
        }
       }
   },
   "address.city": {
       type: String,
       label:"address.city",
       custom:function(){
        if(this.value){
          let patternC = /^[a-zA-Z.\s]*$/
          if(!patternC.test(this.value)){
            return "Invalid"
          }
        }
       }
   },
  "address.state": {
       type: String,
       label:"address.state",
       custom:function(){
        if(this.value){
          let patternC = /^[a-zA-Z.\s]*$/
          if(!patternC.test(this.value)){
            return "Invalid"
          }
        }
       }
   },
   "address.pincode": {
       type: String,
       label:"address.pincode",
   },
   "address.country._id": {
       type: String,
       label:"country._id",
   },
   "address.country.name": {
       type: String,
       optional:true,
       label:"country.name"
   },
   "address.country.iso_code_2": {
       type: String,
       optional:true,
       label:"country.iso_code_2"
   },
   "address.country.iso_code_3": {
       type: String,
       optional:true,
       label:"country.iso_code_3"
   },
   "address.country.display_code": {
       type: String,
       optional:true,
       label:"display_code"
   },
});

Schemas.CustomerSchema.messages({
  required: "[label] is required",
  inValidPhone: "[label] is invalid",
  Invalid:"[label] is invalid",
  minString: "[label] must be at least [min] characters",
  maxString: "[label] cannot exceed [max] characters",
  minNumber: "[label] must be at least [min]",
  maxNumber: "[label] cannot exceed [max]",
  minDate: "[label] must be on or after [min]",
  maxDate: "[label] cannot be after [max]",
  badDate: "[label] is not a valid date",
  minCount: "You must specify at least [minCount] values",
  maxCount: "You cannot specify more than [maxCount] values",
  noDecimal: "[label] must be an integer",
  notAllowed: "[value] is not an allowed value",
  expectedString: "[label] must be a string",
  expectedNumber: "[label] must be a number",
  expectedBoolean: "[label] must be a boolean",
  expectedArray: "[label] must be an array",
  expectedObject: "[label] must be an object",
  expectedConstructor: "[label] must be a [type]",
  passwordMismatch: "These passwords don't match. Try again?",
  regEx: [{
    msg: "[label] failed regular expression validation"
  }, {
    exp: SimpleSchema.RegEx.Email,
    msg: "[label] must be a valid e-mail address"
  }, {
    exp: SimpleSchema.RegEx.WeakEmail,
    msg: "[label] must be a valid e-mail address"
  }, {
    exp: SimpleSchema.RegEx.Domain,
    msg: "[label] must be a valid domain"
  }, {
    exp: SimpleSchema.RegEx.WeakDomain,
    msg: "[label] must be a valid domain"
  }, {
    exp: SimpleSchema.RegEx.IP,
    msg: "[label] must be a valid IPv4 or IPv6 address"
  }, {
    exp: SimpleSchema.RegEx.IPv4,
    msg: "[label] must be a valid IPv4 address"
  }, {
    exp: SimpleSchema.RegEx.IPv6,
    msg: "[label] must be a valid IPv6 address"
  }, {
    exp: SimpleSchema.RegEx.Url,
    msg: "[label] must be a valid URL"
  }, {
    exp: SimpleSchema.RegEx.Id,
    msg: "[label] must be a valid alphanumeric ID"
  }],
  keyNotInSchema: "[key] is not allowed by the schema"
  
})
Customer.attachSchema(Schemas.CustomerSchema);
/*=========================================================================*/
/***********************Login Check**********************************/

let Schemaslog = {};
Schemaslog.LoginChkSchema = new SimpleSchema({
  email: {
    type: String,
    // regEx: SimpleSchema.RegEx.Email,
    label: "Email",
    custom: function() {
      if(this.value !== null){
        let pattern = new RegExp(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/);
        let res = pattern.test(this.value);
        if(!res){
          return "validEmail"
        }
      }
    }
  },
  password: {
    type: String,
    min: 6,
    max: 18,
    label: "Password"
  }
});



Schemaslog.LoginChkSchema.messages({
  validEmail: "Enter valid Email",
  requiredEmail: "dont left email field blank",
  required: "[label] field cannot be blank",
  minString: "[label] must be at least [min] characters",
  maxString: "[label] cannot exceed [max] characters",
  minNumber: "[label] must be at least [min]",
  maxNumber: "[label] cannot exceed [max]",
  minDate: "[label] must be on or after [min]",
  maxDate: "[label] cannot be after [max]",
  badDate: "[label] is not a valid date",
  minCount: "You must specify at least [minCount] values",
  maxCount: "You cannot specify more than [maxCount] values",
  noDecimal: "[label] must be an integer",
  notAllowed: "[value] is not an allowed value",
  expectedString: "[label] must be a string",
  expectedNumber: "[label] must be a number",
  expectedBoolean: "[label] must be a boolean",
  expectedArray: "[label] must be an array",
  expectedObject: "[label] must be an object",
  expectedConstructor: "[label] must be a [type]",
  regEx: [
    {msg: "[label] failed regular expression validation"},
    {exp: SimpleSchema.RegEx.Email, msg: "[label] must be a valid e-mail address"},
    {exp: SimpleSchema.RegEx.WeakEmail, msg: "[label] must be a valid e-mail address"},
    {exp: SimpleSchema.RegEx.Domain, msg: "[label] must be a valid domain"},
    {exp: SimpleSchema.RegEx.WeakDomain, msg: "[label] must be a valid domain"},
    {exp: SimpleSchema.RegEx.IP, msg: "[label] must be a valid IPv4 or IPv6 address"},
    {exp: SimpleSchema.RegEx.IPv4, msg: "[label] must be a valid IPv4 address"},
    {exp: SimpleSchema.RegEx.IPv6, msg: "[label] must be a valid IPv6 address"},
    {exp: SimpleSchema.RegEx.Url, msg: "[label] must be a valid URL"},
    {exp: SimpleSchema.RegEx.Id, msg: "[label] must be a valid alphanumeric ID"}
  ],
  keyNotInSchema: "[key] is not allowed by the schema"
});

Login.attachSchema(Schemaslog.LoginChkSchema);

/***********************Forgot Password**********************************/

let SchemasForgot = {};
SchemasForgot.ForgotPasswordSchema = new SimpleSchema({
  email: {
    type: String,
    // regEx: SimpleSchema.RegEx.Email,
    label: "Email",
    custom: function() {
      if(this.value !== null){
        let pattern = new RegExp(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/);
        let res = pattern.test(this.value);
        if(!res){
          return "validEmail"
        }
      }
    }
  }

});

SchemasForgot.ForgotPasswordSchema.messages({
  validEmail: "Enter valid Email",
  required: "[label] field cannot be blank",
  minString: "[label] must be at least [min] characters",
  maxString: "[label] cannot exceed [max] characters",
  minNumber: "[label] must be at least [min]",
  maxNumber: "[label] cannot exceed [max]",
  minDate: "[label] must be on or after [min]",
  maxDate: "[label] cannot be after [max]",
  badDate: "[label] is not a valid date",
  minCount: "You must specify at least [minCount] values",
  maxCount: "You cannot specify more than [maxCount] values",
  noDecimal: "[label] must be an integer",
  notAllowed: "[value] is not an allowed value",
  expectedString: "[label] must be a string",
  expectedNumber: "[label] must be a number",
  expectedBoolean: "[label] must be a boolean",
  expectedArray: "[label] must be an array",
  expectedObject: "[label] must be an object",
  expectedConstructor: "[label] must be a [type]",
  regEx: [{
    msg: "[label] failed regular expression validation"
  }, {
    exp: SimpleSchema.RegEx.Email,
    msg: "[label] must be a valid e-mail address"
  }, {
    exp: SimpleSchema.RegEx.WeakEmail,
    msg: "[label] must be a valid e-mail address"
  }, {
    exp: SimpleSchema.RegEx.Domain,
    msg: "[label] must be a valid domain"
  }, {
    exp: SimpleSchema.RegEx.WeakDomain,
    msg: "[label] must be a valid domain"
  }, {
    exp: SimpleSchema.RegEx.IP,
    msg: "[label] must be a valid IPv4 or IPv6 address"
  }, {
    exp: SimpleSchema.RegEx.IPv4,
    msg: "[label] must be a valid IPv4 address"
  }, {
    exp: SimpleSchema.RegEx.IPv6,
    msg: "[label] must be a valid IPv6 address"
  }, {
    exp: SimpleSchema.RegEx.Url,
    msg: "[label] must be a valid URL"
  }, {
    exp: SimpleSchema.RegEx.Id,
    msg: "[label] must be a valid alphanumeric ID"
  }],
  keyNotInSchema: "[key] is not allowed by the schema"
});
ForgotPassword.attachSchema(SchemasForgot.ForgotPasswordSchema);

/*************************Reset Password*************************************/


let ResetSchema = {};
ResetSchema.ResetCheckSchema = new SimpleSchema({
  new_password: {
    type: String,
    min: 6,
    max: 12,
    label: "New Password"
  },
  confirm_password: {
    type: String,
    min: 6,
    max: 12,
    label: "Confirm Password",
    custom: function () {
      if (this.value !== this.field('new_password').value) {
        return "passwordMismatch";
      }
    }
  }
});



ResetSchema.ResetCheckSchema.messages({
  required: "[label] is required",
  minString: "[label] must be at least [min] characters",
  maxString: "[label] cannot exceed [max] characters",
  minNumber: "[label] must be at least [min]",
  maxNumber: "[label] cannot exceed [max]",
  minDate: "[label] must be on or after [min]",
  maxDate: "[label] cannot be after [max]",
  badDate: "[label] is not a valid date",
  minCount: "You must specify at least [minCount] values",
  maxCount: "You cannot specify more than [maxCount] values",
  noDecimal: "[label] must be an integer",
  notAllowed: "[value] is not an allowed value",
  expectedString: "[label] must be a string",
  expectedNumber: "[label] must be a number",
  expectedBoolean: "[label] must be a boolean",
  expectedArray: "[label] must be an array",
  expectedObject: "[label] must be an object",
  expectedConstructor: "[label] must be a [type]",
  passwordMismatch: "These passwords don't match. Try again?",
  regEx: [{
    msg: "[label] failed regular expression validation"
  }, {
    exp: SimpleSchema.RegEx.Email,
    msg: "[label] must be a valid e-mail address"
  }, {
    exp: SimpleSchema.RegEx.WeakEmail,
    msg: "[label] must be a valid e-mail address"
  }, {
    exp: SimpleSchema.RegEx.Domain,
    msg: "[label] must be a valid domain"
  }, {
    exp: SimpleSchema.RegEx.WeakDomain,
    msg: "[label] must be a valid domain"
  }, {
    exp: SimpleSchema.RegEx.IP,
    msg: "[label] must be a valid IPv4 or IPv6 address"
  }, {
    exp: SimpleSchema.RegEx.IPv4,
    msg: "[label] must be a valid IPv4 address"
  }, {
    exp: SimpleSchema.RegEx.IPv6,
    msg: "[label] must be a valid IPv6 address"
  }, {
    exp: SimpleSchema.RegEx.Url,
    msg: "[label] must be a valid URL"
  }, {
    exp: SimpleSchema.RegEx.Id,
    msg: "[label] must be a valid alphanumeric ID"
  }],
  keyNotInSchema: "[key] is not allowed by the schema"
});
ResetPassword.attachSchema(ResetSchema.ResetCheckSchema);

/*********************************************************************/

/*=========================================================================*/

