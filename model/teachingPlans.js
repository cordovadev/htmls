TeachingPlans = new Mongo.Collection("teachingplans");
TeachingPlans.allow({
  insert: function(userId, doc) {
    return true;
  },
  update: function(userId, doc, fields, modifier) {
    return true;
  },
  remove: function(userId, doc) {
    return true;
  }

});

var Schemas ={};

Schemas.TeachingPlans = new SimpleSchema({
	title: {
        type: String,
        label: 'Title',
        max: 100,
        min:4
    },
    status:{
        type: String,
        label: 'status',
        max: 50,
        min:4
    },
    teacherId:{
          type: String,
        optional :true
    },
    teaching_plan: {
        type: [Object],
    	label: 'Teaching Plans',
         optional:true,

    },
    "teaching_plan.$._id": {
        type: String,
        optional :true

    },
    "teaching_plan.$.status": {
        type: String,
        optional :true

    },
    "teaching_plan.$.title": {
        type: String,
         optional:false,
         label:"teaching plan title"
       
    },
    // "teaching_plan.time": {
    //     type : Object,
    //     label: 'Time',
    //      optional:false
    // },
    subject_id: {
        type: Number,
        label: 'Subject ID',
         optional :true
    },
    "teaching_plan.$.time.hour": {
        type: Number,
        min: 0,
        label: 'Hour',
        max: 12,
          optional:false
       
    },
    "teaching_plan.$.time.min": {
        type: Number,
        min: 0,
        label: 'Minutes',
        max: 60,
          optional:false
        
    },
    "teaching_plan.$.time.sec": {
        type: Number,
        min: 0,
        label: 'Seconds',
        max: 60,
          optional:false
       
    },
    topic :{
        type : [String],
         optional:false
    },
    objective :{
        type : [String],
        optional:false
      
        
    },
    resources :{
        type : [String],
         optional:false
        
    }
});
TeachingPlans.attachSchema(Schemas.TeachingPlans);