angular.module('cordova_lms').directive('createTeachingPlan', function() {
    return {
        restrict: 'E',
        templateUrl: 'client/createTeachingPlan/create-teaching-plan.html',
        controllerAs: 'teachingPlanCtrl',

        controller: function($scope, $reactive, $state, $filter, $mdToast, $timeout, $interval, $meteor, $interval, $mdDialog, $stateParams) {
            $reactive(this).attach($scope);
            let self = this

            self.teachingPlanTopics = [];
            self.teachingPlanObjectices = [];
            self.teachingPlanResources = [];
            $scope.teachingPlansData = [];
            $scope.teachingPlanArr = []


            $scope.hours = []
            for (var hour = 0; hour <= 12; hour++) {
                hour = hour > 9 ? hour : "0" + hour;
                $scope.hours.push(hour);
            }

            $scope.minutes = []
            for (var min = 0; min < 60; min++) {
                min = min > 9 ? min : "0" + min;
                $scope.minutes.push(min);
            }

            $scope.seconds = []
            for (var sec = 0; sec < 60; sec++) {
                sec = sec > 9 ? sec : "0" + sec;
                $scope.seconds.push(sec);
            }
            $scope.teachingPlans = [{
                "title": "",
                "time": {
                    hour: $scope.hours,
                    min: $scope.minutes,
                    sec: $scope.seconds
                },
                "status": "created"
            }]
            $scope.saveTeachingPlan = function() {

                angular.copy($scope.teachingPlans, $scope.teachingPlanArr);
                console.log($scope.teachingPlanArr);
                console.log(self.teachingPlanTopics)
                let teachingPlansParams = {
                    "teacherId": Meteor.userId(),
                    "status": "created",
                    "title": $scope.teachingPlanTitle,
                    "topic": self.teachingPlanTopics,
                    "objective": self.teachingPlanObjectices,
                    "teaching_plan": $scope.teachingPlanArr,
                    "subject_id": 123,
                    "resources": self.teachingPlanResources
                }
                console.log(teachingPlansParams)
                var teachingPlansDetails = TeachingPlans.simpleSchema().clean(teachingPlansParams);
                TeachingPlans.simpleSchema().namedContext("TeachingPlans").validate(teachingPlansDetails, {
                    modifier: false
                });
                var context = TeachingPlans.simpleSchema().namedContext("TeachingPlans")
                let errorsList = context.invalidKeys();
                let ret_val = context.invalidKeys();
                ret_val = _.map(ret_val, function(res) {
                    return _.extend({
                        message: context.keyErrorMessage(res.name)
                    }, res);
                });
                console.log(ret_val)
                $scope.errors = {}
                if (ret_val.length > 0) {
                    for (key in ret_val) {
                        $scope['errors'][ret_val[key].name] = ret_val[key].message
                    }
                } else {
                    // Meteor.call('addTeachingPlan', teachingPlansParams, (error, result) => {
                    //     if (error) {
                    //         console.log(error.invalidKeys);
                    //     } else {
                    //         mcqQuestions = result
                    //         $mdDialog.cancel();
                    //         $state.go('myPlanerTeachingPlan')
                    //     }
                    // })
                }

            }
            $scope.backToTeachingPlan = function() {


                $state.go('myPlanerTeachingPlan');

            }

            $scope.addnewTeachingPlan = function() {

                $scope.teachingPlanArr1 = []
                angular.copy($scope.teachingPlans, $scope.teachingPlanArr1);
                lastTeachingPlan = $scope.teachingPlanArr1.length - 1;


                $scope.teachingPlans.push($scope.teachingPlanArr1[lastTeachingPlan])

                console.log($scope.teachingPlans);
                //$state.go('myPlanerTeachingPlan');

            }
            $scope.removeTeachingPlan = function(index) {

                $scope.teachingPlans.splice(index, 1);

            }
        }
    }
})
