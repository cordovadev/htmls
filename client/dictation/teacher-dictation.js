angular.module('cordova_lms').directive('teacherDictation', function() {
    return {
        restrict: 'E',
        templateUrl: 'client/dictation/teacherdictation.html',
        controllerAs: 'directiveCtrl',
        controller: function($scope, $mdToast, $reactive, $window, $stateParams, $state, $http, QuestionsFactory) {
            $reactive(this).attach($scope)
            let self = this
            $scope.DicationId = "";
            $scope.groupData = []
            $scope.textMeaning = []
            $scope.meaning = []
            $scope.orginalMeaning = []
            $scope.active = true;
            $scope.active1 = true;
            $scope.checkstatus = false;
            self.saveButtonVisible = true;
            self.updateButtonVisible = false;
            $scope.studentDictation = []
            $scope.timeDictation = 0;

            self.getAllDictation = function() {
                self.subscribe('getQuizDictation', () => {
                    return [] // parameters to be passed to subscribe method
                }, () => {
                    this.helpers({
                        studentDictation: () => {
                            return Quiz.find({
                                "teacherId": Meteor.userId(),
                                "type": "Dictation"
                            })
                        }
                    });
                })
            }

            self.getAllDictation()

            $scope.playSound = function(word) {
                if (word === "" || word === null || word === undefined) {
                    $scope.showToast("Please Enter the word")
                } else {

                    var u = new SpeechSynthesisUtterance();
                    u.text = word;
                    speechSynthesis.speak(u);
                }
            }
            var contains = function(needle) {
                // Per spec, the way to identify NaN is that it is not equal to itself
                var findNaN = needle !== needle;
                var indexOf;

                if (!findNaN && typeof Array.prototype.indexOf === 'function') {
                    indexOf = Array.prototype.indexOf;
                } else {
                    indexOf = function(needle) {
                        var i = -1,
                            index = -1;

                        for (i = 0; i < this.length; i++) {
                            var item = this[i];

                            if ((findNaN && item !== item) || item === needle) {
                                index = i;
                                break;
                            }
                        }

                        return index;
                    };
                }

                return indexOf.call(this, needle) > -1;
            };
            $scope.getDictationDetails = function(groupId, checkstatus) {
                if (checkstatus === false) {
                    index = contains.call($scope.groupData, groupId._id);
                    if (index == false) {
                        $scope.groupData.push(groupId._id);
                    }
                } else {
                    for (var i = 0; i < $scope.groupData.length; i++) {
                        if ($scope.groupData[i] == groupId._id) {
                            $scope.groupData.splice(i, 1);
                        }
                    }
                }

            }
            $scope.distributeDictation = function(obj) {
                console.log($scope.groupData)
                if ($scope.groupData.length <= 0) {
                    $scope.showToast("Please select atleast one question")
                } else {
                    QuestionsFactory.addDictation("Diction");
                    QuestionsFactory.addArray($scope.groupData);
                    $state.go("groupList");
                }
            }

            $scope.conductDitaction = function(obj) {

                $scope.groupData.push(obj)
                console.log($scope.groupData)
                QuestionsFactory.addDictation("Diction");
                QuestionsFactory.addArray($scope.groupData);
                $state.go("groupList");

            }

            $scope.EditDictation = function(id) {
                self.saveButtonVisible = false;
                self.updateButtonVisible = true;
                $scope.DicationId = id;
                //Quiz.find({"teacherId":Meteor.userId(),"type":"Dictation"})
                var studentDictationtoEdit = Quiz.find({
                    "_id": id
                }).fetch();
                $scope.Search = studentDictationtoEdit[0].dictation_word
                $scope.mark = studentDictationtoEdit[0].mark
                $scope.timeDictation = studentDictationtoEdit[0].Duration
            }

            $scope.updateDictation = function(word, mark, time) {

                if (word === "" || word === null || word === undefined) {
                    $scope.showToast("Please Enter the word")
                } else {

                    var d = new Date();


                    Meteor.call('updateDictation', d.toDateString(), word, mark, time, $scope.DicationId, (error, result) => {
                        if (error) {
                            $scope.showToast("Dictation Not saved")
                        } else {
                            self.saveButtonVisible = true;
                            self.updateButtonVisible = false;
                            $scope.showToast("Dictation saved")
                            $scope.clearData();
                        }
                    })
                }
            }
            $scope.clearData = function() {
                $scope.Search = ""
                $scope.mark = ""
                $scope.timeDictation = ""
            }
            $scope.viewDitaction = function(id) {

            }
            $scope.DeleteDictation = function(id) {
                Meteor.call('deletDiction', id, (error, result) => {
                    if (error) {
                        $scope.showToast("Dictation Not Deleted")
                    } else {

                        $scope.showToast("Dictation deleted")
                        $scope.clearData();
                    }
                })
            }
            $scope.saveDictation = function(word, mark, time) {

                if (word === "" || word === null || word === undefined) {
                    $scope.showToast("Please Enter the word")
                } else if (isNaN(mark)) {
                  $scope.showToast("Mark must be a Integer value")                   
                   
                }
                else if (time<=0) {
                  $scope.showToast("please set Time")                   
                   
                }  else {

                     var d = new Date();
                    let DictationInformation = {
                        "teacherId": Meteor.userId(),
                        "date": d.toDateString(),
                        "type": "Dictation",
                        "status": "Not conducted",
                        "dictation_word": word,
                        "mark": mark,
                        "Duration": time,
                        "file": "",
                        "subjectId": 5464,
                        "theme": 87877
                    }
                    Quiz.attachSchema(dictationSchema, {
                        replace: true
                    })
                    Quiz.simpleSchema().namedContext("dictationSchema").validate(DictationInformation, {
                        modifier: false
                    });
                    var context = Quiz.simpleSchema().namedContext("dictationSchema")
                    $scope.errors = {}
                    $scope.errorsList = context.invalidKeys()
                        .filter(function(data) {
                            if (data.type != "keyNotInSchema") {
                                return true
                            }
                        })
                        .map(function(data) {
                            return {
                                message: {
                                    name: data.name,
                                    msg: context.keyErrorMessage(data.name)
                                }
                            }
                        });
                    if ($scope.errorsList.length != 0) {
                        angular.forEach($scope.errorsList, function(error, key) {
                            if (error.message.name.toString().indexOf(".") > -1) {
                                $scope.errors[error.message.name.split(".")[1]] = error.message['msg']
                            } else {
                                $scope.errors[error.message['name']] = error.message['msg']
                            }
                        });
                    }

                    Meteor.call('addQuiz', DictationInformation, (error, result) => {
                        if (error) {
                            $scope.showToast("Dictation Not saved")
                        } else {

                            $scope.showToast("Dictation saved")
                            $scope.clearData();
                        }
                    })
                }
            }

            function data(obj) {
                $scope.$apply()
                $scope.meaning = obj
            }

            var last = {
                bottom: false,
                top: true,
                left: false,
                right: true
            };
            $scope.toastPosition = angular.extend({}, last);
            $scope.getToastPosition = function() {
                sanitizePosition();
                return Object.keys($scope.toastPosition)
                    .filter(function(pos) {
                        return $scope.toastPosition[pos];
                    })
                    .join(' ');
            };

            function sanitizePosition() {
                var current = $scope.toastPosition;
                if (current.bottom && last.top) current.top = false;
                if (current.top && last.bottom) current.bottom = false;
                if (current.right && last.left) current.left = false;
                if (current.left && last.right) current.right = false;
                last = angular.extend({}, current);
            }

            $scope.showToast = function(data) {
                $mdToast.show(
                    $mdToast.simple()
                    .textContent(data)
                    .position($scope.getToastPosition())
                    .hideDelay(3000)
                );
            };
            $scope.collapseAll = function(data) {


                data.expanded = !data.expanded;
            };

        }
    }
});