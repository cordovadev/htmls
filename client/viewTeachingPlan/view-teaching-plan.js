angular.module('cordova_lms').directive('viewTeachingPlan', function() {
  return {
    restrict: 'E',
    templateUrl: 'client/viewTeachingPlan/view-teaching-plan.html',
    controllerAs: 'teachingPlanCtrl',

     controller: function ($scope, $reactive,$state,$filter,$mdToast, $timeout,$interval,$meteor,$interval,$mdDialog, $stateParams){
      $reactive(this).attach($scope);
      let self = this
           $scope.teachingPlan_id=$stateParams.getTeachingPlan;
         $scope.teachingPlan_=[];
      console.log($scope.teachingPlan)
      self.subscribe('getTeachingPlans',
        () => {
          return [] // parameters to be passed to subscribe method
        },
        () => {
          $scope.teachingPlans = TeachingPlans.find({"teacherId": Meteor.userId(),"_id":$scope.teachingPlan_id}).fetch();
          console.log($scope.teachingPlans) 
          $scope.teachingPlan=$scope.teachingPlans[0]
      console.log($scope.teachingPlanData)
        }
      )

      
        $scope.backToTeachingPlan=function(){


$state.go('myPlanerTeachingPlan');

    }


    }
  }
})


