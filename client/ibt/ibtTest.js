angular.module('cordova_lms').directive('ibtTest', function(MyResource) {
    return {
        restrict: 'E',
        templateUrl: 'client/ibt/ibtTest.html',
        controllerAs: 'ibtTest',
        controller: function($scope, $reactive, $state, $mdToast, $filter, $timeout, $interval, $meteor, $interval, $stateParams) {
            $reactive(this).attach($scope);
            let self = this
            self.dynamicTheme = "default"
            self.user = {}

            $scope.indexToShow = 0;
            $scope.fibIndexToShow = 0
            $scope.score = 0;
            $scope.selectedAns = [];
            $scope.actionBtnName = "Submit";
            $scope.totalQuestion = [];
            $scope.isOptionSelected = false;

            $scope.answerStatus = "normal"
            $scope.selectedCheckAns = [];
            $scope.timeData = "";
            $scope.isPracticeTest = true /*$stateParams.isPracticeTest;*/
            $scope.attendedQuesObj = []
            $scope.totalQuestiontoShow = 0

            $scope.elapsedTime = 0
            $scope.remainingTime = ''
            $scope.isSubmitted = false
            $scope.quizQuestionId = ''

            $scope.quizTotalMark = 0
            $scope.resultStatus = ''



            $scope.selectedFIBAns = []
            $scope.panesA = []
                //$scope.modalDetails = {}

            let today = new Date()
            let currentDay = new Date(today.getFullYear(), today.getMonth(), today.getDate())
            let studentId = Meteor.userId()




            var startTime = 0,
                currentTime = null,
                offset = 0,
                interval = null,
                // self = this;
                options = {}

            if (!options.interval) {
                options.interval = 100;
            }

            options.elapsedTime = new Date(0);

            $scope.running = false;

            $scope.updateTime = function() {
                currentTime = new Date().getTime();
                var timeElapsed = offset + (currentTime - startTime);
                options.elapsedTime.setTime(timeElapsed);
            };

            $scope.config = {
                autoHideScrollbar: true,
                theme: 'minimal'
            }

            $scope.startTimer = function() {
                if ($scope.running === false) {
                    startTime = new Date().getTime();
                    interval = $interval($scope.updateTime, options.interval);
                    $scope.running = true;
                }
            };

            $scope.stopTimer = function() {
                if ($scope.running === false) {
                    return;
                }
                $scope.updateTime();
                offset = offset + currentTime - startTime;
                // pushToLog(currentTime - startTime);
                // $interval.cancel(interval);  
                $scope.running = false;
                $scope.timeData = currentTime - startTime
                console.log($scope.timeData)
            };

            // $scope.startTimer();

            //timer with timeout
            $scope.timerWithTimeout = 0;
            $scope.startTimerWithTimeout = function() {
                $scope.timerWithTimeout = 0;
                if ($scope.myTimeout) {
                    $timeout.cancel($scope.myTimeout);
                }
                $scope.onTimeout = function() {
                    $scope.timerWithTimeout++;
                    $scope.myTimeout = $timeout($scope.onTimeout, 1000);
                }
                $scope.myTimeout = $timeout($scope.onTimeout, 1000);
            };
            $scope.startTimerWithTimeout();
            $scope.resetTimerWithTimeout = function() {
                $scope.timerWithTimeout = 0;
                $timeout.cancel($scope.myTimeout);
            }

            //timer with interval
            $scope.timerWithInterval = 0;
            $scope.startTimerWithInterval = function() {
                $scope.timerWithInterval = 0;
                if ($scope.myInterval) {
                    $interval.cancel($scope.myInterval);
                }
                $scope.onInterval = function() {
                    $scope.timerWithInterval++;
                }
                $scope.myInterval = $interval($scope.onInterval, 1000);
            };

            $scope.resetTimerWithInterval = function() {
                $scope.timerWithInterval = 0;
                $interval.cancel($scope.myInterval);
            }
            $scope.getQuestionList = function() {
                    Meteor.call('getibtQuestionsList', 'amma123', (error, result) => {
                        if (error) {
                            console.log("Error:" + error);
                        } else {
                            console.log('passages List : ' + result)
                            $scope.questions = result;
                            $scope.totalQuestiontoShow = $scope.questions.length
                            for (var i = 0; i < result.length; i++) {
                                $scope.totalQuestion.push({
                                    count: i + 1,
                                    isActive: false,
                                    status: 'normal',
                                    label: 'Mark for review'
                                });
                                $scope.panesA.push({
                                    id: 'pane-' + i + 'a',
                                    header: 'View Passage',
                                    content: result[i].passage,
                                    images: result[i].passage_images,
                                    isExpanded: false
                                });
                                if (result[i].question_type == 'FIB') {
                                    let totalFibQuestion = result[i].question_choice.length
                                    $scope.quizTotalMark += (result[i].question_mark * totalFibQuestion)
                                    for (var j = 0; j < totalFibQuestion; j++) {
                                        $scope.attendedQuesObj[i+'.'+j] = {
                                            question_id: result[i].question_id,
                                            question_index: i+'.'+j,
                                            mark: 0,
                                            time_elapsed: 0
                                        }
                                    }
                                } else {
                                    $scope.quizTotalMark += result[i].question_mark
                                    $scope.attendedQuesObj[i] = {
                                        question_id: result[i].question_id,
                                        mark: 0,
                                        time_elapsed: 0
                                    }
                                }
                                
                            }
                            console.log('ibtTotalMark' + $scope.quizTotalMark)
                            $scope.startTimer();
                        }
                    })
                }
                /*self.subscribe('getIbtPassage',
                    () => {
                        return ['amma123'] // parameters to be passed to subscribe method 
                    },
                    () => {
                            $scope.passages = IbtPassage.find({}).fetch();
                            console.log('passages List : '+$scope.passages)
                            for (var i=$scope.passages.length;i>0;i--) {
                                $scope.totalQuestion.push({
                                    count: ($scope.passages.length+1)-i,
                                    isActive: false,
                                    passageId:$scope.passages[i-1]._id,
                                    status: 'normal',
                                    label: 'Submit'
                                });
                            }
                            let passageCount = $scope.totalQuestion.length
                            if(passageCount>0){
                                self.subscribe('getIbtQuestions',
                                    () => {
                                        return [$scope.totalQuestion[0].passageId] // parameters to be passed to subscribe method
                                    },
                                    () => {
                                        $scope.questions = IbtQuestion.find({}).fetch();
                                        console.log('Questions List : '+$scope.questions)
                                    }
                                )
                            }
                            
                    }
                )*/

            /*$scope.panesA = [
                {
                  id: 'pane-1a',
                  header: 'Pane 1',
                  content: 'Curabitur et ligula. Ut molestie a, ultricies porta urna. Vestibulum commodo volutpat a, convallis ac, laoreet enim. Phasellus fermentum in, dolor. Pellentesque facilisis. Nulla imperdiet sit amet magna. Vestibulum dapibus, mauris nec malesuada fames ac turpis velit, rhoncus eu, luctus et interdum adipiscing wisi.',
                  isExpanded: false
                }
              ];*/


            $scope.expandCallback = function(index, id) {
                console.log('expand:', index, id);
            };

            $scope.collapseCallback = function(index, id) {
                console.log('collapse:', index, id);
            };

            $scope.$on('accordionA:onReady', function() {
                console.log('accordionA is ready!');
            });

            let marks = {
                'studentId': Meteor.userId(),
                'type': 'PracticeTest',
                'date': currentDay,
                'result': []
            }
            let scoreDetails = []


            $scope.setSelectedAns = function(option, quesType, index) {
                $scope.isOptionSelected = true;
                if (quesType == "MCQ") {
                    $scope.selectedDesc = option
                    $scope.selectedAns[$scope.indexToShow] = option;
                } else if (quesType == "FIB") {
                    $scope.selectedFIBAns[$scope.indexToShow + '.' + index] = {
                        'index': index,
                        'option': option
                    }
                }
            };



            $scope.changeQuestion = function(questCount) {
                if (questCount != $scope.indexToShow) {
                    //Change button name for next question
                    if ($scope.isPracticeTest) {
                        $scope.resetValues();
                    } else {
                        //if($scope.totalQuestion[$scope.indexToShow].label != 'Unmark'){
                        $scope.evaluateQuizQuestion($scope.indexToShow);
                        //}
                        //$scope.evaluateQuizQuestion($scope.indexToShow);
                    }
                    $scope.indexToShow = questCount;
                    //$scope.splitMatchQuestion();
                    if (!$scope.isPracticeTest) {
                        $scope.startTimer()
                    }
                }
            };



            $scope.resetValues = function() {
                //Change button name for next question
                $scope.actionBtnName = "Submit";
                //Empty the answer type and explanation
                $scope.answerType = "";
                $scope.ansExplanation = "";
                $scope.resultStatus = ''
                $scope.selectedDesc = ''

            };

            $scope.shuffle = function(array) {
                var m = array.length,
                    t, i;
                while (m) {
                    // Pick a remaining element…
                    i = Math.floor(Math.random() * m--);
                    // And swap it with the current element.
                    t = array[m];
                    array[m] = array[i];
                    array[i] = t;
                }
                return array;
            }

            /* Start Quiz*/
            $scope.loadPrevQues = function() {
                //if($scope.totalQuestion[$scope.indexToShow].label != 'Unmark'){
                $scope.evaluateQuizQuestion($scope.indexToShow);
                //}
                $scope.indexToShow = ($scope.indexToShow - 1) % $scope.totalQuestiontoShow
                $scope.isOptionSelected = false;
                //$scope.splitMatchQuestion();
                $scope.startTimer()
            }

            $scope.loadNextQues = function() {
                if (($scope.indexToShow + 1) != $scope.totalQuestiontoShow) {

                    //if($scope.totalQuestion[$scope.indexToShow].label != 'Unmark'){
                    $scope.evaluateQuizQuestion($scope.indexToShow);
                    //}

                    $scope.indexToShow = ($scope.indexToShow + 1) % $scope.totalQuestiontoShow
                    $scope.isOptionSelected = false;
                    //$scope.splitMatchQuestion();
                } else {
                    //$scope.totalQuestion[$scope.indexToShow].label = "Submit"
                }
                $scope.startTimer()
            }
            $scope.reviewQuestion = function() {

                if ($scope.totalQuestion[$scope.indexToShow].label == "Mark for review") {
                    $scope.totalQuestion[$scope.indexToShow].status = 'review'
                    $scope.totalQuestion[$scope.indexToShow].label = 'Unmark'
                } else {
                    $scope.totalQuestion[$scope.indexToShow].status = 'normal'
                    $scope.totalQuestion[$scope.indexToShow].label = 'Mark for review'
                }
            }
            $scope.checkQuizChoiceExists = function(index) {
                return $scope.attendedQuesObj.indexOf(index) > -1;
            };
            $scope.initiateTimer=function(index){
                $scope.startTimer();
                $scope.fibIndexToShow = index
            }
            $scope.evaluateFIB = function(index){
                let isCorrectAns = false;
                let questionMark = 0
                let questObj = $scope.questions[$scope.indexToShow];
                let fibAnswerCount = questObj.question_choice.length;
                let wrongAnsCount = 0;
                
                questionValue = questObj.question_choice[index]
                for (var i = 0; i < questionValue.answer.length; i++) {

                    if ($scope.selectedFIBAns[$scope.indexToShow + '.' + index] == undefined || $scope.selectedFIBAns[$scope.indexToShow + '.' + index].option == "") {
                        wrongAnsCount += 1;
                        //$scope.stopTimer();
                        //console.log($scope.timeData)
                        //Case : User doesnt't write
                    } else {
                        if (questionValue.answer[i].desc == $scope.selectedFIBAns[$scope.indexToShow + '.' + index].option) {
                            //Case :  Correct Answer
                            //$scope.stopTimer();
                            //console.log($scope.timeData)
                            isCorrectAns = true;
                            questionMark = questObj.question_mark
                        } else {
                            //Case :  Wrong Answer
                            wrongAnsCount += 1;
                        }
                    }
                }
                $scope.stopTimer();
                if ($scope.totalQuestion[$scope.indexToShow].label != 'Unmark') {
                    $scope.totalQuestion[$scope.indexToShow].status = 'submit';
                }
                $scope.timeData = $scope.attendedQuesObj[$scope.indexToShow+'.'+index].time_elapsed + $scope.timeData
                $scope.attendedQuesObj[$scope.indexToShow+'.'+index] = {
                    question_id: questObj.question_id,
                    mark: questionMark,
                    question_index: $scope.indexToShow+'.'+index,
                    time_elapsed: $scope.timeData
                } 

            }
            $scope.evaluateQuizQuestion = function(index) {
                let isCorrectAns = false;
                let questionMark = 0
                let questObj = $scope.questions[$scope.indexToShow];
                let currentIndex = 0

                /*START - MCQ*/
                if (questObj.question_type == "MCQ") {
                    angular.forEach(questObj.question_choice, function(value, key) {
                        if (value.correct && $scope.selectedAns[$scope.indexToShow] == value.desc) {
                            //$scope.stopTimer();
                            //console.log($scope.timeData)
                            isCorrectAns = true;
                            questionMark = questObj.question_mark
                        }
                    });
                    /*$scope.stopTimer();
                    console.log($scope.timeData)*/
                }
                /*END - MCQ*/

                /*START - FIB*/
                else if (questObj.question_type == "FIB") {
                    var fibAnswerCount = questObj.question_choice.length;
                    var wrongAnsCount = 0;


                    for (var j = 0; j < questObj.question_choice.length; j++) {
                        questionValue = questObj.question_choice[j]
                        for (var i = 0; i < questionValue.answer.length; i++) {

                            if ($scope.selectedFIBAns[$scope.indexToShow + '.' + j] == undefined || $scope.selectedFIBAns[$scope.indexToShow + '.' + j].option == "") {
                                wrongAnsCount += 1;
                                currentIndex = j
                                //$scope.stopTimer();
                                //console.log($scope.timeData)
                                //Case : User doesnt't write
                            } else {
                                if (questionValue.answer[i].desc == $scope.selectedFIBAns[$scope.indexToShow + '.' + j].option) {
                                    //Case :  Correct Answer
                                    //$scope.stopTimer();
                                    //console.log($scope.timeData)
                                    isCorrectAns = true;
                                    currentIndex = j
                                    questionMark = questObj.question_mark
                                } else {
                                    //Case :  Wrong Answer
                                    wrongAnsCount += 1;
                                    currentIndex = j
                                }
                            }

                        }

                    }

                    /*if (wrongAnsCount == 0) {
                            isCorrectAns = true;
                            questionMark += questObj.question_mark
                    }*/
                    /* $scope.stopTimer();
                     console.log($scope.timeData)*/
                }
                /*END - FIB*/
                $scope.stopTimer();
                //console.log('Current Time : ' + $scope.timeData)
                if ($scope.totalQuestion[$scope.indexToShow].label != 'Unmark') {
                    $scope.totalQuestion[$scope.indexToShow].status = 'submit';
                }
                
                    //console.log('Updated Time : '+$scope.timeData)
                if (questObj.question_type == "MCQ"){
                   $scope.timeData = $scope.attendedQuesObj[$scope.indexToShow].time_elapsed + $scope.timeData
                   $scope.attendedQuesObj[$scope.indexToShow] = {
                        question_id: questObj.question_id,
                        mark: questionMark,
                        time_elapsed: $scope.timeData
                    } 
                }else{
                    $scope.timeData = $scope.attendedQuesObj[$scope.indexToShow+'.'+$scope.fibIndexToShow].time_elapsed + $scope.timeData
                    $scope.attendedQuesObj[$scope.indexToShow+'.'+$scope.fibIndexToShow] = {
                        question_id: questObj.question_id,
                        mark: questionMark,
                        question_index: $scope.indexToShow+'.'+$scope.fibIndexToShow,
                        time_elapsed: $scope.timeData
                    } 
                }
                
            }


            let ibtMarks = {
                'studentId': Meteor.userId(),
                'type': 'IBT',
                'date': currentDay,
                'score':0,
                /*'timeElapsed': $scope.elapsedTime,*/
                'result': []
            }
            $scope.submitQuizQuestion = function() {
                $scope.isSubmitted = true
                clearTimer()
                    //if($scope.totalQuestion[$scope.indexToShow].label != 'Unmark'){
                $scope.evaluateQuizQuestion($scope.indexToShow);
                //}

                let finalScore = 0
               /* for (var i = 0; i < $scope.attendedQuesObj.length; i++) {
                    if ($scope.attendedQuesObj[i] != undefined) {
                        finalScore += $scope.attendedQuesObj[i].mark
                        ibtMarks.result.push({
                            questionId: $scope.attendedQuesObj[i].questionId,
                            mark: $scope.attendedQuesObj[i].mark,
                            timeElapsed: $scope.attendedQuesObj[i].timeElapsed
                        })
                    }
                }*/
                //ibtMarks.result = $scope.attendedQuesObj
                for(var i in $scope.attendedQuesObj){
                    finalScore += $scope.attendedQuesObj[i].mark
                        /*ibtMarks.result.push({
                            questionId: $scope.attendedQuesObj[i].questionId,
                            mark: $scope.attendedQuesObj[i].mark,
                            timeElapsed: $scope.attendedQuesObj[i].timeElapsed
                        })*/
                         ibtMarks.result.push($scope.attendedQuesObj[i])
                }
                ibtMarks.score = finalScore
                //ibtMarks.timeElapsed = $scope.elapsedTime
                    
                Meteor.call('updateExamResult', ibtMarks, (error, result) => {
                     if (error) {
                         console.log("Error:" + error);
                     } else {
                         alert(' Score ' + finalScore + ' / ' + $scope.quizTotalMark)
                     }
                 })
                    
                console.log(' Score ' + finalScore + ' / ' + $scope.quizTotalMark)
                $scope.showToast(' Score ' + finalScore + ' / ' + $scope.quizTotalMark)
            }

            //
            $scope.getTimeRemaining = function(endTime) {
                    let t = Date.parse(endTime) - Date.parse(new Date());
                    let seconds = Math.floor((t / 1000) % 60);
                    let minutes = Math.floor((t / 1000 / 60) % 60);
                    let hours = Math.floor((t / (1000 * 60 * 60)) % 24);
                    let days = Math.floor(t / (1000 * 60 * 60 * 24));
                    return {
                        'total': t,
                        'days': days,
                        'hours': hours,
                        'minutes': minutes,
                        'seconds': seconds
                    }
                }
                // 
            let timeinterval
            $scope.initializeClock = function(endTime) {

                function updateClock() {
                    let t = $scope.getTimeRemaining(endTime);
                    let days = t.days > 0 ? t.days + ' : ' : ''

                    $scope.remainingTime = days + ('0' + t.hours).slice(-2) + " : " + ('0' + t.minutes).slice(-2) + " : " + ('0' + t.seconds).slice(-2)

                    if (t.total <= 0) {
                        clearInterval(timeinterval);
                        //Auto submission
                        if (!$scope.isSubmitted) {
                            $scope.submitQuizQuestion()
                        }
                    }
                }

                updateClock();
                timeinterval = setInterval(updateClock, 1000);
            }
            var last = {
                bottom: false,
                top: true,
                left: false,
                right: true
            };
            $scope.toastPosition = angular.extend({}, last);
            $scope.getToastPosition = function() {
                sanitizePosition();
                return Object.keys($scope.toastPosition)
                    .filter(function(pos) {
                        return $scope.toastPosition[pos];
                    })
                    .join(' ');
            };

            function sanitizePosition() {
                var current = $scope.toastPosition;
                if (current.bottom && last.top) current.top = false;
                if (current.top && last.bottom) current.bottom = false;
                if (current.right && last.left) current.left = false;
                if (current.left && last.right) current.right = false;
                last = angular.extend({}, current);
            }

            $scope.showToast = function(data) {
                $mdToast.show(
                    $mdToast.simple()
                    .textContent(data)
                    .position($scope.getToastPosition())
                    .hideDelay(3000)
                );
            };

            function clearTimer() {
                clearInterval(timeinterval);
                $scope.remainingTime = '00 : 00 : 00'
            }

            /* End Quiz*/
        }
    }
})


angular.module('cordova_lms').filter('hhmmss', function() {
    return function(time) {
        var sec_num = parseInt(time, 10); // don't forget the second param
        var hours = Math.floor(sec_num / 3600);
        var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
        var seconds = sec_num - (hours * 3600) - (minutes * 60);

        if (hours < 10) {
            hours = "0" + hours;
        }
        if (minutes < 10) {
            minutes = "0" + minutes;
        }
        if (seconds < 10) {
            seconds = "0" + seconds;
        }
        var time = hours + ':' + minutes + ':' + seconds;
        return time;
    }
});
angular.module('cordova_lms').filter('character', function() {
    return function(input) {
        return String.fromCharCode(64 + parseInt(input, 10));
    };
});
