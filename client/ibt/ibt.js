angular.module('cordova_lms').directive('ibtDirective', function () {
  return {
    restrict: 'E',
    templateUrl: 'client/ibt/ibt.html',
    controllerAs: 'ibtCtrl',
    controller: function ($scope, $reactive) {


      $scope.panesA = [
        {
          id: 'pane-1a',
          header: 'Pane 1',
          content: 'Curabitur et ligula. Ut molestie a, ultricies porta urna. Vestibulum commodo volutpat a, convallis ac, laoreet enim. Phasellus fermentum in, dolor. Pellentesque facilisis. Nulla imperdiet sit amet magna. Vestibulum dapibus, mauris nec malesuada fames ac turpis velit, rhoncus eu, luctus et interdum adipiscing wisi.',
          isExpanded: false
        }
      ];


      $scope.expandCallback = function (index, id) {
        console.log('expand:', index, id);
      };

      $scope.collapseCallback = function (index, id) {
        console.log('collapse:', index, id);
      };

      $scope.$on('accordionA:onReady', function () {
        console.log('accordionA is ready!');
      });

   

    }
  }
});