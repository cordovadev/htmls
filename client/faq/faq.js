angular.module('cordova_lms').directive('faqComponent', function () {
  return {
    restrict: 'E',
    templateUrl: 'client/faq/faq.html',
    controllerAs: 'faqCtrl',
    controller: function ($scope,$mdToast, $reactive,$window,$stateParams,$state) 
    {
      $reactive(this).attach($scope);
      let self = this
      self.getForumQuestions = function(centerArr) {
        self.subscribe('getFaq',
              () => {
                return [] // parameters to be passed to subscribe method
              },
              () => {
                      self.helpers({
                        getForum: () => {
                          return Forum.find()
                        }
                      })
                      $scope.messageDetails = self.getForum
                    }
        )
      }
      self.getForumQuestions()

      $scope.collapseAll = function(data) {
         for(var i in $scope.messageDetails) {
           if($scope.messageDetails[i] != data) {
             $scope.messageDetails[i].expanded = false;   
           }
          }
          data.expanded = !data.expanded;
      };
    }
  }
});