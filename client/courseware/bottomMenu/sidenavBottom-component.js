angular.module('cordova_lms').directive('sideNavBottom', function() {
  return {
    restrict: 'E',
    templateUrl: 'client/courseware/bottomMenu/sidenavBottom.html',
    controllerAs: 'sidenavBottomCtrl',
    controller: function($scope, $mdDialog, $log, $rootScope, $reactive, PageTitle, $state, $timeout) {





  var self = this;

      self.hidden = true;
      self.hover = true;


 self.topDirections = ['left', 'up'];
      self.bottomDirections = ['down', 'right'];
      self.isOpen = false;
      self.availableModes = ['md-fling', 'md-scale'];
      self.selectedMode = 'md-fling';
      self.availableDirections = ['up', 'down', 'left', 'right'];
      self.selectedDirection = 'up';



      // On opening, add a delayed property which shows tooltips after the speed dial has opened
      // so that they have the proper position; if closing, immediately hide the tooltips
      $scope.$watch('sidenavBottomCtrl.isOpen', function(isOpen) {
        if (isOpen) {
          $timeout(function() {
            $scope.tooltipVisible = self.isOpen;
          }, 600);
        } else {
          $scope.tooltipVisible = self.isOpen;
        }
      });

      self.items = [
        { name: "Revision papers", icon: "autorenew", direction: "left" },
        { name: "Worksheets", icon: "assignment_turned_in", direction: "left" },
        { name: "Practise Test", icon: "assignment", direction: "left" }
      ];

      self.openDialog = function($event, item) {
        // Show the dialog
        $mdDialog.show({
          clickOutsideToClose: true,
          controller: function($mdDialog) {
            // Save the clicked item
            this.item = item;

            // Setup some handlers
            this.close = function() {
              $mdDialog.cancel();
            };
            this.submit = function() {
              $mdDialog.hide();
            };
          },
          controllerAs: 'dialog',
          templateUrl: 'dialog.html',
          targetEvent: $event
        });
      }





    
    }
  }
});
