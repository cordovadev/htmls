angular.module('cordova_lms').directive('sideNavRight', function(MyResource) {
  return {
    restrict: 'E',
    templateUrl: 'client/courseware/rightMenu/sidenavRight.html',
    controllerAs: 'sidenavRightCtrl',
    controller: function($scope, $mdDialog, $log, $rootScope, $reactive, PageTitle, $state, $timeout, $mdSidenav) {



    $scope.toggleRight = buildToggler('right');
    $scope.isOpenRight = function(){
      return $mdSidenav('right').isOpen();
    };


 function buildToggler(navID) {
      return function() {
        $mdSidenav(navID)
          .toggle()
          .then(function () {
            $log.debug("toggle " + navID + " is done");
          });
      }
    }


    $scope.close = function () {
      $mdSidenav('right').close()
        .then(function () {
          $log.debug("close RIGHT is done");
        });
    };



 $('#carouselv').jsCarousel({ onthumbnailclick: function(src) { alert(src); }, autoscroll: false, masked: false, itemstodisplay: 3, orientation: 'v' });



    
   






    
    }
  }
});
