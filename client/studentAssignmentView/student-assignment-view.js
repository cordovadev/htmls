angular.module('cordova_lms').directive('studentAssignmentView', function(MyResource) {
    return {
        restrict: 'E',
        templateUrl: 'client/studentAssignmentView/student-assignment-view.html',
        controllerAs: 'practiceTest',
        controller: function($scope, $reactive, $state, $filter, $timeout, $interval, $meteor, $interval, $stateParams) {
            $reactive(this).attach($scope);
            $scope.getAssginmentQuestion = $stateParams.getAssginmentQuestion;
            $scope.getAssginment_id = $stateParams.assignment_id;
            $scope.getAssginmentQuestion_id = $stateParams.question_id;
            console.log($scope.getAssginment_id);
            let self = this
            self.dynamicTheme = "default"
            self.user = {}
            $scope.lockLeft = true;
            $scope.indexToShow = 0;
            $scope.score = 0;
            $scope.selectedAns = [];
            $scope.actionBtnName = "Submit";
            $scope.totalQuestion = [];
            $scope.isOptionSelected = false;
            $scope.ansFib = [];
            $scope.errorFib = {};
            $scope.errorMtf = {};
            $scope.errorMrQues = {};
            $scope.errorMcqQues = {};
            $scope.errorTFQues = {};
            $scope.quesData = [];
            $scope.ansData = [];
            $scope.answerStatus = "normal"
            $scope.selectedCheckAns = [];
            $scope.timeData = "";
            $scope.isPracticeTest = true /*$stateParams.isPracticeTest;*/
            $scope.attendedQuesObj = []
            $scope.totalQuestiontoShow = 0
            $scope.mtfAnsData = [];
            $scope.multipleCheckAnsData = [];
            $scope.noOfAttempts = 0;
            $scope.elapsedTime = 0
            $scope.remainingTime = ''
            $scope.isSubmitted = false
            $scope.quizQuestionId = ''
            $scope.quizNotificationId = ''
            $scope.quizTotalMark = 0
            $scope.resultStatus = ''
            $scope.ansExplantionMTF = ''
            $scope.ansExplantionMR = ''
            $scope.ansExplantionFIB = ''
                //$scope.modalDetails = {}

            let today = new Date()
            let currentDay = new Date(today.getFullYear(), today.getMonth(), today.getDate())
            let studentId = Meteor.userId()




            var startTime = 0,
                currentTime = null,
                offset = 0,
                interval = null,
                // self = this;
                options = {}

            if (!options.interval) {
                options.interval = 100;
            }

            options.elapsedTime = new Date(0);

            $scope.running = false;

            $scope.updateTime = function() {
                currentTime = new Date().getTime();
                var timeElapsed = offset + (currentTime - startTime);
                options.elapsedTime.setTime(timeElapsed);
            };

            $scope.config = {
                autoHideScrollbar: true,
                theme: 'minimal'
            }

            $scope.startTimer = function() {
                if ($scope.running === false) {
                    startTime = new Date().getTime();
                    interval = $interval($scope.updateTime, options.interval);
                    $scope.running = true;
                }
            };

            $scope.stopTimer = function() {
                if ($scope.running === false) {
                    return;
                }
                $scope.updateTime();
                offset = offset + currentTime - startTime;
                // pushToLog(currentTime - startTime);
                // $interval.cancel(interval);  
                $scope.running = false;
                $scope.timeData = currentTime - startTime
                console.log($scope.timeData)
            };

            // $scope.startTimer();

            //timer with timeout
            $scope.timerWithTimeout = 0;
            $scope.startTimerWithTimeout = function() {
                $scope.timerWithTimeout = 0;
                if ($scope.myTimeout) {
                    $timeout.cancel($scope.myTimeout);
                }
                $scope.onTimeout = function() {
                    $scope.timerWithTimeout++;
                    $scope.myTimeout = $timeout($scope.onTimeout, 1000);
                }
                $scope.myTimeout = $timeout($scope.onTimeout, 1000);
            };
            $scope.startTimerWithTimeout();
            $scope.resetTimerWithTimeout = function() {
                $scope.timerWithTimeout = 0;
                $timeout.cancel($scope.myTimeout);
            }

            //timer with interval
            $scope.timerWithInterval = 0;
            $scope.startTimerWithInterval = function() {
                $scope.timerWithInterval = 0;
                if ($scope.myInterval) {
                    $interval.cancel($scope.myInterval);
                }
                $scope.onInterval = function() {
                    $scope.timerWithInterval++;
                }
                $scope.myInterval = $interval($scope.onInterval, 1000);
            };

            $scope.resetTimerWithInterval = function() {
                $scope.timerWithInterval = 0;
                $interval.cancel($scope.myInterval);
            }

            let arryData = {};
            console.log($scope)
            $scope.uploadMultiformativeFile = (file) => {
                console.log(file)
                console.log($scope)
                var data = new FormData();
                for (var i in $scope.files) {
                    data.append("file", $scope.files[i]);

                }
                console.log(data)
                console.log($scope.files)

                // ADD LISTENERS.
                var objXhr = new XMLHttpRequest();
                objXhr.addEventListener("progress", updateaddresProgress, false);
                objXhr.addEventListener("load", transferaddresComplete, false);

                // SEND FILE DETAILS TO THE API.
                objXhr.open("POST", "/pdfUpload");
                objXhr.send(data);
            }

            // UPDATE PROGRESS BAR.
            function updateaddresProgress(e) {

                $scope.loading = false
                if (e.lengthComputable) {
                    document.getElementById('pro').setAttribute('value', e.loaded);
                    document.getElementById('pro').setAttribute('max', e.total);
                }
            }

            // CONFIRMATION.
            function transferaddresComplete(res) {
                //console.log(self.themeIdData)

                $scope.assmtUrlData = res.currentTarget.response;
                console.log($scope.assmtUrlData)
                $scope.pdfFileUpload = true;

                // Meteor.call("formativeAssessment", $scope.assmtUrlData,self.obj,self.formativeAssessment, function(error, result) {
                //   if (error) {
                //     console.log(error)
                //     ToastService.getToastbox($mdToast, 'Something Went Wrong', "error")

                //   } else {
                //     $scope.loading = false;
                //     // console.log('okay')
                //     ToastService.getToastbox($mdToast, 'FormativeAssessment Successfully Saved', "success")
                //   }

                // })

                //console.log(res.currentTarget.response)
                console.log("Files uploaded successfully");

            }

            var _validFileExtensions = [".pdf"];
            $scope.getLsnPlnDetails = function(row) {
                console.log(row)
                if (row.type == "file") {
                    var pdfFilename = row.value;
                    if (pdfFilename.length > 0) {
                        var blnValid = false;
                        for (var j = 0; j < _validFileExtensions.length; j++) {
                            var sCurExtension = _validFileExtensions[j];
                            if (pdfFilename.substr(pdfFilename.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                                blnValid = true;
                                break;
                            }
                        }
                        if (!blnValid) {
                            ToastService.getToastbox($mdToast, 'Sorry! ' + pdfFilename.split('\\')[2] + " is invalid, allowed extensions are: " + _validFileExtensions.join(", "), "error")
                                //alert("Sorry, " + sFileName + " is invalid, allowed extensions are: " + _validFileExtensions.join(", "));
                            row.value = "";
                            return false;
                        }
                    }
                }
                var files = row.files;
                $scope.lsnPlnPdfName = files[0].name;
                //console.log($scope.lsnPlnPdfName)
                $scope.files = [];
                $scope.$apply(function() {
                    // STORE THE FILE OBJECT IN AN ARRAY.
                    for (var i = 0; i < row.files.length; i++) {
                        $scope.files.push(row.files[i])
                    }
                });
            };


            $scope.splitMatchQuestion = function() {
                //Match the Following
                if ($scope.questions.length > 0 && $scope.questions[$scope.indexToShow].type == "MTF") {
                    let isDataExist = false
                        //if($scope.isPracticeTest){
                    $scope.quesData = [];
                    $scope.ansData = [];
                    //}

                    if ($scope.mtfAnsData[$scope.indexToShow] != undefined) {
                        isDataExist = true
                    }

                    angular.forEach($scope.questions[$scope.indexToShow].option, function(value, key) {
                        $scope.quesData.push({
                            tag: value.question.tag,
                            desc: value.question.desc
                        });
                        //$scope.ansData.push({ tag: value.answer.tag, desc: value.answer.desc});
                        if (!isDataExist) {
                            $scope.ansData.push({
                                tag: value.answer.tag,
                                desc: value.answer.desc
                            });
                        }
                    });
                    if (!isDataExist) {
                        $scope.shuffle($scope.ansData)
                        $scope.mtfAnsData[$scope.indexToShow] = $scope.ansData
                    }
                }
            };

            self.subscribe('checkAssessmentNotification', () => {
                return [studentId, currentDay] // parameters to be passed to subscribe method 
            }, () => {
                let moduleDetails = Notification.find({}).fetch();
                console.log(moduleDetails.length)
                if (moduleDetails.length <= 0) {


                    Meteor.call('getAssignmentQuestion', $scope.getAssginmentQuestion, (error, result) => {
                        if (error) {
                            console.log("Error:" + error);
                        } else {
                            //alert('')
                            $scope.questions = result
                            $scope.totalQuestiontoShow = $scope.questions.length
                            console.log('questions ' + result)
                            for (var i = 0; i < $scope.questions.length; i++) {
                                $scope.totalQuestion.push({
                                    count: i + 1,
                                    isActive: false,
                                    status: 'normal',
                                    label: 'Mark for review'
                                });
                                if ($scope.questions[i].type == 'MTF') {
                                    $scope.quizTotalMark += $scope.questions[i].mark.total
                                } else {
                                    $scope.quizTotalMark += $scope.questions[i].mark
                                }
                                $scope.attendedQuesObj[i] = {
                                    questionId: $scope.questions[i]._id,
                                    mark: 0,
                                    timeElapsed: 0
                                }
                            }
                            console.log('quizTotalMark' + $scope.quizTotalMark)
                                //If First Question is MTF
                            $scope.splitMatchQuestion();

                            let deadline = new Date(Date.parse(new Date()) + $scope.elapsedTime);
                            $scope.initializeClock(deadline);

                            $scope.startTimer();
                        }
                    })
                } else {

                    self.subscribe('getQuestionBank', () => {
                        return [] // parameters to be passed to subscribe method
                    }, () => {
                        $scope.questions = QuestionBank.find({}, {
                            limit: 50
                        }).fetch();
                        for (var i = 0; i < $scope.questions.length; i++) {
                            $scope.totalQuestion.push({
                                count: i + 1,
                                isActive: false,
                                status: 'normal',
                                label: 'Submit'
                            });
                        }
                        //If First Question is MTF
                        $scope.splitMatchQuestion();

                        $scope.startTimer();
                    })
                }
            })

            let marks = {
                'studentId': Meteor.userId(),
                'type': 'PracticeTest',
                'date': currentDay,
                'result': []
            }
            let scoreDetails = []
            $scope.submitQuestion = function() {

                if ($scope.actionBtnName == "Submit" || $scope.actionBtnName == "Try Again") {

                    $scope.noOfAttempts += 1
                    var isCorrectAns = false;
                    var questObj = $scope.questions[$scope.indexToShow];
                    console.log(questObj.type)
                    if (questObj.type == "MCQ") {
                        if (questObj.pattern == "single") {
                            $scope.errorMcqQues = {}
                            angular.forEach(questObj.choice, function(value, key) {
                                if (value.correct && $scope.selectedAns[$scope.indexToShow] == value.desc) {
                                    $scope.stopTimer();
                                    console.log($scope.timeData)

                                    if (scoreDetails.length > 0 && scoreDetails[$scope.indexToShow] != undefined) {

                                    } else {
                                        scoreDetails[$scope.indexToShow] = {
                                            mark: questObj.mark
                                        }
                                        $scope.score += questObj.mark;
                                    }

                                    isCorrectAns = true;
                                    $scope.answerType = "Correct Answer";
                                    $scope.ansExplanation = value.explanation;
                                    $scope.answerStatus = "correct"
                                    $scope.errorMcqQues[value.desc] = {
                                        answerType: 'Correct Answer',
                                        answerStatus: 'correct',
                                        msg: value.explanation
                                    };
                                    marks.result[$scope.indexToShow] = {
                                        questionId: questObj._id,
                                        noOfAttempts: $scope.noOfAttempts,
                                        timeElapsed: $scope.timeData
                                    }

                                } else if ($scope.selectedAns[$scope.indexToShow] == value.desc) {
                                    $scope.answerType = "Wrong Answer";
                                    $scope.ansExplanation = value.hint;
                                    $scope.answerStatus = "wrong"
                                    $scope.errorMcqQues[value.desc] = {
                                        answerType: 'Wrong Answer',
                                        answerStatus: 'wrong',
                                        msg: value.hint
                                    };
                                }
                            });
                        } else {
                            let wrongAnsCount = 0;
                            $scope.ansExplantionMR = ''
                            angular.forEach(questObj.choice, function(value, key) {

                                if (value.correct) {
                                    if ($scope.checkChoiceExists(value.desc)) {
                                        $scope.errorMrQues[value.desc] = {
                                            answerType: 'Correct Answer',
                                            answerStatus: 'correct',
                                            msg: (key + 1) + ' . ' + value.explanation
                                        };
                                        $scope.ansExplantionMR = key == 0 ? $scope.ansExplantionMR = (key + 1) + ' . ' + value.explanation : $scope.ansExplantionMR += ' , ' + (key + 1) + ' . ' + value.explanation
                                    } else {
                                        wrongAnsCount += 1;
                                        //$scope.errorMrq[value.desc] = {answerType: 'Wrong Answer', answerStatus: 'wrong', msg:(key+1)+' . '+value.hint};
                                    }
                                    $scope.stopTimer();
                                    console.log($scope.timeData)
                                } else if ($scope.checkChoiceExists(value.desc)) {
                                    wrongAnsCount += 1;
                                    $scope.errorMrQues[value.desc] = {
                                        answerType: 'Wrong Answer',
                                        answerStatus: 'wrong',
                                        msg: (key + 1) + ' . ' + value.hint
                                    };
                                    $scope.ansExplantionMR = key == 0 ? $scope.ansExplantionMR = (key + 1) + ' . ' + value.hint : $scope.ansExplantionMR += ' , ' + (key + 1) + ' . ' + value.hint
                                }

                            });
                            if (wrongAnsCount == 0) {
                                //$scope.score+=questObj.mark;
                                if (scoreDetails.length > 0 && scoreDetails[$scope.indexToShow] != undefined) {

                                } else {
                                    scoreDetails[$scope.indexToShow] = {
                                        mark: questObj.mark
                                    }
                                    $scope.score += questObj.mark;
                                }
                                marks.result[$scope.indexToShow] = {
                                    questionId: questObj._id,
                                    noOfAttempts: $scope.noOfAttempts,
                                    timeElapsed: $scope.timeData
                                }
                                isCorrectAns = true;
                            }
                        }
                    } else if (questObj.type == "TF") {
                        var keepGoing = false;
                        $scope.errorTFQues = {}
                        angular.forEach(questObj.choice, function(value, key) {
                            if (!keepGoing) {
                                if (value.correct && $scope.selectedAns[$scope.indexToShow] == value.tag) {
                                    $scope.stopTimer();
                                    console.log($scope.timeData)
                                        //$scope.score+=questObj.mark;
                                    if (scoreDetails.length > 0 && scoreDetails[$scope.indexToShow] != undefined) {

                                    } else {
                                        scoreDetails[$scope.indexToShow] = {
                                            mark: questObj.mark
                                        }
                                        $scope.score += questObj.mark;
                                    }
                                    marks.result[$scope.indexToShow] = {
                                            questionId: questObj._id,
                                            noOfAttempts: $scope.noOfAttempts,
                                            timeElapsed: $scope.timeData
                                        }
                                        //marks.result.push(markObj)
                                    isCorrectAns = true;
                                    keepGoing = true;
                                    $scope.answerType = "Correct Answer";
                                    $scope.ansExplanation = value.explanation;
                                    $scope.answerStatus = "correct"
                                    $scope.errorTFQues[$scope.selectedAns[$scope.indexToShow]] = {
                                        answerType: 'Correct Answer',
                                        answerStatus: 'correct',
                                        msg: value.explanation
                                    };

                                } else {
                                    keepGoing = false;
                                    $scope.answerType = "Wrong Answer";
                                    $scope.ansExplanation = value.hint;
                                    $scope.answerStatus = "wrong"
                                    $scope.errorTFQues[$scope.selectedAns[$scope.indexToShow]] = {
                                        answerType: 'Wrong Answer',
                                        answerStatus: 'wrong',
                                        msg: value.hint
                                    };
                                }
                            }

                        });
                    } else if (questObj.type == "FIB") {
                        var fibAnswerCount = questObj.answer.length;
                        var wrongAnsCount = 0;
                        $scope.ansExplantionFIB = ''

                        angular.forEach(questObj.answer, function(value, key) {
                            //if($scope.ansFib>0){
                            if ($scope.ansFib[$scope.indexToShow].choice[value.desc] == "" || $scope.ansFib[$scope.indexToShow].choice[value.desc] == undefined) {
                                wrongAnsCount += 1;
                                $scope.stopTimer();
                                console.log($scope.timeData)
                                $scope.errorFib[value.desc] = {
                                    answerType: 'Cant be empty',
                                    answerStatus: 'info',
                                    msg: (key + 1) + ' . ' + value.hint
                                };
                                $scope.ansExplantionFIB = key == 0 ? $scope.ansExplantionFIB = (key + 1) + ' . ' + value.hint : $scope.ansExplantionFIB += ' , ' + (key + 1) + ' . ' + value.hint
                            } else {
                                if (value.desc.toLowerCase() == $scope.ansFib[$scope.indexToShow].choice[value.desc].toLowerCase()) {
                                    $scope.errorFib[value.desc] = {
                                        answerType: 'Correct Answer',
                                        answerStatus: 'correct',
                                        msg: (key + 1) + ' . ' + value.explanation
                                    };
                                    $scope.ansExplantionFIB = key == 0 ? $scope.ansExplantionFIB = (key + 1) + ' . ' + value.explanation : $scope.ansExplantionFIB += ' , ' + (key + 1) + ' . ' + value.explanation
                                } else {
                                    $scope.stopTimer();
                                    $scope.errorFib[value.desc] = {
                                        answerType: 'Wrong Answer',
                                        answerStatus: 'wrong',
                                        msg: (key + 1) + ' . ' + value.hint
                                    };
                                    $scope.ansExplantionFIB = key == 0 ? $scope.ansExplantionFIB = (key + 1) + ' . ' + value.hint : $scope.ansExplantionFIB += ' , ' + (key + 1) + ' . ' + value.hint
                                    wrongAnsCount += 1;
                                }
                            }
                            /*}else{
                                wrongAnsCount+=1;
                                $scope.stopTimer();
                                console.log($scope.timeData)
                                $scope.errorFib[value.desc] = {answerType: 'Cant be empty', answerStatus: 'info', msg:(key+1)+' . '+value.hint};
                            }*/


                        });

                        if (wrongAnsCount == 0) {
                            $scope.stopTimer();
                            //$scope.score+=questObj.mark;
                            if (scoreDetails.length > 0 && scoreDetails[$scope.indexToShow] != undefined) {

                            } else {
                                scoreDetails[$scope.indexToShow] = {
                                    mark: questObj.mark
                                }
                                $scope.score += questObj.mark;
                            }
                            marks.result[$scope.indexToShow] = {
                                questionId: questObj._id,
                                noOfAttempts: $scope.noOfAttempts,
                                timeElapsed: $scope.timeData
                            }
                            isCorrectAns = true;
                        }
                    } else if (questObj.type == "MTF") {
                        $scope.ansExplantionMTF = ''
                        let wrongAnsCount = 0
                        for (var i = 0; i < $scope.quesData.length; i++) {
                            var optionsObj = questObj.option[i];
                            if ($scope.quesData[i].tag == optionsObj.question.tag && $scope.mtfAnsData[$scope.indexToShow][i].desc == optionsObj.answer.desc) {
                                $scope.errorMtf[$scope.quesData[i].tag] = {
                                    answerType: 'Correct Answer',
                                    answerStatus: 'correct',
                                    msg: (i + 1) + ' . ' + optionsObj.explanation
                                };

                                $scope.ansExplantionMTF = i == 0 ? $scope.ansExplantionMTF = (i + 1) + ' . ' + optionsObj.explanation : $scope.ansExplantionMTF += ' , ' + (i + 1) + ' . ' + optionsObj.explanation
                            } else {
                                $scope.errorMtf[$scope.quesData[i].tag] = {
                                    answerType: 'Wrong Answer',
                                    answerStatus: 'wrong',
                                    msg: (i + 1) + ' . ' + optionsObj.explanation
                                };
                                wrongAnsCount += 1;
                                $scope.ansExplantionMTF = i == 0 ? $scope.ansExplantionMTF = (i + 1) + ' . ' + optionsObj.explanation : $scope.ansExplantionMTF += ' , ' + (i + 1) + ' . ' + optionsObj.explanation
                            }
                        }

                        if (wrongAnsCount == 0) {
                            $scope.stopTimer();
                            //$scope.score+=questObj.mark.total;
                            if (scoreDetails.length > 0 && scoreDetails[$scope.indexToShow] != undefined) {

                            } else {
                                scoreDetails[$scope.indexToShow] = {
                                    mark: questObj.mark
                                }
                                $scope.score += questObj.mark.total;
                            }
                            isCorrectAns = true;
                            marks.result[$scope.indexToShow] = {
                                questionId: questObj._id,
                                noOfAttempts: $scope.noOfAttempts,
                                timeElapsed: $scope.timeData
                            }
                        }

                    } else if (questObj.type == "SA") {
                        $scope.ansExplantionMTF = ''

                        for (var i = 0; i < $scope.quesData.length; i++) {

                        }


                    }

                    if (isCorrectAns) {

                        $scope.resultStatus = 'Explanation'
                        $scope.totalQuestion[$scope.indexToShow].isActive = true;
                        $scope.totalQuestion[$scope.indexToShow].status = 'submit';
                        if (($scope.indexToShow + 1) != $scope.questions.length) {
                            $scope.actionBtnName = "Next";
                        } else {
                            Meteor.call('updateExamResult', marks, (error, result) => {
                                if (error) {
                                    console.log("Error:" + error);
                                } else {
                                    alert('Success')
                                    $scope.actionBtnName = "Close";
                                    $scope.stopTimer();
                                    //$scope.score  =0
                                    $state.go("studentAssignmentList");
                                    //$scope.totalQuestion = []
                                }
                            })

                        }
                        $scope.noOfAttempts = 0

                    } else {
                        $scope.resultStatus = 'Hint'
                        $scope.actionBtnName = "Try Again";
                    }

                } else if ($scope.actionBtnName == "Next") {
                    $scope.startTimer()
                    $scope.answerStatus = "normal"
                    $scope.resetValues();
                    //load next question
                    $scope.indexToShow = ($scope.indexToShow + 1) % $scope.questions.length;
                    $scope.isOptionSelected = false;
                    $scope.splitMatchQuestion();

                } else {

                }


            };

            $scope.setSelectedAns = function(option, quesType) {
                $scope.isOptionSelected = true;
                if (quesType == "MCQ" || quesType == "TF") {
                    $scope.selectedDesc = option
                    $scope.selectedAns[$scope.indexToShow] = option;
                } else if (quesType == "MR") {
                    $scope.selectedCheckAns = []
                    if ($scope.multipleCheckAnsData[$scope.indexToShow] != undefined) {
                        $scope.selectedCheckAns = $scope.multipleCheckAnsData[$scope.indexToShow]
                        var idx = $scope.selectedCheckAns.indexOf(option);
                        if (idx > -1) {
                            $scope.selectedCheckAns.splice(idx, 1);
                        } else {
                            $scope.selectedCheckAns.push(option);
                        }
                    } else {
                        $scope.selectedCheckAns.push(option);
                    }
                    $scope.multipleCheckAnsData[$scope.indexToShow] = $scope.selectedCheckAns;
                }
            };

            $scope.checkChoiceExists = function(item) {
                let isChecked = false;
                if ($scope.multipleCheckAnsData[$scope.indexToShow] != undefined) {
                    $scope.selectedCheckAns = $scope.multipleCheckAnsData[$scope.indexToShow]
                    return $scope.selectedCheckAns.indexOf(item) > -1
                } else {
                    return isChecked
                }
            };

            $scope.changeQuestion = function(questCount) {
                if (questCount != $scope.indexToShow) {
                    //Change button name for next question
                    if ($scope.isPracticeTest) {
                        $scope.resetValues();
                    } else {
                        //if($scope.totalQuestion[$scope.indexToShow].label != 'Unmark'){
                        $scope.evaluateQuizQuestion($scope.indexToShow);
                        //}
                        //$scope.evaluateQuizQuestion($scope.indexToShow);
                    }
                    $scope.indexToShow = questCount;
                    $scope.splitMatchQuestion();
                    if (!$scope.isPracticeTest) {
                        $scope.startTimer()
                    }
                }
            };


            $scope.checkMatchChoiceExists = function(index) {
                return $scope.mtfAnsData.indexOf(index) > -1;
            };
            $scope.resetValues = function() {
                //Change button name for next question
                $scope.actionBtnName = "Submit";
                //Empty the answer type and explanation
                $scope.answerType = "";
                $scope.ansExplanation = "";
                $scope.ansFib = [];
                $scope.errorFib = {};
                $scope.errorMtf = {};
                $scope.errorMcqQues = {};
                $scope.errorMrQues = {};
                $scope.resultStatus = ''
                $scope.selectedDesc = ''
                $scope.errorTFQues = {};
                $scope.ansExplantionMTF = ''
                $scope.ansExplantionMR = ''
                $scope.ansExplantionFIB = ''
            };

            $scope.shuffle = function(array) {
                var m = array.length,
                    t, i;
                while (m) {
                    // Pick a remaining element…
                    i = Math.floor(Math.random() * m--);
                    // And swap it with the current element.
                    t = array[m];
                    array[m] = array[i];
                    array[i] = t;
                }
                return array;
            }

            /* Start Quiz*/
            $scope.loadPrevQues = function() {
                //if($scope.totalQuestion[$scope.indexToShow].label != 'Unmark'){
                $scope.evaluateQuizQuestion($scope.indexToShow);
                //}
                $scope.indexToShow = ($scope.indexToShow - 1) % $scope.totalQuestiontoShow
                $scope.isOptionSelected = false;
                $scope.splitMatchQuestion();
                $scope.startTimer()
            }

            $scope.loadNextQues = function() {
                if (($scope.indexToShow + 1) != $scope.totalQuestiontoShow) {

                    //if($scope.totalQuestion[$scope.indexToShow].label != 'Unmark'){
                    $scope.evaluateQuizQuestion($scope.indexToShow);
                    //}

                    $scope.indexToShow = ($scope.indexToShow + 1) % $scope.totalQuestiontoShow
                    $scope.isOptionSelected = false;
                    $scope.splitMatchQuestion();
                } else {
                    //$scope.totalQuestion[$scope.indexToShow].label = "Submit"
                }
                $scope.startTimer()
            }
            $scope.reviewQuestion = function() {

                if ($scope.totalQuestion[$scope.indexToShow].label == "Mark for review") {
                    $scope.totalQuestion[$scope.indexToShow].status = 'review'
                    $scope.totalQuestion[$scope.indexToShow].label = 'Unmark'
                } else {
                    $scope.totalQuestion[$scope.indexToShow].status = 'normal'
                    $scope.totalQuestion[$scope.indexToShow].label = 'Mark for review'
                }
            }
            $scope.checkQuizChoiceExists = function(index) {
                return $scope.attendedQuesObj.indexOf(index) > -1;
            };
            $scope.evaluateQuizQuestion = function(index) {
                let isCorrectAns = false;
                let questionMark = 0
                let questObj = $scope.questions[$scope.indexToShow];

                /*START - MCQ*/
                if (questObj.type == "MCQ") {
                    if (questObj.pattern == "single") {
                        angular.forEach(questObj.choice, function(value, key) {
                            if (value.correct && $scope.selectedAns[$scope.indexToShow] == value.desc) {
                                //$scope.stopTimer();
                                //console.log($scope.timeData)
                                isCorrectAns = true;
                                questionMark = questObj.mark
                            }
                        });
                        /*$scope.stopTimer();
                        console.log($scope.timeData)*/
                    } else {
                        var wrongAnsCount = 0;
                        angular.forEach(questObj.choice, function(value, key) {

                            if (value.correct) {
                                if ($scope.checkChoiceExists(value.desc)) {
                                    //CASE : Correct Ans
                                } else {
                                    wrongAnsCount += 1;
                                }
                                //$scope.stopTimer();
                                //console.log($scope.timeData)
                            } else if ($scope.checkChoiceExists(value.desc)) {
                                wrongAnsCount += 1;
                            }

                        });
                        /* $scope.stopTimer();
                         console.log($scope.timeData)*/
                        if (wrongAnsCount == 0) {
                            isCorrectAns = true;
                            questionMark = questObj.mark
                        }
                    }
                }
                /*END - MCQ*/

                /* START - TF*/
                else if (questObj.type == "TF") {
                    var keepGoing = false;
                    angular.forEach(questObj.choice, function(value, key) {
                        if (!keepGoing) {
                            if (value.correct && $scope.selectedAns[$scope.indexToShow] == value.tag) {
                                //$scope.stopTimer();
                                //console.log($scope.timeData)
                                isCorrectAns = true;
                                keepGoing = true;
                                questionMark = questObj.mark
                            } else {
                                keepGoing = false;
                            }
                        }

                    });
                    /*$scope.stopTimer();
                    console.log($scope.timeData)*/
                }
                /*END - TF*/

                /*START - FIB*/
                else if (questObj.type == "FIB") {
                    var fibAnswerCount = questObj.answer.length;
                    var wrongAnsCount = 0;

                    angular.forEach(questObj.answer, function(value, key) {
                        if ($scope.ansFib.length > 1) {
                            if ($scope.ansFib[$scope.indexToShow] == undefined || $scope.ansFib[$scope.indexToShow].choice[value.desc] == "" || $scope.ansFib[$scope.indexToShow].choice[value.desc] == undefined) {
                                wrongAnsCount += 1;
                                //$scope.stopTimer();
                                //console.log($scope.timeData)
                                //Case : User doesnt't write
                            } else {
                                if (value.desc.toLowerCase() == $scope.ansFib[$scope.indexToShow].choice[value.desc].toLowerCase()) {
                                    //Case :  Correct Answer
                                    //$scope.stopTimer();
                                    //console.log($scope.timeData)
                                } else {
                                    //Case :  Wrong Answer
                                    wrongAnsCount += 1;
                                }
                            }
                        } else {
                            wrongAnsCount += 1;
                        }

                    });

                    if (wrongAnsCount == 0) {
                        isCorrectAns = true;
                        questionMark = questObj.mark
                    }
                    /* $scope.stopTimer();
                     console.log($scope.timeData)*/
                }
                /*END - FIB*/

                /*START - MTF*/
                else if (questObj.type == "MTF") {
                    var wrongAnsCount = 0;
                    for (var i = 0; i < $scope.quesData.length; i++) {
                        var optionsObj = questObj.option[i];
                        if ($scope.quesData[i].tag == optionsObj.question.tag && $scope.mtfAnsData[$scope.indexToShow][i].tag == optionsObj.answer.tag) {
                            //Case : Correct Answer
                            // $scope.stopTimer();
                            //console.log($scope.timeData)
                        } else {
                            //Case : Wrong Answer
                            wrongAnsCount += 1;
                        }
                    }
                    if (wrongAnsCount == 0) {
                        isCorrectAns = true;
                        questionMark = questObj.mark.total;
                    }
                    /* $scope.stopTimer();
                     console.log($scope.timeData)*/
                }

                /*END - MTF*/
                $scope.stopTimer();
                //console.log('Current Time : ' + $scope.timeData)
                if ($scope.totalQuestion[$scope.indexToShow].label != 'Unmark') {
                    $scope.totalQuestion[$scope.indexToShow].status = 'submit';
                }
                $scope.timeData = $scope.attendedQuesObj[$scope.indexToShow].timeElapsed + $scope.timeData
                    //console.log('Updated Time : '+$scope.timeData)
                $scope.attendedQuesObj[$scope.indexToShow] = {
                    questionId: questObj._id,
                    mark: questionMark,
                    timeElapsed: $scope.timeData
                }
            }


            let quizMarks = {
                'studentId': Meteor.userId(),
                'type': 'Quiz',
                'date': currentDay,
                'timeElapsed': $scope.elapsedTime,
                'result': []
            }
            $scope.uploadAssignment = function() {
                let today = new Date()
                var day = today.getDay()
                var currentDay = new Date(today.getFullYear(), today.getMonth(), today.getDate())


                if ($scope.pdfFileUpload) {

                    var answerData = [];
                    let answer = {
                        "studentId": Meteor.userId(),
                        "question_id": $scope.getAssginmentQuestion_id,
                        "type": "pdf",
                        "data": $scope.assmtUrlData,
                        "submitedDate": currentDay
                    }
                    answerData = answer
                } else {

                    let answer = {
                        "studentId": Meteor.userId(),
                        "question_id": $scope.getAssginmentQuestion_id,
                        "type": "text",
                        "data": $scope.textareaText,
                        "submitedDate": currentDay
                    }
                    answerData = answer
                }


                console.log(answerData);



                Meteor.call('updateAssignment', answerData, $scope.getAssginment_id, Meteor.userId(), (error, result) => {
                    if (error) {
                        console.log("Error:" + error);
                    } else {

                    }
                })
                $state.go('studentAssignmentList');
            }
            $scope.submitQuizQuestion = function() {
                $scope.isSubmitted = true
                clearTimer()
                    //if($scope.totalQuestion[$scope.indexToShow].label != 'Unmark'){
                $scope.evaluateQuizQuestion($scope.indexToShow);
                //}

                let finalScore = 0
                for (var i = 0; i < $scope.attendedQuesObj.length; i++) {
                    if ($scope.attendedQuesObj[i] != undefined) {
                        finalScore += $scope.attendedQuesObj[i].mark
                        quizMarks.result.push({
                            questionId: $scope.attendedQuesObj[i].questionId,
                            mark: $scope.attendedQuesObj[i].mark,
                            timeElapsed: $scope.attendedQuesObj[i].timeElapsed
                        })
                    }
                }
                quizMarks.timeElapsed = $scope.elapsedTime
                    //
                Meteor.call('updateExamResult', quizMarks, (error, result) => {
                    if (error) {
                        console.log("Error:" + error);
                    } else {
                        alert(' Score ' + finalScore + ' / ' + $scope.quizTotalMark)
                    }
                })
                Meteor.call('setNotificationStatus', $scope.quizNotificationId, (error, result) => {
                        if (error) {
                            console.log("Error:" + error);
                        } else {
                            console.log('Notification status has been updated');
                        }
                    })
                    //
                console.log(' Score ' + finalScore + ' / ' + $scope.quizTotalMark)
            }

            //
            $scope.getTimeRemaining = function(endTime) {
                    let t = Date.parse(endTime) - Date.parse(new Date());
                    let seconds = Math.floor((t / 1000) % 60);
                    let minutes = Math.floor((t / 1000 / 60) % 60);
                    let hours = Math.floor((t / (1000 * 60 * 60)) % 24);
                    let days = Math.floor(t / (1000 * 60 * 60 * 24));
                    return {
                        'total': t,
                        'days': days,
                        'hours': hours,
                        'minutes': minutes,
                        'seconds': seconds
                    }
                }
                // 
            let timeinterval
            $scope.initializeClock = function(endTime) {

                function updateClock() {
                    let t = $scope.getTimeRemaining(endTime);
                    let days = t.days > 0 ? t.days + ' : ' : ''

                    $scope.remainingTime = days + ('0' + t.hours).slice(-2) + " : " + ('0' + t.minutes).slice(-2) + " : " + ('0' + t.seconds).slice(-2)

                    if (t.total <= 0) {
                        clearInterval(timeinterval);
                        //Auto submission
                        if (!$scope.isSubmitted) {
                            $scope.submitQuizQuestion()
                        }
                    }
                }

                updateClock();
                timeinterval = setInterval(updateClock, 1000);
            }

            function clearTimer() {
                clearInterval(timeinterval);
                $scope.remainingTime = '00 : 00 : 00'
            }


            /* End Quiz*/

        }
    }
})


angular.module('cordova_lms').filter('hhmmss', function() {
    return function(time) {
        var sec_num = parseInt(time, 10); // don't forget the second param
        var hours = Math.floor(sec_num / 3600);
        var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
        var seconds = sec_num - (hours * 3600) - (minutes * 60);

        if (hours < 10) {
            hours = "0" + hours;
        }
        if (minutes < 10) {
            minutes = "0" + minutes;
        }
        if (seconds < 10) {
            seconds = "0" + seconds;
        }
        var time = hours + ':' + minutes + ':' + seconds;
        return time;
    }
});

angular.module('cordova_lms')
    .controller('AppCtrl', function($scope) {})
    .directive('chooseFile', function() {
        return {
            link: function(scope, elem, attrs) {
                var button = elem.find('button');
                var input = angular.element(elem[0].querySelector('input#fileInput'));
                button.bind('click', function() {
                    input[0].click();

                });
                input.bind('change', function(e) {
                    scope.$apply(function() {
                        var files = e.target.files;
                        if (files[0]) {
                            scope.fileName = files[0].name;
                        } else {
                            scope.fileName = null;
                        }
                    });
                });
            }
        };
    });
