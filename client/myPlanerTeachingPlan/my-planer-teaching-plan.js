angular.module('cordova_lms').directive('myPlanerTeachingPlan', function() {
  return {
    restrict: 'E',
    templateUrl: 'client/myPlanerTeachingPlan/my-planer-teaching-plan.html',
    controllerAs: 'teachingPlanCtrl',

     controller: function ($scope, $reactive,$state,$filter,$mdToast, $timeout,$interval,$meteor,$interval,$mdDialog, $document){
      $reactive(this).attach($scope);
      let self = this
      $scope.teachingPlans=[]
      self.subscribe('getTeachingPlans',
        () => {
          return [] // parameters to be passed to subscribe method
        },
        () => {
          $scope.teachingPlans = TeachingPlans.find().fetch();
          console.log($scope.teachingPlans) 
        }
      )
        $scope.getTeachingPlan=function(teachingPlan){
console.log(teachingPlan)


$state.go('viewTeachingPlan',{"getTeachingPlan":teachingPlan._id});

    }
 $scope.createTeachingPlan=function(teachingPlan){
console.log(teachingPlan)


$state.go('createTeachingPlan');

    }

    }
  }
})


