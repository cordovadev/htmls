angular.module('cordova_lms').directive('studentAssignmentList', function(MyResource) {
    return {
        restrict: 'E',
        templateUrl: 'client/studentAssignmentList/student-assignment-list.html',
        controllerAs: 'studentAssignmentListCtrl',
        controller: function($scope, $reactive, $state, $filter, $timeout, $interval, $meteor, $interval, $rootScope, QuestionsFactory, $mdDialog, $mdMedia) {
            $scope.status = '  ';
            $scope.customFullscreen = $mdMedia('xs') || $mdMedia('sm');
            $reactive(this).attach($scope);
            let self = this
            self.dynamicTheme = "default"
            self.user = {}
            let studentId = Meteor.userId();
            $scope.studentId = Meteor.userId();
            $scope.assignmentStatus = "";
            $scope.quesTypes = []
            console.log($scope.questions)
            $scope.getstudentDetailsData = function() {
                self.subscribe('getStudentData', () => {
                    return [$scope.studentId] // parameters to be passed to subscribe method
                }, () => {
                    console.log(StudentDetails.find({
                        userId: $scope.studentId
                    }).fetch())
                    $scope.getstudentDetails = StudentDetails.find({
                        userId: $scope.studentId
                    }).fetch()[0];
                    self.subscribe('checkAssignmentNotification', () => {
                        return [studentId] // parameters to be passed to subscribe method 
                    }, () => {
                        let moduleDetails = Quiz.find({
                                "type": "assignment"
                            }).fetch()
                            // console.log(moduleDetails)
                        $scope.quesTypes = moduleDetails
                    })

                })

                console.log($scope.quesTypes)
            };

            $scope.getDateInFormat = function(date) {
                dateFromat = $filter('date')(date, "dd-MM-yyyy")
                console.log(dateFromat)
                return dateFromat
            }

            $scope.collapseAll = function(data) {
                for (var i in $scope.quesTypes) {
                    if ($scope.quesTypes[i] != data) {
                        $scope.quesTypes[i].expanded = false;
                    }
                }
                data.expanded = !data.expanded;
            };
            $scope.getStudentAssignmentStatus = function(assignment) {
                console.log(assignment)
                $scope.assignmentStatus = "";
                 $scope.SubmitedDate = ""
                let assignmentData = assignment

                let assignmentAnswer = assignmentData.answer
                assignmentAnswer.forEach(function(val) {
                    console.log(val.submitedDate);
                    $scope.SubmitedDate=val.submitedDate;
                     $scope.mark=val.mark;
                    if (val.studentId == $scope.getstudentDetails._id) {
                        console.log("status: " + val.status);
                        if (val.status == "not Submited") {
                            $scope.assignmentStatus = "Start Now";


                        } 
                        if (val.status == "submited") {
                            $scope.assignmentStatus = "submited";


                        } 
                        if (val.status == "reviewd") {
                            $scope.assignmentStatus = "reviewd";


                        }
                        console.log($scope.assignmentStatus)
                    }
                })

                $scope.quesTypes1 = assignmentData
                var asingValue = $scope.assignmentStatus
                return asingValue

            };
            /* list assignment questions*/
            $scope.getAssginmentQuestions = function(assignmentId, question_list, data, ev) {

                let questionsList = question_list;
               if (data == 'startAssignment') {
                    $state.go('studentAssignmentView', {
                        "getAssginmentQuestion": questionsList,
                        "assignment_id":assignmentId,
                        "question_id": assignmentId
                    });
                } else {
                   console.log(assignmentId)
                console.log(question_list)
                
                    var useFullScreen = ($mdMedia('sm') || $mdMedia('xs')) && $scope.customFullscreen;

                    $mdDialog.show({
                            locals: {
                                "assignment_id": assignmentId,
                                "answer": question_list,
                                "student_id": $scope.getstudentDetails._id
                            },
                            controller: DialogController,
                            templateUrl: 'client/studentAssignmentList/student-assignment-answer-view-popup.html',
                            parent: angular.element(document.body),
                            targetEvent: ev,
                            clickOutsideToClose: true,
                            fullscreen: useFullScreen
                        })
                        .then(function(answer) {
                            $scope.status = 'You said the information was "' + answer + '".';
                        }, function() {
                            $scope.status = 'You cancelled the dialog.';
                        });



                    $scope.$watch(function() {
                        return $mdMedia('xs') || $mdMedia('sm');
                    }, function(wantsFullScreen) {
                        $scope.customFullscreen = (wantsFullScreen === true);
                    });
                }
            }


        }
    }
})

function DialogController($scope, $mdDialog, assignment_id, student_id, answer, $state) {
    console.log(answer)
    angular.forEach(answer, function(value, key) {
        console.log(value.studentId + "\t" + student_id)
        if (value.studentId == student_id) {
            $scope.marks = value.mark
            $scope.review = value.review
        }

    })
    $scope.hide = function() {
        $mdDialog.hide();
    };

    $scope.cancel = function() {
        $mdDialog.cancel();
    };

    $scope.answer = function(answer) {
        $mdDialog.hide(answer);
    };
    $scope.studentAssignmentList = function() {

        $mdDialog.hide();
        $state.go('studentAssignmentList');


    }
}
