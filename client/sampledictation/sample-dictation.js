angular.module('cordova_lms').directive('sampleDictation', function () {
  return {
    restrict: 'E',
    templateUrl: 'client/sampledictation/sampledictation.html',
    controllerAs: 'directiveCtrl',
    controller: function ($scope,$mdToast, $reactive,$window,$stateParams,$state,$http) 
    {
    	$reactive(this).attach($scope)
   		let self = this
      $scope.groupData= []
      $scope.textMeaning= []
      $scope.meaning= []
      $scope.orginalMeaning= []
      $scope.conductDitaction = function (word) 
	  	{
	  		if(word==="" || word===null || word===undefined)
	  		{
	  			$scope.showToast("Please Enter the word")
	  		}
	  		else
	  		{
          $scope.groupData= []
          $scope.textMeaning= []
          $scope.meaning= []
          $scope.orginalMeaning= []
		  		var u = new SpeechSynthesisUtterance();
		  		u.text = word;
		  		speechSynthesis.speak(u);
          Meteor.call('callDictionaryApi',word, (error,result) => 
          {
            if(error)
            {
              console.log(error.invalidKeys);
            }
            else
            {       
              $scope.meaning=[]     
              var obj = JSON.parse(result.content);
              $scope.groupData=   obj.tuc
              $scope.textMeaning.push($scope.groupData[0].meanings)
              var output=$scope.textMeaning[0]
              for( var i=0;i<output.length;i++)
              {
                $scope.orginalMeaning.push(output[i])
              }
              for( var i=0;i<$scope.orginalMeaning.length;i++)
              {
                $scope.meaning.push($scope.orginalMeaning[i].text)
              } 
              data ($scope.meaning)                     
            }
          })
	  		}	  	
	  	}

      function data (obj){    
         $scope.$apply()
        $scope.meaning = obj
      }

      var last = {
        bottom: false,
        top: true,
        left: false,
        right: true
      };
      $scope.toastPosition = angular.extend({},last);
      $scope.getToastPosition = function() {
        sanitizePosition();
        return Object.keys($scope.toastPosition)
          .filter(function(pos) { return $scope.toastPosition[pos]; })
          .join(' ');
      };
      function sanitizePosition() {
        var current = $scope.toastPosition;
        if ( current.bottom && last.top ) current.top = false;
        if ( current.top && last.bottom ) current.bottom = false;
        if ( current.right && last.left ) current.left = false;
        if ( current.left && last.right ) current.right = false;
        last = angular.extend({},current);
      }

      $scope.showToast = function( data) {
        $mdToast.show(
          $mdToast.simple()
            .textContent(data)
            .position($scope.getToastPosition())
            .hideDelay(3000)
        );
      };

    }
  }
});