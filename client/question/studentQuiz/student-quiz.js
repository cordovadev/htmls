angular.module('cordova_lms').directive('studentQuiz', function (MyResource) {
  return {
    restrict: 'E',
    templateUrl: 'client/quiz/studentQuiz/student-quiz.html',
    controllerAs: 'studentQuizCtrl',
    controller: function ($scope, $reactive,$state,$filter, $timeout,$interval,$meteor,$interval,$state){
      $reactive(this).attach($scope);
      let self = this
      self.dynamicTheme = "default"
      self.user = {}
      $scope.lockLeft = true;
      $scope.indexToShow = 0;
      $scope.score = 0;
      $scope.selectedAns = "";
      $scope.actionBtnName="Submit";
      $scope.totalQuestion = [];
      $scope.isOptionSelected = false;
      $scope.ansFib = {};
      $scope.errorFib = {};
      $scope.errorMtf = {};
      $scope.errorMrQues = {};
      $scope.quesData = [];
      $scope.ansData = [];
      $scope.answerStatus = "normal";
      $scope.selectedCheckAns = [];


//timer with timeout
   $scope.timerWithTimeout = 0;
   $scope.startTimerWithTimeout = function() {
    $scope.timerWithTimeout = 0;
    if($scope.myTimeout){
      $timeout.cancel($scope.myTimeout);
    }
    $scope.onTimeout = function(){
        $scope.timerWithTimeout++;
        $scope.myTimeout = $timeout($scope.onTimeout,1000);
    }
    $scope.myTimeout = $timeout($scope.onTimeout,1000);
  };
 $scope.startTimerWithTimeout();
  $scope.resetTimerWithTimeout = function(){
    $scope.timerWithTimeout = 0;
    $timeout.cancel($scope.myTimeout);
  }
  
  //timer with interval
  $scope.timerWithInterval = 0;
   $scope.startTimerWithInterval = function() {
    $scope.timerWithInterval = 0;
    if($scope.myInterval){
      $interval.cancel($scope.myInterval);
    }
    $scope.onInterval = function(){
        $scope.timerWithInterval++;
    }
    $scope.myInterval = $interval($scope.onInterval,1000);
  };
  
  $scope.resetTimerWithInterval = function(){
    $scope.timerWithInterval = 0;
    $interval.cancel($scope.myInterval);
  }

       /*Subscribe call to get Question Bank Details*/
      self.subscribe('getQuestionBank',
        () => {
          return [] // parameters to be passed to subscribe method
        },
        () => {
          $scope.questions = QuestionBank.find().fetch();
          for (var i = 0; i < $scope.questions.length; i++) {
            $scope.totalQuestion.push({ count: i+1, isActive: false});
          }
        }
      )
      let marks = []
      $scope.submitQuestion = function(){
        
        if($scope.actionBtnName == "Submit"){
            var isCorrectAns  =false;
            var questObj = $scope.questions[$scope.indexToShow];
            if(questObj.type=="MCQ"){
              if(questObj.pattern == "single"){
                angular.forEach(questObj.choice, function(value, key) {
                    if(value.correct && $scope.selectedAns == value.desc){
                      $scope.score+=questObj.mark;
                      isCorrectAns = true;
                      $scope.answerType = "Correct Answer";
                      $scope.ansExplanation = value.explanation;
                      $scope.answerStatus = "correct"
                      let markObj = {"level":questObj.level,"type":questObj.type,"mark":questObj.mark,"timeElapsed":"23423"}
                      marks.push(markObj)
                      
                    }else if($scope.selectedAns == value.desc){
                      $scope.answerType = "Wrong Answer";
                      $scope.ansExplanation = value.hint;
                      $scope.answerStatus = "wrong"
                    }
                  });
              }else{
                  var wrongAnsCount = 0;
                  angular.forEach(questObj.choice, function(value, key) {
                    
                      if(value.correct){
                        if($scope.checkChoiceExists(value.desc)){
                          $scope.errorMrQues[value.desc] = {answerType: 'Correct Answer', answerStatus: 'correct',msg:(key+1)+' . '+value.explanation};
                        }else{
                          wrongAnsCount+=1;
                           //$scope.errorMrq[value.desc] = {answerType: 'Wrong Answer', answerStatus: 'wrong', msg:(key+1)+' . '+value.hint};
                        }
                      }else if($scope.checkChoiceExists(value.desc)){
                        wrongAnsCount+=1;
                        $scope.errorMrQues[value.desc] = {answerType: 'Wrong Answer', answerStatus: 'wrong', msg:(key+1)+' . '+value.hint};
                      }
                   
                  });
                  if(wrongAnsCount==0){
                      $scope.score+=questObj.mark;
                      isCorrectAns = true;
                  }
                }
              }
              else if(questObj.type=="TF"){
              var keepGoing = false;
              angular.forEach(questObj.choice, function(value, key) {
                if(!keepGoing){
                    if(value.correct && $scope.selectedAns == value.tag){
                    $scope.score+=questObj.mark;
                    let markObj = {"level":questObj.level,"type":questObj.type,"mark":questObj.mark,"timeElapsed":"23423"}
                    marks.push(markObj)
                    isCorrectAns = true;
                    keepGoing = true;
                    $scope.answerType = "Correct Answer";
                    $scope.ansExplanation = value.explanation;
                    $scope.answerStatus = "correct"
                    
                  }else{
                    keepGoing = false;
                    $scope.answerType = "Wrong Answer";
                    $scope.ansExplanation = value.hint;
                    $scope.answerStatus = "wrong"
                  }
                }
                
              });
            }else if(questObj.type=="FIB"){
              var fibAnswerCount = questObj.answer.length;
              var wrongAnsCount = 0;
              
              angular.forEach(questObj.answer, function(value, key) {
                if($scope.ansFib[value.desc] == "" || $scope.ansFib[value.desc] ==undefined){
                  wrongAnsCount+=1;
                  $scope.errorFib[value.desc] = {answerType: 'Cant be empty', answerStatus: 'info', msg:(key+1)+' . '+value.hint};
                }else{
                  if(value.desc.toLowerCase() == $scope.ansFib[value.desc].toLowerCase()){
                    $scope.errorFib[value.desc] = {answerType: 'Correct Answer', answerStatus: 'correct',msg:(key+1)+' . '+value.explanation};
                    let markObj = {"level":questObj.level,"type":questObj.type,"mark":questObj.mark,"timeElapsed":"23423"}
                    marks.push(markObj)
                  }else{
                     $scope.errorFib[value.desc] = {answerType: 'Wrong Answer', answerStatus: 'wrong', msg:(key+1)+' . '+value.hint};
                     wrongAnsCount+=1;
                  }
                }
                
              });
              
              if(wrongAnsCount==0){
                $scope.score+=questObj.mark;
                isCorrectAns = true;
              }
            }else if(questObj.type=="MTF"){
                var wrongAnsCount = 0;
                
                for (var i = 0; i < $scope.quesData.length; i++){
                    var optionsObj = questObj.option[i];
                    if($scope.quesData[i].tag == optionsObj.question.tag && $scope.ansData[i].tag == optionsObj.answer.tag){
                        $scope.errorMtf[$scope.quesData[i].tag] = { answerType: 'Correct Answer', answerStatus: 'correct',msg:(i+1)+' . '+optionsObj.explanation};//questObj.option[i].explanation};
                        let markObj = {"level":questObj.level,"type":questObj.type,"mark":questObj.mark,"timeElapsed":"23423"}
                        marks.push(markObj)
                    }else{
                        $scope.errorMtf[$scope.quesData[i].tag] = { answerType: 'Wrong Answer', answerStatus: 'wrong',msg:(i+1)+' . '+optionsObj.explanation};//questObj.option[i].explanation};
                        wrongAnsCount+=1;
                    }
                }
                
                if(wrongAnsCount==0){
                  $scope.score+=questObj.mark.total;
                  isCorrectAns = true;
                }
            }

            if(isCorrectAns){
                $scope.totalQuestion[$scope.indexToShow].isActive = true;
                if(($scope.indexToShow + 1)!=$scope.questions.length){
                  $scope.actionBtnName = "Next";
                }else{
                  Meteor.call('updateStudent', marks,  (error,result) => {
                      if(error){
                        console.log("Error:"+error);
                      }
                      else{
                          alert('Succes')
                      }
                  })
                  $scope.actionBtnName = "Close";

                }
            }

        }else if($scope.actionBtnName == "Next"){
             $scope.answerStatus = "normal"
             $scope.resetValues();
             //load next question
             $scope.indexToShow = ($scope.indexToShow + 1) % $scope.questions.length;
             $scope.isOptionSelected = false;
             $scope.splitMatchQuestion();
             
        }else{
              
        }
          
          
      };

      $scope.setSelectedAns = function(option,quesType){
        $scope.isOptionSelected = true;
        if(quesType=="MCQ" || quesType=="TF"){
          $scope.selectedAns = option;
        }else if(quesType=="MR"){
          var idx =  $scope.selectedCheckAns.indexOf(option);
          if (idx > -1) {
             $scope.selectedCheckAns.splice(idx, 1);
          }
          else {
             $scope.selectedCheckAns.push(option);
          }
        }
      };

      $scope.checkChoiceExists = function (item) {
        return  $scope.selectedCheckAns.indexOf(item) > -1;
      };

      $scope.changeQuestion = function(questCount){
       //Change button name for next question
        $scope.resetValues();
        $scope.indexToShow = questCount;
        $scope.splitMatchQuestion();
      };

      $scope.splitMatchQuestion = function(){
        //Match the Following
         if($scope.questions[$scope.indexToShow].type == "MTF"){
            $scope.quesData=[];
            $scope.ansData=[];
            angular.forEach($scope.questions[$scope.indexToShow].option, function(value, key) {
                $scope.quesData.push({ tag: value.question.tag, desc: value.question.desc});
                $scope.ansData.push({ tag: value.answer.tag, desc: value.answer.desc});
            });
            $scope.shuffle($scope.ansData);
        }
      };
      $scope.resetValues = function(){
          //Change button name for next question
           $scope.actionBtnName = "Submit";
           //Empty the answer type and explanation
           $scope.answerType = "";
           $scope.ansExplanation = "";
           $scope.ansFib = {};
           $scope.errorFib = {};
           $scope.errorMtf = {};
           $scope.errorMrQues = {};
           
      };

      $scope.shuffle = function(array) {
        var m = array.length, t, i;
        while (m) {
          // Pick a remaining element…
          i = Math.floor(Math.random() * m--);
          // And swap it with the current element.
          t = array[m];
          array[m] = array[i];
          array[i] = t;
        }
        return array;
      }

    }
  }
})


angular.module('cordova_lms').filter('hhmmss', function () {  
  return function (time) {
    var sec_num = parseInt(time, 10); // don't forget the second param
    var hours   = Math.floor(sec_num / 3600);
    var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
    var seconds = sec_num - (hours * 3600) - (minutes * 60);

    if (hours   < 10) {hours   = "0"+hours;}
    if (minutes < 10) {minutes = "0"+minutes;}
    if (seconds < 10) {seconds = "0"+seconds;}
    var time    = hours+':'+minutes+':'+seconds;
    return time;
  }
});
