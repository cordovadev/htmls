angular.module('cordova_lms').directive('previewQuiz', function (MyResource) {
  return {
    restrict: 'E',
    templateUrl: 'client/question/questionPreview/question-preview.html',
    controllerAs: 'quizPreviewCtrl',
    controller: function ($scope, $reactive,$state,$filter, $timeout,$interval,$meteor,$interval,$rootScope,QuestionsFactory){
      $reactive(this).attach($scope);
      $scope.questions = QuestionsFactory.getArray();
      console.log($scope.questions)
  }
}
})