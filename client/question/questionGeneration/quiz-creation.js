angular.module('cordova_lms').directive('quizCreation', function (MyResource) {
  return {
    restrict: 'E',
    templateUrl: 'client/question/questionGeneration/quiz-creation.html',
    controllerAs: 'quizCreationCtrl',
    controller: function ($scope, $reactive,$state,$filter, $timeout,$interval,$meteor,$interval,$rootScope,QuestionsFactory,$mdDialog,$mdMedia){
      $reactive(this).attach($scope);
      let self = this
      self.dynamicTheme = "default"
      self.user = {}

      $scope.qCount = 50
      $scope.Math = window.Math;
      $scope.info = "";
      //$scope.isMCQOpen =false;
      $scope.qSubCount = 0
      $scope.isSuccess = false
      $scope.isQuestionSetPage = false

      $scope.onlyNumbers = /^\d+$/
      let today = new Date()
      let currentDay = new Date(today.getFullYear(),today.getMonth(),today.getDate())

      $scope.quesTypes=[{
                          title:'Multiple Choice Question',
                          type: 'MCQ', 
                          count:0,
                          levelOne:false,
                          levelTwo:false,
                          levelThree:false,
                          isOpen:false,
                          icon:'+',
                          isSelected:false,
                          l1Percent:0,
                          l2Percent:0,
                          l3Percent:0,
                          expanded:true
                        },
                        {
                          title:'Fill in the blanks',
                          type: 'FIB',
                          count:0,
                          levelOne:false,
                          levelTwo:false,
                          levelThree:false,
                          isOpen:false,
                          icon:'+',
                          isSelected:false,
                          l1Percent:0,
                          l2Percent:0,
                          l3Percent:0,
                          expanded:false
                        },
                        {
                          title:'True or False',
                          type: 'TF',count:0,
                          levelOne:false,
                          levelTwo:false,
                          levelThree:false,
                          isOpen:false,
                          icon:'+',
                          isSelected:false,
                          l1Percent:0,
                          l2Percent:0,
                          l3Percent:0,
                          expanded:false
                        },
                        {
                          title:'Match the following',
                          type: 'MTF',count:0,
                          levelOne:false,
                          levelTwo:false,
                          levelThree:false,
                          isOpen:false,
                          icon:'+',
                          isSelected:false,
                          l1Percent:0,
                          l2Percent:0,
                          l3Percent:0,
                          expanded:false
                        },
                        {
                          title:'Short Answer',
                          type: 'SA',count:0,
                          levelOne:false,
                          levelTwo:false,
                          levelThree:false,
                          isOpen:false,
                          icon:'+',
                          isSelected:false,
                          l1Percent:0,
                          l2Percent:0,
                          l3Percent:0,
                          expanded:false
                        }];

      $scope.setQuestionCount = function(index){
        $scope.qSubCount = 0
        $scope.quesTypes[index].isSelected = true
        for(var i=0;i<$scope.quesTypes.length;i++){
          if(angular.isNumber($scope.quesTypes[i].count)){
            $scope.qSubCount += $scope.quesTypes[i].count

             if($scope.quesTypes[i].count<$scope.quesTypes[i].l1Percent){
                $scope.quesTypes[i].l1Percent = $scope.quesTypes[i].count
             }

             if($scope.quesTypes[i].count<$scope.quesTypes[i].l2Percent){
                $scope.quesTypes[i].l2Percent = $scope.quesTypes[i].count
             }

             if($scope.quesTypes[i].count<$scope.quesTypes[i].l3Percent){
                $scope.quesTypes[i].l3Percent = $scope.quesTypes[i].count
             }
          }
        }
        console.log($scope.qSubCount);
      }
      $scope.setSubQuestionCount = function(index,mode){
        $scope.qSubChildCount = 0
        console.log($scope.qSubChildCount);
      }
      $scope.resetValues = function(index,mode){
        if(mode == 1){
          $scope.quesTypes[index].l1Percent = 0;
        }else if(mode == 2){
          $scope.quesTypes[index].l2Percent = 0;
        }else{
          $scope.quesTypes[index].l3Percent = 0;
        }
      }
      $scope.openChildComp = function(index,title){

        for(var i=0;i<$scope.quesTypes.length;i++){
          if(index == i){
            $scope.quesTypes[i].isOpen = !$scope.quesTypes[i].isOpen
            $scope.quesTypes[i].icon = $scope.quesTypes[i].isOpen? '-' : '+'
          }else{
            $scope.quesTypes[i].isOpen = false
            $scope.quesTypes[i].icon ='+'
          }
        }
      }

      $scope.setSelectedOption = function(index){
        $scope.quesTypes[index].isSelected = $scope.quesTypes[index].isSelected ? false : true
        //Set initial value
        if(!$scope.quesTypes[index].isSelected){
          $scope.quesTypes[index].count = 0
          $scope.quesTypes[index].levelOne = false
          $scope.quesTypes[index].levelTwo = false
          $scope.quesTypes[index].levelThree = false
          $scope.quesTypes[index].l1Percent = 0
          $scope.quesTypes[index].l2Percent = 0
          $scope.quesTypes[index].l3Percent = 0
        }else{
           $scope.quesTypes[index].isSelected  =true;
        }
      }

      $scope.setSelectedSource = function(source){
          $scope.selectedSource = source
      }

      $scope.navigateQuestionSetPage = function(){

        let questionParams = {
            "type": "quiz",
            "total_question": $scope.qCount,
            "date": currentDay,
            "teacherId" : Meteor.userId(),
            'status' : "not published",
            'title' : $scope.qTitle,
            'subTitle' : $scope.qSubTitle,
            'description' : $scope.qDescription,
            "time" :{hour: $scope.selectedHour,min:$scope.selectedMin,sec:$scope.selectedSecond},
            "subject_id": 123,
            "theme_id": 123,
            "strands": $scope.selectedStrands,
            "themes": $scope.selectedThemes,
            "cross_subjects": $scope.selectedSubjects,
            "tags": $scope.selectedTags,
            "fetch_from": $scope.selectedSource
          }

         Quiz.attachSchema(quizSchemas,{replace: true})
          Quiz.simpleSchema().namedContext("quizSchemas").validate(questionParams, {modifier: false});
          var context = Quiz.simpleSchema().namedContext("quizSchemas")
          $scope.errors = {}
          $scope.errorsList = context.invalidKeys()
              .filter(function(data){
                  if(data.type != "keyNotInSchema") { return true}})
              .map(function(data){
                  return {
                      message : {name:data.name, msg:context.keyErrorMessage(data.name)}
                  }
          });
          if($scope.errorsList.length != 0 ) {
            angular.forEach($scope.errorsList, function(error,key) {
              if (error.message.name.toString().indexOf(".")>-1){
                $scope.errors[error.message.name.split(".")[1]]  = error.message['msg']
              }
              else{
                $scope.errors[error.message['name']]=error.message['msg']
              }
            });
          }else{
            $scope.isQuestionSetPage = true
          }
          //$scope.isQuestionSetPage = true
      }

      $scope.collapseAll = function(data) {
         for(var i in $scope.quesTypes) {
           if($scope.quesTypes[i] != data) {
             $scope.quesTypes[i].expanded = false;   
           }
         }
         data.expanded = !data.expanded;
      };

      $scope.navigateToManageScreen = function(){
        $scope.info = ""
        $scope.qSubCount =0

        $scope.isMCQSelected = false;
        $scope.isFIBSelected = false;
        $scope.isTFSelected = false;
        $scope.isMTFSelected = false;
        $scope.isSASelected = false;

        $scope.countMCQ = 0;
        $scope.countFIB = 0;
        $scope.countTF = 0;
        $scope.countMTF = 0;
        $scope.countSA = 0;

        $scope.subCountMCQ = 0;
        $scope.subCountFIB = 0;
        $scope.subCountTF = 0;
        $scope.subCountMTF = 0;
        $scope.subCountSA = 0;

        for(var i=0;i<$scope.quesTypes.length;i++){
          if(angular.isNumber($scope.quesTypes[i].count)){
            $scope.qSubCount += $scope.quesTypes[i].count
            
            switch($scope.quesTypes[i].type){
              case 'MCQ' :
                $scope.isMCQSelected = $scope.quesTypes[i].isSelected
                $scope.countMCQ = $scope.quesTypes[i].count
                $scope.subCountMCQ = $scope.quesTypes[i].l1Percent+$scope.quesTypes[i].l2Percent+$scope.quesTypes[i].l3Percent
                break;

              case 'FIB' :
                $scope.isFIBSelected = $scope.quesTypes[i].isSelected
                $scope.countFIB = $scope.quesTypes[i].count
                $scope.subCountFIB = $scope.quesTypes[i].l1Percent+$scope.quesTypes[i].l2Percent+$scope.quesTypes[i].l3Percent
                break;

              case 'TF' :
                $scope.isTFSelected = $scope.quesTypes[i].isSelected
                $scope.countTF = $scope.quesTypes[i].count
                $scope.subCountTF = $scope.quesTypes[i].l1Percent+$scope.quesTypes[i].l2Percent+$scope.quesTypes[i].l3Percent
                break;

              case 'MTF' :
                $scope.isMTFSelected = $scope.quesTypes[i].isSelected
                $scope.countMTF = $scope.quesTypes[i].count
                $scope.subCountMTF = $scope.quesTypes[i].l1Percent+$scope.quesTypes[i].l2Percent+$scope.quesTypes[i].l3Percent
                break;

              case 'SA' :
                $scope.isSASelected = $scope.quesTypes[i].isSelected
                $scope.countSA = $scope.quesTypes[i].count
                $scope.subCountSA = $scope.quesTypes[i].l1Percent+$scope.quesTypes[i].l2Percent+$scope.quesTypes[i].l3Percent
                break;
            }
          }
        }

        if($scope.qCount>$scope.qSubCount){
          $scope.info = "You have to select "+($scope.qCount-$scope.qSubCount)+" more Question(s)"
        }else if($scope.qCount<$scope.qSubCount){
         $scope.info = "You selected "+$scope.qSubCount+" out of "+$scope.qCount+" Question(s)"
        }else if($scope.isMCQSelected && $scope.countMCQ!=$scope.subCountMCQ){
            $scope.info = "OOPS, MCQ Question(s) count is mismatched!!"
        }
        else if($scope.isFIBSelected && $scope.countFIB!=$scope.subCountFIB){
            $scope.info = "OOPS, Fill in the blanks Question(s) count is mismatched!!"
        }
        else if($scope.isTFSelected && $scope.countTF!=$scope.subCountTF){
            $scope.info = "OOPS, True or false Question(s) count is mismatched!!"
        }
        else if($scope.isMTFSelected && $scope.countMTF!=$scope.subCountMTF){
            $scope.info = "OOPS, Match the following Question(s) count is mismatched!!"
        }
        else if($scope.isSASelected && $scope.countSA!=$scope.subCountSA){
            $scope.info = "OOPS, Short Answer Question(s) count is mismatched!!"
        }
        else{
          console.log($scope.quesTypes[0].count);
          //console.log($scope.quesTypes['MCQ'].count);

          //Question Objects
           $scope.quesDetails = {
              "type": "quiz",
              "total_question": $scope.qCount,
              "date": currentDay,
              "teacherId" : Meteor.userId(),
              'status' : "not published",
              'title' : $scope.qTitle,
              'subTitle' : $scope.qSubTitle,
              'description' : $scope.qDescription,
              "time" :{hour: $scope.selectedHour,min:$scope.selectedMin,sec:$scope.selectedSecond},
              "question": [
                {
                  "type": $scope.quesTypes[0].type,
                  "count":  $scope.quesTypes[0].count,
                  "level1": $scope.quesTypes[0].l1Percent,
                  "level2": $scope.quesTypes[0].l2Percent,
                  "level3": $scope.quesTypes[0].l3Percent
                },
                {
                  "type": $scope.quesTypes[1].type,
                  "count":  $scope.quesTypes[1].count,
                  "level1": $scope.quesTypes[1].l1Percent,
                  "level2": $scope.quesTypes[1].l2Percent,
                  "level3": $scope.quesTypes[1].l3Percent
                },
                {
                  "type": $scope.quesTypes[2].type,
                  "count":  $scope.quesTypes[2].count,
                  "level1": $scope.quesTypes[2].l1Percent,
                  "level2": $scope.quesTypes[2].l2Percent,
                  "level3": $scope.quesTypes[2].l3Percent
                },
                {
                  "type": $scope.quesTypes[3].type,
                  "count":  $scope.quesTypes[3].count,
                  "level1": $scope.quesTypes[3].l1Percent,
                  "level2": $scope.quesTypes[3].l2Percent,
                  "level3": $scope.quesTypes[3].l3Percent
                },
                {
                  "type": $scope.quesTypes[4].type,
                  "count":  $scope.quesTypes[4].count,
                  "level1": $scope.quesTypes[4].l1Percent,
                  "level2": $scope.quesTypes[4].l2Percent,
                  "level3": $scope.quesTypes[4].l3Percent
                }
            ],
            "subject_id": 123,
            "theme_id": 123,
            "strands": $scope.selectedStrands,
            "themes": $scope.selectedThemes,
            "cross_subjects": $scope.selectedSubjects,
            "tags": $scope.selectedTags,
            "fetch_from": $scope.selectedSource
          }
       
          //Server call to fetch question based on the selected params
           Quiz.attachSchema(quizSchemas,{replace: true})
          Quiz.simpleSchema().namedContext("quizSchemas").validate($scope.quesDetails , {modifier: false});
          var context = Quiz.simpleSchema().namedContext("quizSchemas")
          $scope.errors = {}
          $scope.errorsList = context.invalidKeys()
              .filter(function(data){
                  if(data.type != "keyNotInSchema") { return true}})
              .map(function(data){
                  return {
                      message : {name:data.name, msg:context.keyErrorMessage(data.name)}
                  }
          });
          if($scope.errorsList.length != 0 ) {
            angular.forEach($scope.errorsList, function(error,key) {
              if (error.message.name.toString().indexOf(".")>-1){
                $scope.errors[error.message.name.split(".")[1]]  = error.message['msg']
              }
              else{
                $scope.errors[error.message['name']]=error.message['msg']
              }
            });
          }else{
              
               //$scope.info = "Success";
               $scope.isSuccess = true

              Meteor.call('fetchQuestion',$scope.quesDetails,(error,result) => {
                if(error){
                    console.log(error.invalidKeys);
                }else{

                  mcqQuestions = result
                  QuestionsFactory.addArray(mcqQuestions);
                  let useFullScreen = ($mdMedia('sm') || $mdMedia('xs'));
                  $mdDialog.show({
                    controller: DialogController,
                    templateUrl: 'client/question/questionGeneration/preview.html',
                    parent: angular.element(document.body),
                    locals:{questions:mcqQuestions,questionDetails : $scope.quesDetails },
                    clickOutsideToClose:true,
                    fullscreen: useFullScreen
                  })
                  .then(function(answer) {
                    console.log("POPUP")

                  }, function() {
                    self.status = 'You cancelled the dialog.';
                  })         
                }
              })
            }

          }
    }



    $scope.roundOffLevelOnePerecentValue = function(index){
      //console.log($scope.quesTypes[index].l1Percent);

      let value = 1*$scope.quesTypes[index].l1Percent+1
      if(!isNaN(value) && angular.isNumber(value)){
        console.log($scope.Math.round(value));
      }else{
        $scope.quesTypes[index].l1Percent="";
      }
    }
    
    $scope.hideAllChild = function(){
      for(var i=0;i<$scope.quesTypes.length;i++){
        $scope.quesTypes[i].isOpen  =false;
        $scope.quesTypes[i].icon  = '+';
        if($scope.qCount<$scope.quesTypes[i].count){
          $scope.quesTypes[i].count = $scope.qCount
        }
      }
    }



    $scope.topicList =['Measurement','Number','Space']
    $scope.crossSubList =['English','Grammar','Alphabet']
    $scope.themes =['Space','Measurement','Number']
    $scope.appTags =['Application','Knowledge','Evaluation']
    $scope.fetchSources =['Cloud','School cloud']

    $scope.hours =[]
    for (var hour = 0; hour <= 12; hour++) {
      hour = hour > 9 ? hour : "0" + hour;
      $scope.hours.push(hour);
    }

    $scope.minutes =[]
    for (var min = 0; min < 60; min++) {
      min = min > 9 ? min : "0" + min;
      $scope.minutes.push(min);
    }

    $scope.seconds =[]
    for (var sec = 0; sec < 60; sec++) {
      sec = sec > 9 ? sec : "0" + sec;
      $scope.seconds.push(sec);
    }

    }

  }
})

  function DialogController($scope, $mdDialog,$mdToast,$document, $state,questions,$reactive,questionDetails) {
      $scope.questions = []/*questions*/
      $scope.questionDetails = questionDetails
      $scope.quesData = [];
      $scope.quesObj = [];
      $scope.quesEachData = [];
      $scope.ansData = [];
      $scope.ansObj = [];

      $scope.questionHour = 0;
      $scope.questionMin = 0;
      $scope.questionSec = 0;
      $scope.themes = ''
      $scope.selectedType = ''

      $scope.questionTypes = [];
      $scope.qTypes = [];



      $reactive(this).attach($scope);
      let self = this
      console.log($scope.questions)
      $scope.cancel = function() {
        $mdDialog.cancel();
      };
      let QuestionArray = []

      angular.forEach(questions,function(value,key){
       var questionId ={_id : value._id} 
       QuestionArray.push(questionId)
        /*Question type*/
         if($scope.questionTypes.length==0 ){
           $scope.questionTypes.push(value.type)
         }else if($scope.questionTypes.indexOf(value.type) == -1){
            $scope.questionTypes.push(value.type)
         }
       
      })
      // Modify the striucture of question types array
      for(var i in $scope.questionTypes){
        $scope.qTypes.push({type:$scope.questionTypes[i],status:'unselected'})
      }

      $scope.showSelectedQuestionGroup = function(qType,index){

        $scope.questions = []
        for(var i in questions) {
           if(questions[i].type == qType.type) {
             $scope.questions.push(questions[i])
           }
        }
        for(var i in $scope.qTypes){
          if(i!=index){
            $scope.qTypes[i].status = 'unselected'
          }else{
            $scope.qTypes[i].status = 'selected'
          }
        }
        
        $scope.selectedType = qType.type
      }
      //$scope.selectedType = $scope.questionTypes[0]
      $scope.showSelectedQuestionGroup($scope.qTypes[0],0)

      /*Themes collection*/
      if($scope.questionDetails.themes.length == 1){
        $scope.themes=$scope.questionDetails.themes[0]
      }else{
        $scope.themes=$scope.questionDetails.themes[0]
        for(var theme = 1; theme < $scope.questionDetails.themes.length; theme++){
            $scope.themes+=' , '+$scope.questionDetails.themes[theme]
        }
      }


      console.log($scope.questionTypes)
     
      $scope.questionHour = $scope.questionDetails.time.hour > 9 ? $scope.questionDetails.time.hour : "0" + $scope.questionDetails.time.hour;
      $scope.questionMin = $scope.questionDetails.time.min > 9 ? $scope.questionDetails.time.min : "0" + $scope.questionDetails.time.min;
      $scope.questionSec = $scope.questionDetails.time.sec > 9 ? $scope.questionDetails.time.sec : "0" + $scope.questionDetails.time.sec;


      $scope.splitMatchQuestion = function(index){
        //Match the Following
          $scope.quesData=[];
          $scope.ansData=[];
          angular.forEach($scope.questions[index].option, function(value, key) {
              $scope.quesData.push({ tag: value.question.tag, desc: value.question.desc});
              $scope.ansData.push({ tag: value.answer.tag, desc: value.answer.desc});
          });
          $scope.shuffle($scope.ansData);
          $scope.quesObj[index] = $scope.quesData;
          $scope.ansObj[index] = $scope.ansData;
      };

      $scope.shuffle = function(array) {
        var m = array.length, t, i;
        while (m) {
          // Pick a remaining element…
          i = Math.floor(Math.random() * m--);
          // And swap it with the current element.
          t = array[m];
          array[m] = array[i];
          array[i] = t;
        }
        return array;
      }
      $scope.addQuestions =  function(index) {
        questionDetails.question = QuestionArray 
        Meteor.call('addQuiz',questionDetails, (error,result) => {
          if(error){
              console.log(error.invalidKeys);
          }else{
            mcqQuestions = result
            $mdDialog.cancel();
            $state.go('questionList')
          }
        })
      }
  }
