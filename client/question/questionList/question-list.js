angular.module('cordova_lms').directive('questionList', function (MyResource) {
  return {
    restrict: 'E',
    templateUrl: 'client/question/questionList/question-list.html',
    controllerAs: 'questionListCtrl',
    controller: function ($scope, $reactive,$state,$filter,$mdToast, $timeout,$interval,$meteor,$interval,$mdDialog, $document){
      $reactive(this).attach($scope);
      let today = new Date()
      var day = today.getDay()
      var currentDay = new Date(today.getFullYear(),today.getMonth(),today.getDate())
      let self = this
      self.dynamicTheme = "default"
      self.user = {}
      $scope.questionID = []
      $scope.questionList=[]
      $scope.Search="";
      $scope.status= ['All','created','published']
       /*Subscribe call to get Quiz questions*/
      self.subscribe('getQuizQuestions',
        () => {
          return [] // parameters to be passed to subscribe method
        },
        () => {
          $scope.questionList = Quiz.find().fetch();

        }
      )

      var contains = function(needle) {
          // Per spec, the way to identify NaN is that it is not equal to itself
          var findNaN = needle !== needle;
          var indexOf;

          if(!findNaN && typeof Array.prototype.indexOf === 'function') {
              indexOf = Array.prototype.indexOf;
          } else {
              indexOf = function(needle) {
                  var i = -1, index = -1;

                  for(i = 0; i < this.length; i++) {
                      var item = this[i];

                      if((findNaN && item !== item) || item === needle) {
                          index = i;
                          break;
                      }
                  }

                  return index;
              };
          }

          return indexOf.call(this, needle) > -1;
      };

      $scope.validateSearch=function(value)
      {
        console.log(value )
        // let obj_to_val = { "search_text" : value}
        // // let validationData = Modules.simpleSchema().clean(moduleData);
        // Search.simpleSchema().namedContext("SearchTextSchema").validate(obj_to_val, {
        //   modifier: false
        // });
        // let context = Search.simpleSchema().namedContext("SearchTextSchema");
        // let errorsList = context.invalidKeys();
        // let ret_val = context.invalidKeys();
        // ret_val = _.map(ret_val, function(res) {
        //   return _.extend({
        //     message: context.keyErrorMessage(res.name)
        //   }, res);
        // });
        // console.log(ret_val)

        if(value===null || value==="" || value==="undefined")
        {
          alert ("please enter value to search")
        }
        else
        {
           $scope.questionList = Quiz.find().fetch();
        }
      }
      $scope.createddateOfQuestion = function(date) 
      {
        var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return [year, month, day].join('/');
        return mydate.toDateString();
      };

      /*$scope.$watch(function() 
        { return $scope.Search; }, 
        function (newVal) 
        {
          if (newVal === "") {
           $scope.questionList = Quiz.find().fetch();
          }
        });*/


      $scope.getGroupDetails = function(groupId,data)
      {
        checkStatus = data
        index = contains.call($scope.questionID, groupId._id);
        if(index == false){
           $scope.questionID.push(groupId._id);
        }
        else if(index == true){
          if(checkStatus == true){
            var index = $scope.questionID.indexOf(groupId._id);    // <-- Not supported in <IE9
            if (index !== -1) {
                $scope.questionID.splice(index, 1);
            }
          }
        }
      }
      $scope.filterQuestion = function(status)
      {
        console.log("dfbgjhfdkjgkfdjhg"+status)
        if( status==="All" )
        {
           $scope.questionList = Quiz.find().fetch();
        }
        else if( status==="published" )
        {
            $scope.questionList =Quiz.find({"status" : status}).fetch();
           // = Quiz.find().fetch();
        }
        else if( status==="created" )
        {
           $scope.questionList =Quiz.find({"status" : "not published"}).fetch();
        }
      }

      $scope.createNewQuiz=function(){
        $state.go("createQuiz");
      }

      var last = {
      bottom: false,
      top: true,
      left: false,
      right: true
    };
  $scope.toastPosition = angular.extend({},last);
  $scope.getToastPosition = function() {
    sanitizePosition();
    return Object.keys($scope.toastPosition)
      .filter(function(pos) { return $scope.toastPosition[pos]; })
      .join(' ');
  };
  function sanitizePosition() {
    var current = $scope.toastPosition;
    if ( current.bottom && last.top ) current.top = false;
    if ( current.top && last.bottom ) current.bottom = false;
    if ( current.right && last.left ) current.left = false;
    if ( current.left && last.right ) current.right = false;
    last = angular.extend({},current);
  }

    $scope.showQuestion = function( data) {
    $mdToast.show(
      $mdToast.simple()
        .textContent(data)
        .position($scope.getToastPosition())
        .hideDelay(3000)
    );
  };

      $scope.startQuiz=function(){
        teacherId = Meteor.userId()
        if($scope.questionID.length<=0)
        {
          $scope.showQuestion('select atleast one Question')
           
            //alert("please select atleast one Published Question");
        }
        else
        {
           Meteor.call('AddToNotification',teacherId,$scope.questionID,currentDay, (error,result) => {
            if(error){
                console.log(error.invalidKeys);
            }else{
              mcqQuestions = result
              $mdDialog.cancel();
                $scope.showQuestion("Quiz has been sent to students")
              //alert()
              $state.go('questionList')
            }
          })
        }       
      }
    }
  }
})

angular.module('cordova_lms').run(function($rootScope, $location) {
    $rootScope.$on('$locationChangeSuccess', function() {
        if($rootScope.previousLocation == $location.path()) {
           $state.go("questionList");
        }
        $rootScope.previousLocation = $rootScope.actualLocation;
        $rootScope.actualLocation = $location.path();
    });
});

