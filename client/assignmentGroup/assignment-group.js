angular.module('cordova_lms').directive('assignmentGroup', function () {
  return {
    restrict: 'E',
    templateUrl: 'client/assignmentGroup/assignment-group.html',
    controllerAs: 'assignmentGroupCtrl',
    controller: function ($scope,$state,$mdToast,$reactive,$window,QuestionsFactory,$stateParams,$mdDialog, $mdMedia) {
  $scope.status = '  ';
  $scope.customFullscreen = $mdMedia('xs') || $mdMedia('sm');
        var QuestionId = $stateParams.id
//$scope.leaderId==="" || $scope.leaderId===null || $scope.leaderId==="undefined"
        if(QuestionId===""||QuestionId===null||QuestionId==="undefined")
        {
          
        }
        else
        {
          QuestionsFactory.addQuestion(QuestionId)
        }
      // controller code begins
      $reactive(this).attach($scope)
      let self = this
      $scope.questionId= QuestionId;
      $scope.checkstatus = false;
      $scope.studentNewGroup = [];
      $scope.leaderId="";
      $scope.leaderName="";
      self.saveButtonVisible=true;
      self.updateButtonVisible=false;
      $scope.studentId ="";
       $scope.assignmentId =$stateParams.id;
       console.log($scope.assignmentId);
      $scope.editStudentGroupDetailsId ="";
      $scope.groupData = []
      $scope.remainingStudents = []
      $scope.groupStudents = []
      $scope.studentdetailss = []

      self.getAllStudents = function () {
        self.subscribe('getStudent',
          () => {
            return [] // parameters to be passed to subscribe method
          },
          () => {
                 
                /*fetching student  details*/
                $scope.studentdetailss = StudentDetails.find().fetch()
                //console.log($scope.studentdetailss)
            }
          )
      } 

      self.getAllStudents()

      /*to get the student group details*/
      self.getStudentGroup = function () {
        self.subscribe('getStudentGroup',
          () => {
            return [] // parameters to be passed to subscribe method
          },
          () => {
                  this.helpers(
                  {
                        studentgroupdetails: () => 
                        {
                          //console.log(StudentGroup.find({}))
                           return StudentGroup.find({});
                        }
                  });
                }
          )
      } 
      self.getStudentGroup()


      $scope.createNewGroup=function(){
        $state.go("createAssignmentGroup");
        // $state.go(result.goTo);
      }

      /*fetching student group details*/           
      function data (){
      
        $scope.studentdetailss = StudentDetails.find().fetch()
      }

   /*   $scope.removingStudentfromListWhenEdit = ( removeStudent ){
        $scope.studentdetailss = []
         console.log("Unable to Publish Data")
       // $scope.studentdetailss = StudentDetails.find().fetch()
      }*/
           
      /*for drag and drop*/
      $scope.dragControlListeners = {
              //optional param
              containment: '#blocks'
      };

      $scope.sortableCloneOptions1 = {
        containment: '#sortable-container',
        allowDuplicates: true,
        ctrlClone: true
      };
      
      $scope.sortableCloneOptions2 = {
          containment: '#sortable-container',
          allowDuplicates: true,
          ctrlClone: true
      };

      var contains = function(needle) {
          // Per spec, the way to identify NaN is that it is not equal to itself
          var findNaN = needle !== needle;
          var indexOf;

          if(!findNaN && typeof Array.prototype.indexOf === 'function') {
              indexOf = Array.prototype.indexOf;
          } else {
              indexOf = function(needle) {
                  var i = -1, index = -1;

                  for(i = 0; i < this.length; i++) {
                      var item = this[i];

                      if((findNaN && item !== item) || item === needle) {
                          index = i;
                          break;
                      }
                  }

                  return index;
              };
          }

          return indexOf.call(this, needle) > -1;
      };

      $scope.openMenu = function($mdOpenMenu, ev) {
          console.log("error.invalidKeys");
        originatorEv = ev;
        $mdOpenMenu(ev);
      };

      $scope.deleteStudentGroup = function( groupId )
      {
         Meteor.call('deletGroup',groupId,(error,result) => 
         {
            if(error){
              console.log(error.invalidKeys);
            }
            else
            {
              if(result == 1)
              {
                $state.go('asigment')  
              }
              else
              {
                console.log("Unable to Publish Data")
              }            
            }
        })
      }
      $scope.getGroupDetails = function( groupId ,checkstatus )
      {
        if(checkstatus===false)
        {
          index = contains.call($scope.groupData, groupId._id);
          if(index == false)
          {
            $scope.groupData.push(groupId._id);
          }
        }
        else
        {
          for (var i = 0; i <$scope.groupData.length; i++) 
          {
            $scope.groupData.splice(i, 1);
          }
        }

      }
       /*Distributing the questions */
      $scope.distributeQuestion=function(data,ev)
      {
        console.log($scope.groupData);
        if($scope.groupData.length<=0)
        {
           $scope.showQuestion("please select atleast one group")
          //alert("please select atleast one group");
        }
        else if($scope.groupData.length>=2)
        {
          var addToArray=true;
          for(var i=0;i<$scope.groupData.length;i++)
          {
            var studentGrouptoEdit=StudentGroup.find({"_id" :$scope.groupData[i] }).fetch();
            for(var j=1;j<$scope.groupData.length;j++)
            {
              if(i!==j)
              {
                var studentGroup=StudentGroup.find({"_id" :$scope.groupData[j] }).fetch();
                var studentGr =studentGrouptoEdit[0].students
                var studentd =studentGroup[0].students
                for(var k=0;k<studentGr.length;k++)
                {
                  for(var m=0;m<studentd.length;m++)
                  {
                    if(studentGr[k]._id===studentd[m]._id)
                    {
                      addToArray=false
                      console.log("equal"+studentGr[k]._id +"id"+k)
                      console.log("equal dss"+studentd[m]._id+"id"+m)
                    }
                  }
                }
              }
            }  
          }
          if(addToArray==true)
          {
            let id=QuestionsFactory.questionID
            Meteor.call('publishQuestionAssignment',id,$scope.groupData, (error,result) => {
              if(error){

                console.log(error.invalidKeys);
              }
              else
              {
                if(result == 1)
                {
                  $scope.showQuestion("assignment conducted")
                  //alert("Question published");
                 // $state.go('asigment')  
                     var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'))  && $scope.customFullscreen;

    $mdDialog.show({
      locals: {
        "assignment_id":QuestionId,
       
      },
      controller: DialogController,
      templateUrl: 'client/assignmentGroup/create-assignment-dueDate-view-popup.html',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose:true,
      fullscreen: useFullScreen
    })
    .then(function(answer) {
      $scope.status = 'You said the information was "' + answer + '".';
    }, function() {
      $scope.status = 'You cancelled the dialog.';
    });



    $scope.$watch(function() {
      return $mdMedia('xs') || $mdMedia('sm');
    }, function(wantsFullScreen) {
      $scope.customFullscreen = (wantsFullScreen === true);
    });
                }
                else
                {
                  console.log("Unable to Publish Data")
                }            
              }
            })
          }
          else
          {
            $scope.showQuestion("group contains repeated members")
            //alert("group contains repeated value");
          }
        }
        else
        {
           let id=QuestionsFactory.questionID
          Meteor.call('publishQuestionAssignment',id,$scope.groupData, (error,result) => {
            if(error){
              console.log(error.invalidKeys);
            }else{
            if(result == 1){
              $scope.showQuestion("assignment conducted")
                        //alert("Question published");
                 // $state.go('asigment')  
                     var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'))  && $scope.customFullscreen;

    $mdDialog.show({
      locals: {
        "assignment_id":QuestionId,
       
      },
      controller: DialogController,
      templateUrl: 'client/assignmentGroup/create-assignment-dueDate-view-popup.html',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose:true,
      fullscreen: useFullScreen
    })
    .then(function(answer) {
      $scope.status = 'You said the information was "' + answer + '".';
    }, function() {
      $scope.status = 'You cancelled the dialog.';
    });



    $scope.$watch(function() {
      return $mdMedia('xs') || $mdMedia('sm');
    }, function(wantsFullScreen) {
      $scope.customFullscreen = (wantsFullScreen === true);
    });
              //$state.go('asigment')  

            }
            else{
              console.log("Unable to Publish Data")
            }
            
            }
          })
        }
      }

       /*Editing student group*/
      $scope.editStudentGroup = function(id) 
      {
        $scope.editStudentGroupDetailsId=id;
        self.saveButtonVisible=false;
        self.updateButtonVisible=true;
        //clearing the datas from the form
        $scope.clearData(); 
        data()
        //retrive all the values from the database
        var studentGrouptoEdit=StudentGroup.find({"_id" : id}).fetch();
        self.groupName=studentGrouptoEdit[0].groupName
        var studentOldGroup=studentGrouptoEdit[0].students
        for (var i = 0; i <studentOldGroup.length; i++) 
        {
            var studentName;
            var studentitem;
            if( studentOldGroup[i].leader === true )
            {
              studentName = $scope.studentGroupLeaderToFetchName(studentOldGroup[i]._id);
              studentitem = {_id:studentOldGroup[i]._id,username:studentName[0].username}; 
              $scope.studentId=studentOldGroup[i]._id
            }
            else
            {
              studentName = $scope.studentGroupLeaderToFetchName(studentOldGroup[i]._id);
              studentitem = {_id:studentOldGroup[i]._id,username:studentName[0].username}; 
            }
            $scope.studentNewGroup.push(studentitem);
            for (var j = 0; j <$scope.studentdetailss.length; j++) 
            {
              if( $scope.studentdetailss[j]._id===studentitem._id)
              {
                $scope.studentdetailss.splice(j, 1);
              }

            }                  
        }        
        //setting the leader as selected in radio button        
        $scope.studentNewGroup._id=$scope.studentId
        $scope.leaderId = $scope.studentId
      };

      /*fetching student leader name base on id from the student model*/
      $scope.studentGroupLeaderToFetchName = function(id) 
      {
        var studentName=StudentDetails.find({"_id" : id}).fetch();
        return studentName;
      };

       /*fetching student leader name base on id from the student model*/
      $scope.studentGroupLeaderToFetchNameAndShowInList = function(groupname) 
      {
        var studentName ;
         for (var i = 0; i <groupname.students.length; i++) 
        {
           if( groupname.students[i].leader === true )
            {
               studentName = StudentDetails.find({"_id" : groupname.students[i]._id}).fetch();
            }            
        }
        return studentName[0].username;
       
      };

      /*checking group if it is already available*/
      $scope.checkingStudentGroupNameAlreadyAvailable = function(groupname) 
      {
        var studentGroupName=StudentGroup.find({"groupname" : groupname}).fetch();
        return studentGroupName;
      };

      /*adding student name and id to the array*/
      $scope.addStudentToGroup = function( id , name ) 
      {
        var addToArray=true;
        var studentitem={Name:name,Id:id};

       if( $scope.studentNewGroup.length === 0 )
        {
          $scope.studentNewGroup.push(studentitem);
        }
        else
        {
          for (var i = 0; i <$scope.studentNewGroup.length; i++) 
          {
            if($scope.studentNewGroup[i].Id===studentitem.Id){
                addToArray=false;
            }             
          }
          if(addToArray)
          {
            $scope.studentNewGroup.push(studentitem);
          }
          else
          {
            $scope.showQuestion("already added student")
            //alert("already added student")
          }
        }              
      }  
      /*Remove student from group*/
      $scope.removeStudentFromGroup= function( id  ) 
      {
        var addFromArray = false;
        var index        = 0;
        for (var i = 0; i <$scope.studentNewGroup.length; i++) 
        {
            if($scope.studentNewGroup[i].Id===id){
              addFromArray=true;
              index=i;
            }             
        }
        if(addFromArray)
          {
            $scope.studentNewGroup.splice(index, 1);
            $scope.leaderId=null;
            $scope.studentId=null;

          }
      }
      /*Save the group*/
      $scope.saveGroup = function()
      {        
        //checking group name is already available
        var studentGroupName = $scope.checkingStudentGroupNameAlreadyAvailable(self.groupName)

        //validation
        if($scope.studentNewGroup.length<=0)
        {
           $scope.showQuestion("Please Enter atleast on student")
          //alert("Please Enter atleast on student")
        }
        else if($scope.leaderId==="" || $scope.leaderId===null || $scope.leaderId==="undefined")
        {
          $scope.showQuestion("Please select Leader")
          //alert("Please select Leader")
        }
        else if(self.groupName==="" || self.groupName===null || self.groupName==="undefined")
        {
          //alert("Please add Group Name")
          $scope.showQuestion("Please add Group Name")
        }
        else if(studentGroupName.length>0)
        {
          $scope.showQuestion("Group Name is already available")
         // alert("Group Name is already available")
        }
        else
        {
          var studentmemberid=""
          var obj=[];
          var bleaderflag=false;
          for (var i = 0; i <$scope.studentNewGroup.length; i++) 
          {
            if($scope.studentNewGroup._id===$scope.studentNewGroup[i]._id)
            {
              bleaderflag=true;
            }
            else
            {
              bleaderflag=false;
            }
            var studentitem={"_id":$scope.studentNewGroup[i]._id,"leader":bleaderflag};
            obj.push(studentitem) 
                    
          }        
          $scope.studentGroupInformation = 
          {         
             "groupName": self.groupName,
             "teacherId" : Meteor.userId(),
              "active" : "l",
             "students" : obj                           
          }
         
          /*inserting group in server*/
          Meteor.call('registerGroupStudentsDetails', $scope.studentGroupInformation, function(error, result) 
          {
            if(error)
            {
              console.log(error);
            }else
            {
               $scope.showToast("Data Succssfully Added")
              $scope.clearData();   
              $scope.$apply()
            }
          });                
        }
      } 

      /*checking the leader is selected*/     
      $scope.checkTest = function(id,name)
      {
        console.log("kdjfkhdsf");
        $scope.leaderId=id;
        $scope.leaderName=name;
        $scope.studentId=id;      
      }
      /*clearing the data*/     
      $scope.clearData = function()
      {
        $scope.leaderId="";
        self.groupName="";
        $scope.leaderName ="";
        $scope.studentNewGroup=[]        
      }
      /*Adding the data*/     
      $scope.AddNewData = function()
      {

        $scope.leaderId="";
        self.groupName="";
        $scope.leaderName ="";
        $scope.studentNewGroup=[]   
        self.saveButtonVisible=true;
        self.updateButtonVisible=false;    
        data()
      }



      var last = {
      bottom: false,
      top: true,
      left: false,
      right: true
    };
  $scope.toastPosition = angular.extend({},last);
  $scope.getToastPosition = function() {
    sanitizePosition();
    return Object.keys($scope.toastPosition)
      .filter(function(pos) { return $scope.toastPosition[pos]; })
      .join(' ');
  };
  function sanitizePosition() {
    var current = $scope.toastPosition;
    if ( current.bottom && last.top ) current.top = false;
    if ( current.top && last.bottom ) current.bottom = false;
    if ( current.right && last.left ) current.left = false;
    if ( current.left && last.right ) current.right = false;
    last = angular.extend({},current);
  }

    $scope.showQuestion = function( data) {
    $mdToast.show(
      $mdToast.simple()
        .textContent(data)
        .position($scope.getToastPosition())
        .hideDelay(3000)
    );
  };

      $scope.updateGroup =function()
      {
        var studentId=$scope.leaderId;
        var addFromArray = false;
        for(var k=0; k < $scope.studentNewGroup.length; k++)
        {
          if( $scope.studentNewGroup[k]._id===studentId)
          {
            addFromArray = true
          }
        }
        if(addFromArray)
        {            
          $scope.leaderId=studentId;
        }
        else
        {
          $scope.leaderId=null;
        }
        $scope.studentNewGroup._id =studentId
        if($scope.studentNewGroup._id===$scope.studentId)
        {
            studentId=$scope.studentId;
        }             
        //validation
        if($scope.studentNewGroup.length<=0)
        {
           $scope.showQuestion("Please Enter atleast on student")
          
        }
        else if($scope.leaderId==="" || $scope.leaderId===null || $scope.leaderId==="undefined")
        {
           $scope.showQuestion("Please select Leader")               
        }
        else if(self.groupName==="" || self.groupName===null || self.groupName==="undefined")
        {
          $scope.showQuestion("Please add Group Name")
        }
        else
        {          
          var studentmemberid=""
          var obj=[];
          var bleaderflag=false;
          for (var i = 0; i <$scope.studentNewGroup.length; i++) 
          {
            if($scope.studentNewGroup._id===$scope.studentNewGroup[i]._id)
            {
              bleaderflag=true;
            }
            else
            {
              bleaderflag=false;
            }
            var studentitem={"_id":$scope.studentNewGroup[i]._id,"leader":bleaderflag};
            obj.push(studentitem) 
                    
          }      
          $scope.studentGroupInformation = 
          {     
             "groupName": self.groupName,
             "teacherId" : null,
              "active" : "l",
             "students" : obj
          }
         
          /*inserting group in server*/
          Meteor.call('updateGroupStudentsDetails',$scope.editStudentGroupDetailsId,self.groupName,
                  obj,$scope.leaderId,$scope.leaderName, function(error, result) 
          {
            if(error)
            {
              console.log(error);
            }else
            {
               $scope.showToast("Updated Succssfully")
              self.saveButtonVisible=true;
              self.updateButtonVisible=false; 
              $scope.clearData();   
              $scope.$apply()             
            }
          });                
        }
      }  
    }

  }
});
// angular.module('cordova_lms').run(function($rootScope, $location) {
//     $rootScope.$on('$locationChangeSuccess', function() {
//         if($rootScope.previousLocation == $location.path()) {
//            $state.go("questionList");
//         }
//         $rootScope.previousLocation = $rootScope.actualLocation;
//         $rootScope.actualLocation = $location.path();
//     });
// });
function DialogController($scope,$state,$mdDialog,assignment_id) {
  console.log("assignment_id"+assignment_id)

  $scope.hide = function() {
    $mdDialog.hide();
  };

  $scope.cancel = function() {
    $mdDialog.cancel();
  };

  $scope.answer = function(answer) {
    $mdDialog.hide(answer);
  };
  $scope.teacherAssignmentList=function(){
            
    $mdDialog.hide();
    Meteor.call('updateDueDate',assignment_id,$scope.myDate, function(error, result) 
          {
            if(error)
            {
              console.log(error);
            }else
            {
              self.saveButtonVisible=true;
              self.updateButtonVisible=false; 
              
              $state.go('asigment');      
            }
          });        
 

        
       }
}

