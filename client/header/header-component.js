angular.module('cordova_lms').directive('headerComponent', function(MyResource) {
    return {
        restrict: 'E',
        templateUrl: 'client/header/header.html',
        controllerAs: 'headerCtrl',

        controller: function($scope, $mdSidenav, $mdDialog, $mdMedia, $log, $timeout, $rootScope, $reactive, $state, $timeout, $q, PageTitle, $mdToast) {

            $reactive(this).attach($scope);

            let self = this;
            let item = {} //for header search item
            self.showme = true
            self.CurrentPage = PageTitle;
            self.title = self.CurrentPage.setTitle();
            $scope.logo = "images/logo.svg";

            self.toggleSidenav = function(menuId) {
                $scope.$parent.lockLeft = !$scope.$parent.lockLeft
                if (!$mdMedia('gt-md'))
                    $mdSidenav(menuId).toggle()
            };

            self.getUserAcces = function() {
                self.subscribe('usersData', () => {
                    return [] // parameters to be passed to subscribe method
                }, () => {
                    userRole = Meteor.users.find().fetch()
                    if (userRole.length > 0) {
                        role = userRole[0].role
                        var studentSubjects = Meteor.users.find({_id: Meteor.userId()}).fetch()
                        if(studentSubjects.length > 0){
                          studentSubjects = studentSubjects[0].endorse_session_curriculum
                          var classId = studentSubjects[0].class_id
                          var batch = studentSubjects[0].batch_id
                          var subjects = studentSubjects[0].subjects

                          Session.setAuth("subjects", subjects)
                          Session.setAuth("classId", classId)
                          Session.setAuth("batchId", batch)
                          console.log(userRole[0].name)
                          $scope.name = userRole[0].name
                        }
                        
                        
                            // if(role == "teacher"){
                            //   self.getForumQuestionsTeacher(subjects,classId,batch)
                            // }
                            // else if(role == "student"){
                            //   self.getForumQuestionsStudent(subjects,classId,batch)
                            // }
                    }
                })
            }

           // self.getUserAcces()

                //For the logout menu  
            this.openMenu = function($mdOpenMenu, ev) {
                originatorEv = ev;
                Meteor.call("updateNotificationTracker", function(error, result) {
                    if (error) {
                        console.log("error updating notification")
                    } else {
                        console.log("success updating notification")
                    }
                });
                $mdOpenMenu(ev);
            };

            self.logoutSession = function() {
                console.log("ASdfsdf")
                Accounts.logout(function(error) {
                    if (error) {
                        console.log('error')
                    } else {
                        $state.go('login')
                    }
                })
            }

        }
    }
})
