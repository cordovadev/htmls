angular.module('cordova_lms').directive('previewAssignment', function (MyResource) {
  return {
    restrict: 'E',
    templateUrl: 'client/previewAssignment/preview-assignment.html',
    controllerAs: 'quizPreviewCtrl',
    controller: function ($scope, $reactive,$state,$filter, $timeout,$interval,$meteor,$interval,$rootScope,QuestionsFactory){
      $reactive(this).attach($scope);
      $scope.questions = QuestionsFactory.getArray();
      console.log($scope.questions)
  }
}
})