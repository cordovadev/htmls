angular.module('cordova_lms').directive('teachingPlanList', function() {
  return {
    restrict: 'E',
    templateUrl: 'client/teachingPlanList/teaching-plan-list.html',
    controllerAs: 'teachingPlanCtrl',

     controller: function ($scope, $reactive,$state,$filter,$mdToast, $timeout,$interval,$meteor,$interval,$mdDialog, $document){
      $reactive(this).attach($scope);
      let self = this
      $scope.teachingPlans=[]
      self.subscribe('getTeachingPlans',
        () => {
          return [] // parameters to be passed to subscribe method
        },
        () => {
          $scope.teachingPlans = TeachingPlans.find({"status":"verified","teacherId": Meteor.userId()}).fetch();
          console.log($scope.teachingPlans) 
        }
      )
        $scope.getTeachingPlanCrossware=function(teachingPlan){
console.log(teachingPlan)


$state.go('teacherPlanView',{"getTeachingPlan":teachingPlan._id});

    }


    }
  }
})


