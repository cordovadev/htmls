angular.module('cordova_lms').directive('sideNav', function(MyResource) {
  return {
    restrict: 'E',
    templateUrl: 'client/sidenav/sidenav.html',
    controllerAs: 'sidenavCtrl',
    controller: function($scope, $mdDialog, $log, $rootScope, $reactive, PageTitle, $state, $timeout, $mdSidenav) {


      $reactive(this).attach($scope);
      let self = this;


      $scope.toggleLeft = buildToggler('left');
      $scope.isOpenLeft = function(){
        return $mdSidenav('left').isOpen();
      };

      
      function buildToggler(navID) {
        return function() {
          // Component lookup should always be available since we are not using `ng-if`
          $mdSidenav(navID)
            .toggle()
            .then(function () {
              $log.debug("toggle " + navID + " is done");
            });
        }
      }
 

   $scope.hrzntLst=[
        {class:'button button-action button-rounded', text:'bluberry'},
        {class:'button button-highlight button-rounded', text:'Peaches'},
        {class:'button button-primary button-rounded', text:'Apples'},
        {class:'button button-caution button-rounded', text:'Strawberry'},
        {class:'button button-royal button-rounded', text:'Grapes'},
        {class:'button button-highlight button-pill', text:'Banana'},
        {class:'button button-action button-pill', text:'Mango'},
        {class:'button button-caution button-pill', text:'Cherries'},
        {class:'button button-royal button-pill', text:'Rubarb'},
        {class:'button button-3d button-action button-rounded', text:'Guava'},
        {class:'button button-3d button-highlight button-rounded', text:'Pineapple'},
        {class:'button button-3d button-primary button-rounded', text:'Pear'},
        {class:'button button-3d button-caution button-rounded', text:'Cranberry'},
        {class:'button button-3d button-royal button-rounded', text:'Eggplants'}
      ];

      



    
    }
  }
});



 
  angular.module('cordova_lms').controller('LeftCtrl', function ($scope, $timeout, $mdSidenav, $log) {
    $scope.close = function () {
      // Component lookup should always be available since we are not using `ng-if`
      $mdSidenav('left').close()
        .then(function () {
          $log.debug("close LEFT is done");
        });

    };
  })