
angular.module('cordova_lms').directive('studentGraph', function () {
  return {
    restrict: 'E',
    templateUrl: 'client/studenGraph/studentgraph.html',
    controllerAs: 'questionListCtrl',
     scope: {
      graphInfo: '=info'
    },
       
    controller: function ($scope, $reactive,$state,$filter,$mdToast,chartFactory, $timeout,$interval,$meteor,$interval,$mdDialog, $document)
    {   
      	$reactive(this).attach($scope);
          let self = this
        $scope.selectedCharts='ColumnChart';
        $scope.myCharts =[ 'PieChart','BarChart','ColumnChart','LineChart']
 		$scope.myChartObject = {};

        this.autorun(() => {
            console.log($scope.getReactively("graphInfo"))
             $scope.myChartObject.data= $scope.graphInfo
        })
      
        // console.log(graphInfo);
        $scope.studentid="vdYqCobR6Tpw7FiRK";
         $scope.studentGroupInformation ="";
    	$scope.myChartObject.type = $scope.selectedCharts;
        $scope.ondatachange =function( data )
        {      
           $scope.myChartObject.type =data
        }

    	$scope.myChartObject.options = {
          chart: {
            title: 'Company Performance',
            subtitle: 'Sales, Expenses, and Profit: 2014-2017',
          },
          bars: 'horizontal' // Required for Material Bar Charts.
        };
    }
  }		
})

        // $scope.showGraphData = function()
        // {
        //     $scope.myChartObject.data= {}
        //     Meteor.call('getStudentTarget', $scope.studentid, (error,result) => {
        //     if(error){
        //       console.log(error.invalidKeys);
        //     }
        //     else
        //     {                      
        //         var orginaldata=[];       
        //         for(var j=0;j<result.length;j++)
        //         {
        //           for( var i=0;i<result[j].teacher_target.length;i++)
        //             {
        //                  var data =[];  
        //                // console.log(result[j].teacher_target[i].subject_name)
        //                 var item={v:result[j].teacher_target[i].subject_name};                        
        //                 data.push(item)
        //                  var item={v:result[j].teacher_target[i].target_value};                        
        //                 data.push(item)
        //                 var item={v:result[j].student_target[i].target_value};                        
        //                 data.push(item)
        //                 var item={v:result[j].marks_scored[i].mark_scored};                        
        //                 data.push(item)
        //                 var value={c:data}
        //                 orginaldata.push(value)                      
        //             }  
                   
        //         }

           
        //         $scope.studentGroupInformation = 
        //         {         
        //              "cols": 
        //             [
        //                 {id: 'A', label: 'Subjects', type: 'string'},
        //                 {id: 'B', label: 'Student Achived', type: 'number'},
        //                 {id: 'C', label: 'Teacher Target', type: 'number'},
        //                 {id: 'D', label: 'Student Target', type: 'number'}
        //             ],
        //             "rows": 
                    
        //                 orginaldata
                                              
        //           }
        //           $scope.myChartObject.data=$scope.studentGroupInformation                
        //     }
        //   })
        // }


        // function showGraph()
        // {
        //    $scope.myChartObject.data= 
        //    {
        //     "cols": 
        //     [
        //         {id: 'A', label: 'Subjects', type: 'string'},
        //         {id: 'B', label: 'Student Achived', type: 'number'},
        //         {id: 'C', label: 'Teacher Target', type: 'number'},
        //         {id: 'D', label: 'Student Target', type: 'number'}
        //     ],
        //     "rows": 
        //     [
        //         {c:[{v: 'chemistry'},
        //          {v: 100},
        //          {v: 60},
        //          {v: 50}
        //         ]},
        //         {c:[{v: 'physics'},
        //          {v: 20},
        //          {v: 10},
        //          {v: 50}
        //         ]},
        //         {c:[{v: 'maths'},
        //          {v: 30},
        //          {v: 60},
        //          {v: 50}
        //         ]},
        //          {c:[{v: 'English'},
        //          {v: 30},
        //          {v: 10},
        //          {v: 50}
        //         ]},
        //          {c:[{v: 'Tamil'},
        //          {v: 80},
        //          {v: 50},
        //          {v: 50}
        //         ]},
        //         {c:[{v: 'Social science'},
        //          {v: 80},
        //          {v: 90},
        //          {v: 50}
        //         ]},
        //          {c:[{v: 'Social science'},
        //          {v: 80},
        //          {v: 90},
        //          {v: 50}
        //         ]},
        //          {c:[{v: 'Social science'},
        //          {v: 80},
        //          {v: 90},
        //          {v: 50}
        //         ]}
        //     ]
        //   }
        // }