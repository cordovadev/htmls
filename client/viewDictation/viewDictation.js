angular.module('cordova_lms').directive('viewDictation', function() {
    return {
        restrict: 'E',
        templateUrl: 'client/viewDictation/viewDictation.html',
        controllerAs: 'directiveCtrl',
        controller: function($scope, $mdToast, $reactive, $window, $stateParams, $interval, $state) {

            $reactive(this).attach($scope)
            let self = this
            $scope.active = true;
            $scope.active1 = true;
            let studentId = Meteor.userId();
            $scope.counter = [];
            $scope.Result = "";
           $scope.countdown = []
            $scope.studentId = Meteor.userId();
            $scope.getstudentDetails = []
            $scope.initialCountdown=[]

            $scope.getstudentDetailsData = function() {
                self.subscribe('getStudentData', () => {
                    return [$scope.studentId] // parameters to be passed to subscribe method
                }, () => {
                    console.log(StudentDetails.find({
                        userId: $scope.studentId
                    }).fetch())
                    $scope.getstudentDetails = StudentDetails.find({
                        userId: $scope.studentId
                    }).fetch()[0];
                    self.subscribe('checkDictionNotification', () => {
                        return [studentId] // parameters to be passed to subscribe method  getQuiz
                    }, () => {
                        let moduleDetails = Quiz.find({
                                "type": "Dictation"
                            }).fetch()
                            // console.log(moduleDetails)
                        $scope.quesTypes = moduleDetails
                        $scope.timeremaining = moduleDetails.Duration
                    })

                     self.subscribe('getQuiz', () => {
                        return [] // parameters to be passed to subscribe method  getQuiz
                    }, () => {
                        let moduleDetail = Quiz.find({
                                "type": "Dictation"
                            }).fetch()
                            // console.log(moduleDetails)
                       console.log(moduleDetail)
                        
                    })

                })

                console.log($scope.quesTypes)
            };

            $scope.ListenDictation = function(word, countdowntime, index) {
                var u = new SpeechSynthesisUtterance();
                u.text = word;
                speechSynthesis.speak(u);
                countdown("countdown", 0, countdowntime, index);
                // $scope.countController(countdowntime)
            }


            $scope.SaveDictation = function(id, word) {
                let studentid;
                $scope.getstudentDetails = StudentDetails.find({
                    userId: $scope.studentId
                }).fetch()
                for (var i = 0; i < $scope.getstudentDetails.length; i++) {
                    studentid = $scope.getstudentDetails[i]._id;
                }
                if (word === "" || word === null || word === undefined) {
                    $scope.showToast("Please Enter the word")
                } else {
                   // $interval.cancel(intervalId);
                    Meteor.call('StudentsubmitDiction', studentid, id, word, $scope.counter, (error, result) => {
                        if (error) {
                            $scope.showToast("Diction not submitted")
                        } else {
                            $scope.clearData();
                            $scope.$apply()
                            $scope.showToast("Diction submitted")

                        }
                    })
                }
            }
            $scope.clearData = function() {
                $scope.Result = "";
            }

            $scope.timer = function(index) {
                var startTime = new Date();
                intervalId = $interval(function() {
                    var actualTime = new Date();

                    $scope.counter[index] = Math.floor((actualTime - startTime) / 1000);
                    $scope.countdown[index] = $scope.initialCountdown[index] - $scope.counter[index];
                    console.log($scope.countdown)
                    if($scope.countdown[index]===0)
                    {
                         $interval.cancel(intervalId);
                    }
                }, 1000);
            };

            function countdown(elementName, minutes, seconds, index) {
                //var element, endTime, hours, mins, msLeft, time;
                $scope.initialCountdown[index] = seconds;
                $scope.timer(index);
            }


            var last = {
                bottom: false,
                top: true,
                left: false,
                right: true
            };

            $scope.collapseAll = function(data) {

                data.expanded = !data.expanded;                
                //$interval.cancel(intervalId);
            };


            $scope.toastPosition = angular.extend({}, last);
            $scope.getToastPosition = function() {
                sanitizePosition();
                return Object.keys($scope.toastPosition)
                    .filter(function(pos) {
                        return $scope.toastPosition[pos];
                    })
                    .join(' ');
            };

            function sanitizePosition() {
                var current = $scope.toastPosition;
                if (current.bottom && last.top) current.top = false;
                if (current.top && last.bottom) current.bottom = false;
                if (current.right && last.left) current.left = false;
                if (current.left && last.right) current.right = false;
                last = angular.extend({}, current);
            }

            $scope.showToast = function(data) {
                $mdToast.show(
                    $mdToast.simple()
                    .textContent(data)
                    .position($scope.getToastPosition())
                    .hideDelay(3000)
                );
            };

            $scope.countController = function(time) {
                $scope.countDown = time;
                var timer = setInterval(function() {
                    $scope.countDown--;
                    $scope.$apply();
                    console.log($scope.countDown);
                }, 1000);
            }



        }
    }
});

angular.module('cordova_lms').filter('secondsToDateTime', [function() {
    return function(seconds) {
        return new Date(1970, 0, 1).setSeconds(seconds);
    };
}])