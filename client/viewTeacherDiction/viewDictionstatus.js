angular.module('cordova_lms').directive('viewDictionstatus', function() {
    return {
        restrict: 'E',
        templateUrl: 'client/viewTeacherDiction/viewDictionstatus.html',
        controllerAs: 'directiveCtrl',

        controller: function($scope, $mdToast, $reactive, $window, $mdMedia, $stateParams, $state) {

            $reactive(this).attach($scope);
            $scope.status = '  ';
            $scope.customFullscreen = $mdMedia('xs') || $mdMedia('sm');

            let self = this
            self.dynamicTheme = "default"
            self.user = {}
            let studentId = Meteor.userId();
            console.log($scope.questions)
            $scope.getstudentGroupArray = [];
            $scope.getstudentDetails = [];
            $scope.quesTypes = [];
            $scope.studentDictation =[]
            $scope.studentArray=[]

            $scope.getAllStudentGroup = function() {
                self.subscribe('getStudentGroup', () => {
                    return [] // parameters to be passed to subscribe method
                }, () => {
                    console.log(StudentGroup.find().fetch())
                    $scope.getstudentGroupArray = StudentGroup.find().fetch();
                    $scope.getAllStudent();
                    $scope.getAllDictation();

                })
            }

             $scope.getAllDictation = function() {
                self.subscribe('getQuizDictation', () => {
                    return [] // parameters to be passed to subscribe method
                }, () => {
                    
                       
                        	console.log("wwwwwwwwwwwwwwwww"+Quiz.find({
                                "teacherId": Meteor.userId(),
                                "type": "Dictation",
                                "status" : "Conducted"
                            }))
                             $scope.quesTypes = Quiz.find({
                                "teacherId": Meteor.userId(),
                                "type": "Dictation",
                                "status" : "Conducted"
                            }).fetch();


                    
                    // $scope.quesTypes = $stateParams.getAssginment;

                    console.log("sadaaaaaaaaa"+$scope.quesTypes)
                                                 
                  
                })
            }

            
            $scope.getAllStudent = function() {
                self.subscribe('getStudent', () => {
                    return [] // parameters to be passed to subscribe method
                }, () => {
                    console.log(StudentDetails.find({
                        "teacherId": Meteor.userId()
                    }).fetch())
                    $scope.getstudentDetails = StudentDetails.find({
                        "teacherId": Meteor.userId()
                    }).fetch();

                })
            }
        


            $scope.getstudentGroup = function(group_id) {
                console.log(group_id)
                var getstudentGroupArray1 = StudentGroup.find({
                    "_id": group_id
                }).fetch()
                console.log(getstudentGroupArray1[0].students)

                return getstudentGroupArray1[0].groupName
            }

            $scope.collapseAll = function(data, group_id) {
                $scope.getstudentDetails1 = [];
                var getstudentGroupArray1 = StudentGroup.find({
                    "_id": group_id
                }).fetch()
                $scope.getstudentDetails = getstudentGroupArray1[0].students
                angular.forEach($scope.getstudentDetails, function(value, key) {
                    console.log(StudentDetails.find({
                        "_id": value._id
                    }).fetch())
                    studentname = StudentDetails.find({
                        "_id": value._id
                    }).fetch()[0].username
                    studentUserId = StudentDetails.find({
                        "_id": value._id
                    }).fetch()[0].userId
                    studentObj = {
                        name: studentname,
                        _id: studentUserId
                    }
                    $scope.getstudentDetails1.push(studentObj);
                })
                $scope.studentArray = $scope.getstudentDetails1

                //    for(var i in  $scope.getAssginment_id) {
                //      if( $scope.getAssginment_id[i] != data) {
                //         $scope.getAssginment_id[i].expanded = false;   
                //      }
                //    }

                data.expanded = !data.expanded;
            };



        }
    }
});