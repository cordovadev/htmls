angular.module('cordova_lms').directive('createGroup', function () {
  return {
    restrict: 'E',
    templateUrl: 'client/createGroup/createGroup.html',
    controllerAs: 'directiveCtrl',
    controller: function ($scope,$mdToast, $reactive,$window,$stateParams,$state) 
    {
        var userId = $stateParams.id
      // controller code begins
      $reactive(this).attach($scope)
      let self = this
      $scope.groupName="";
      $scope.checkstatus = false;
      $scope.studentNewGroup = [];
      $scope.leaderId="";
      $scope.leaderName="";
      self.saveButtonVisible=true;
      self.updateButtonVisible=false;
      $scope.studentId ="";

      $scope.editStudentGroupDetailsId ="";
      $scope.groupData = []
      $scope.remainingStudents = []
      $scope.groupStudents = []
      $scope.studentdetailss = []




      self.getAllStudents = function () {
        self.subscribe('getStudent',
          () => {
            return [] // parameters to be passed to subscribe method
          },
          () => {
                 
                /*fetching student  details*/
                $scope.studentdetailss = StudentDetails.find().fetch()
            }
          )
      } 

      self.getAllStudents()

      /*to get the student group details*/
      self.getStudentGroup = function () {
        self.subscribe('getStudentGroup',
          () => {
            return [] // parameters to be passed to subscribe method
          },
          () => {
                  this.helpers(
                  {
                        studentgroupdetails: () => 
                        {
                          
                           return StudentGroup.find({});
                        }
                  });
                  getuserDetailstoupdate()
                }
          )
      } 
      self.getStudentGroup()

      /*fetching student group details*/           
      function data (){
      
        $scope.studentdetailss = StudentDetails.find().fetch()
      }

function getuserDetailstoupdate()
{
    if(userId!=="" && userId!==null && userId!=="undefined")
      {
        $scope.editStudentGroupDetailsId=userId;
        self.saveButtonVisible=false;
        self.updateButtonVisible=true;
        //clearing the datas from the form
        //$scope.clearData(); 
        data()
        var studentGrouptoEdit=[];
         Meteor.call('getStudentGroupDetailsToEdit',userId,(error,result) => 
         {
            if(error){
              console.log(error.invalidKeys);
            }
            else
            {
                 studentGrouptoEdit =    result;
                 $scope.groupName=studentGrouptoEdit[0].groupName
        var studentOldGroup=studentGrouptoEdit[0].students
        for (var i = 0; i <studentOldGroup.length; i++) 
        {
            var studentName;
            var studentitem;
            if( studentOldGroup[i].leader === true )
            {
              studentName = $scope.studentGroupLeaderToFetchName(studentOldGroup[i]._id);
              studentitem = {_id:studentOldGroup[i]._id,username:studentName[0].username}; 
              $scope.studentId=studentOldGroup[i]._id
            }
            else
            {
              studentName = $scope.studentGroupLeaderToFetchName(studentOldGroup[i]._id);
              studentitem = {_id:studentOldGroup[i]._id,username:studentName[0].username}; 
            }
            $scope.studentNewGroup.push(studentitem);
            for (var j = 0; j <$scope.studentdetailss.length; j++) 
            {
              if( $scope.studentdetailss[j]._id===studentitem._id)
              {
                $scope.studentdetailss.splice(j, 1);
              }

            }                  
        }        
        //setting the leader as selected in radio button        
        $scope.studentNewGroup._id=$scope.studentId
        $scope.leaderId = $scope.studentId

            }
        })

        //retrive all the values from the database
        //var studentGrouptoEdit=StudentGroup.find({"_id" : userId}).fetch();
        
      }
      else
      {
      self.saveButtonVisible=true;
      self.updateButtonVisible=false;
      }
    }

   /*   $scope.removingStudentfromListWhenEdit = ( removeStudent ){
        $scope.studentdetailss = []
         console.log("Unable to Publish Data")
       // $scope.studentdetailss = StudentDetails.find().fetch()
      }*/
           
      /*for drag and drop*/
      $scope.dragControlListeners = {
              //optional param
              containment: '#blocks'
      };

      $scope.sortableCloneOptions1 = {
        containment: '#sortable-container',
        allowDuplicates: true,
        ctrlClone: true
      };
      
      $scope.sortableCloneOptions2 = {
          containment: '#sortable-container',
          allowDuplicates: true,
          ctrlClone: true
      };

      var contains = function(needle) {
          // Per spec, the way to identify NaN is that it is not equal to itself
          var findNaN = needle !== needle;
          var indexOf;

          if(!findNaN && typeof Array.prototype.indexOf === 'function') {
              indexOf = Array.prototype.indexOf;
          } else {
              indexOf = function(needle) {
                  var i = -1, index = -1;

                  for(i = 0; i < this.length; i++) {
                      var item = this[i];

                      if((findNaN && item !== item) || item === needle) {
                          index = i;
                          break;
                      }
                  }

                  return index;
              };
          }

          return indexOf.call(this, needle) > -1;
      };

      $scope.openMenu = function($mdOpenMenu, ev) {
          console.log("error.invalidKeys");
        originatorEv = ev;
        $mdOpenMenu(ev);
      };

      $scope.deleteStudentGroup = function( groupId )
      {
         Meteor.call('deletGroup',groupId,(error,result) => 
         {
            if(error){
              console.log(error.invalidKeys);
            }
            else
            {
              if(result == 1)
              {
                $state.go('questionList')  
              }
              else
              {
                console.log("Unable to Publish Data")
              }            
            }
        })
      }
      $scope.getGroupDetails = function( groupId ,checkstatus )
      {
        if(checkstatus===false)
        {
          index = contains.call($scope.groupData, groupId._id);
          if(index == false)
          {
            $scope.groupData.push(groupId._id);
          }
        }
        else
        {
          for (var i = 0; i <$scope.groupData.length; i++) 
          {
            $scope.groupData.splice(i, 1);
          }
        }

      }
       /*Distributing the questions */
      $scope.distributeQuestion=function(data)
      {
        if($scope.groupData.length<=0)
        {
          $scope.showToast("please select atleast one group");
        }
        else if($scope.groupData.length>=2)
        {
          var addToArray=true;
          for(var i=0;i<$scope.groupData.length;i++)
          {
            var studentGrouptoEdit=StudentGroup.find({"_id" :$scope.groupData[i] }).fetch();
            for(var j=1;j<$scope.groupData.length;j++)
            {
              if(i!==j)
              {
                var studentGroup=StudentGroup.find({"_id" :$scope.groupData[j] }).fetch();
                var studentGr =studentGrouptoEdit[0].students
                var studentd =studentGroup[0].students
                for(var k=0;k<studentGr.length;k++)
                {
                  for(var m=0;m<studentd.length;m++)
                  {
                    if(studentGr[k]._id===studentd[m]._id)
                    {
                      addToArray=false
                      console.log("equal"+studentGr[k]._id +"id"+k)
                      console.log("equal dss"+studentd[m]._id+"id"+m)
                    }
                  }
                }
              }
            }  
          }
          if(addToArray==true)
          {
            Meteor.call('publishQuestion',QuestionId,$scope.groupData, (error,result) => {
              if(error){

                console.log(error.invalidKeys);
              }
              else
              {
                if(result == 1)
                {
                  $state.go('questionList')  
                }
                else
                {
                  console.log("Unable to Publish Data")
                }            
              }
            })
          }
          else
          {
            $scope.showToast("group contains repeated value");
          }
        }
        else
        {
          Meteor.call('publishQuestion',QuestionId,$scope.groupData, (error,result) => {
            if(error){
              console.log(error.invalidKeys);
            }else{
            if(result == 1){
              $state.go('questionList')  
            }
            else{
              console.log("Unable to Publish Data")
            }
            
            }
          })
        }
      }

       /*Editing student group*/
      $scope.editStudentGroup = function(id) 
      {
        $scope.editStudentGroupDetailsId=id;
        self.saveButtonVisible=false;
        self.updateButtonVisible=true;
        //clearing the datas from the form
        $scope.clearData(); 
        data()
        //retrive all the values from the database
        var studentGrouptoEdit=StudentGroup.find({"_id" : id}).fetch();
        self.groupName=studentGrouptoEdit[0].groupName
        var studentOldGroup=studentGrouptoEdit[0].students
        for (var i = 0; i <studentOldGroup.length; i++) 
        {
            var studentName;
            var studentitem;
            if( studentOldGroup[i].leader === true )
            {
              studentName = $scope.studentGroupLeaderToFetchName(studentOldGroup[i]._id);
              studentitem = {_id:studentOldGroup[i]._id,username:studentName[0].username}; 
              $scope.studentId=studentOldGroup[i]._id
            }
            else
            {
              studentName = $scope.studentGroupLeaderToFetchName(studentOldGroup[i]._id);
              studentitem = {_id:studentOldGroup[i]._id,username:studentName[0].username}; 
            }
            $scope.studentNewGroup.push(studentitem);
            for (var j = 0; j <$scope.studentdetailss.length; j++) 
            {
              if( $scope.studentdetailss[j]._id===studentitem._id)
              {
                $scope.studentdetailss.splice(j, 1);
              }

            }                  
        }        
        //setting the leader as selected in radio button        
        $scope.studentNewGroup._id=$scope.studentId
        $scope.leaderId = $scope.studentId
      };

      /*fetching student leader name base on id from the student model*/
      $scope.studentGroupLeaderToFetchName = function(id) 
      {
        var studentName=StudentDetails.find({"_id" : id}).fetch();
        return studentName;
      };

       /*fetching student leader name base on id from the student model*/
      $scope.studentGroupLeaderToFetchNameAndShowInList = function(groupname) 
      {
        var studentName ;
         for (var i = 0; i <groupname.students.length; i++) 
        {
           if( groupname.students[i].leader === true )
            {
               studentName = StudentDetails.find({"_id" : groupname.students[i]._id}).fetch();
            }            
        }
        return studentName[0].username;
       
      };

      /*checking group if it is already available*/
      $scope.checkingStudentGroupNameAlreadyAvailable = function(groupname) 
      {
        var studentGroupName=StudentGroup.find({"groupName" : groupname}).fetch();
        return studentGroupName;
      };

      /*adding student name and id to the array*/
      $scope.addStudentToGroup = function( id , name ) 
      {
        var addToArray=true;
        var studentitem={Name:name,Id:id};

       if( $scope.studentNewGroup.length === 0 )
        {
          $scope.studentNewGroup.push(studentitem);
        }
        else
        {
          for (var i = 0; i <$scope.studentNewGroup.length; i++) 
          {
            if($scope.studentNewGroup[i].Id===studentitem.Id){
                addToArray=false;
            }             
          }
          if(addToArray)
          {
            $scope.studentNewGroup.push(studentitem);
          }
          else
          {
            $scope.showToast("already added student")
          }
        }              
      }  
      /*Remove student from group*/
      $scope.removeStudentFromGroup= function( id  ) 
      {
        var addFromArray = false;
        var index        = 0;
        for (var i = 0; i <$scope.studentNewGroup.length; i++) 
        {
            if($scope.studentNewGroup[i].Id===id){
              addFromArray=true;
              index=i;
            }             
        }
        if(addFromArray)
          {
            $scope.studentNewGroup.splice(index, 1);
            $scope.leaderId=null;
            $scope.studentId=null;

          }
      }
      /*Save the group*/
      $scope.saveGroup = function()
      {        
        //checking group name is already available
        var studentGroupName = $scope.checkingStudentGroupNameAlreadyAvailable($scope.groupName)

        //validation
        if($scope.studentNewGroup.length<=0)
        {
          $scope.showToast("Please Enter atleast on student")
        }
        else if($scope.leaderId==="" || $scope.leaderId===null || $scope.leaderId==="undefined")
        {
          $scope.showToast("Please select Leader")
        }
        else if($scope.groupName==="" || $scope.groupName===null || $scope.groupName==="undefined")
        {
          $scope.showToast("Please add Group Name")
        }
        else if(studentGroupName.length>0)
        {
          $scope.showToast("Group Name is already available")
        }
        else
        {
          var studentmemberid=""
          var obj=[];
          var bleaderflag=false;
          for (var i = 0; i <$scope.studentNewGroup.length; i++) 
          {
            if($scope.studentNewGroup._id===$scope.studentNewGroup[i]._id)
            {
              bleaderflag=true;
            }
            else
            {
              bleaderflag=false;
            }
            var studentitem={"_id":$scope.studentNewGroup[i]._id,"leader":bleaderflag};
            obj.push(studentitem) 
                    
          }        
          $scope.studentGroupInformation = 
          {         
             "groupName": $scope.groupName,
             "teacherId" : Meteor.userId(),
              "active" : "l",
             "students" : obj                           
          }
         
          /*inserting group in server*/
          Meteor.call('registerGroupStudentsDetails', $scope.studentGroupInformation, function(error, result) 
          {
            if(error)
            {
              console.log(error);
            }else
            {

              $scope.clearData();   
              $scope.$apply()
              $scope.showToast("Data Succssfully Added")
              $state.go("groupList");
            }
          });                
        }
      } 

      /*checking the leader is selected*/     
      $scope.checkTest = function(id,name)
      {
        console.log("kdjfkhdsf");
        $scope.leaderId=id;
        $scope.leaderName=name;
        $scope.studentId=id;      
      }
      /*clearing the data*/     
      $scope.clearData = function()
      {
        $scope.leaderId="";
        self.groupName="";
        $scope.leaderName ="";
        $scope.studentNewGroup=[]        
      }
      /*Adding the data*/     
      $scope.AddNewData = function()
      {

        $scope.leaderId="";
        self.groupName="";
        $scope.leaderName ="";
        $scope.studentNewGroup=[]   
        self.saveButtonVisible=true;
        self.updateButtonVisible=false;    
        data()
      }


      var last = {
        bottom: false,
        top: true,
        left: false,
        right: true
      };
      $scope.toastPosition = angular.extend({},last);
      $scope.getToastPosition = function() {
        sanitizePosition();
        return Object.keys($scope.toastPosition)
          .filter(function(pos) { return $scope.toastPosition[pos]; })
          .join(' ');
      };
      function sanitizePosition() {
        var current = $scope.toastPosition;
        if ( current.bottom && last.top ) current.top = false;
        if ( current.top && last.bottom ) current.bottom = false;
        if ( current.right && last.left ) current.left = false;
        if ( current.left && last.right ) current.right = false;
        last = angular.extend({},current);
      }

      $scope.showToast = function( data) {
        $mdToast.show(
          $mdToast.simple()
            .textContent(data)
            .position($scope.getToastPosition())
            .hideDelay(3000)
        );
      };

      $scope.updateGroup =function()
      {
        var studentId=$scope.leaderId;
        var addFromArray = false;
        for(var k=0; k < $scope.studentNewGroup.length; k++)
        {
          if( $scope.studentNewGroup[k]._id===studentId)
          {
            addFromArray = true
          }
        }
        if(addFromArray)
        {            
          $scope.leaderId=studentId;
        }
        else
        {
          $scope.leaderId=null;
        }
        $scope.studentNewGroup._id =studentId
        if($scope.studentNewGroup._id===$scope.studentId)
        {
            studentId=$scope.studentId;
        }             
        //validation
        if($scope.studentNewGroup.length<=0)
        {
          $scope.showToast("Please Enter atleast on student")
        }
        else if($scope.leaderId==="" || $scope.leaderId===null || $scope.leaderId==="undefined")
        {
               $scope.showToast("Please select Leader")
        }
        else if($scope.groupName==="" || $scope.groupName===null || $scope.groupName==="undefined")
        {
          $scope.showToast("Please add Group Name")
        }
        else
        {          
          var studentmemberid=""
          var obj=[];
          var bleaderflag=false;
          for (var i = 0; i <$scope.studentNewGroup.length; i++) 
          {
            if($scope.studentNewGroup._id===$scope.studentNewGroup[i]._id)
            {
              bleaderflag=true;
            }
            else
            {
              bleaderflag=false;
            }
            var studentitem={"_id":$scope.studentNewGroup[i]._id,"leader":bleaderflag};
            obj.push(studentitem) 
                    
          }      
          $scope.studentGroupInformation = 
          {     
             "groupName": self.groupName,
             "teacherId" : null,
              "active" : "l",
             "students" : obj
          }
         
          /*inserting group in server*/
          Meteor.call('updateGroupStudentsDetails',$scope.editStudentGroupDetailsId,$scope.groupName,
                  obj,$scope.leaderId,$scope.leaderName, function(error, result) 
          {
            if(error)
            {
              console.log(error);
            }else
            {
              self.saveButtonVisible=true;
              self.updateButtonVisible=false; 
              $scope.clearData();   
              $scope.$apply()  
              $scope.showToast("Updated Succssfully")
              $state.go("groupList");           
            }
          });                
        }
      }  
    }
  }
});

// angular.module('cordova_lms').run(function($rootScope, $location) {
//     $rootScope.$on('$locationChangeSuccess', function() {
//         if($rootScope.previousLocation == $location.path()) {
//            $state.go("groupList");
//         }
//         $rootScope.previousLocation = $rootScope.actualLocation;
//         $rootScope.actualLocation = $location.path();
//     });
// });
