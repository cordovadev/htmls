angular.module('cordova_lms').directive('pdfViewer', function(MyResource,$window) {
    return {
        restrict: 'E',
        templateUrl: 'client/pdf/pdfViewer.html',
        controllerAs: 'pdfViewerCtrl',
        scope: {
            toolName: '='
        },
        link: function(scope, element, attrs){
              
              console.log('element : '+element);
              canvas = element.find('canvas')[0];
              
              console.log('canvas : '+canvas);
              if(canvas) {
                var ctx = canvas.getContext('2d'); 
              }
              
              scope.normalHeight = $window.innerHeight
              
            /*var w = angular.element($window);
            scope.getWindowDimensions = function () {

                return { 'h': w.height(), 'w': w.width() };
            };
            scope.$watch(scope.getWindowDimensions, function (newValue, oldValue) {
                //scope.windowHeight = newValue.h;
                //scope.windowWidth = newValue.w;
                scope.getPdfHeight()
                //console.log('canvas.height : '+newValue.h)
                scope.style = function () {
                    return { 
                        'height': (newValue.h - 100) + 'px',
                        'width': (newValue.w - 100) + 'px' 
                    };
                };

            }, true);

            w.bind('resize', function () {
                scope.$apply();
                //scope.resetDrawing()
            });*/
             
              let drawing = false;
              let lastX;
              let lastY;
              ctx.lineJoin = ctx.lineCap = 'round';
              let points = [],fhPoints = [];
              let rect;

              scope.setTool = function(tool){
                scope.toolName = tool;
              };
              scope.getPdfHeight = function(){
                pdfHolder = element.find('pdfviewer')[0];
                
                var ratio = scope.backingScale()
                var w = pdfHolder.firstChild.clientWidth
                var h = pdfHolder.firstChild.clientHeight
                scope.windowWidth = Math.floor(w * ratio);
                scope.windowHeight = Math.floor(h * ratio);
                scope.windowInnerWidth = Math.floor(w) + 'px';
                scope.windowInnerHeight = Math.floor(h) + 'px';

                console.log('ratio : '+ratio)
                console.log('scope.windowHeight : '+scope.windowHeight)
                console.log('scope.windowHeight : '+scope.windowWidth)
                console.log('scope.windowInnerWidth : '+scope.windowInnerWidth)
                console.log('scope.windowInnerHeight : '+scope.windowInnerHeight)
                //canvas.viewport(0, 0, scope.windowWidth, scope.windowHeight);
                ctx.setTransform(ratio, 0, 0, ratio, 0, 0);
                //scope.resetDrawing()
                //ctx.scale(scope.scale,scope.scale);
                scope.resetDrawing()
                canvas.style.pointerEvents = 'auto|none';
              };
              scope.backingScale = function() {
                  //var ctx = canvas.getContext('2d');
                  var dpr = window.devicePixelRatio || 1;
                  var bsr = ctx.webkitBackingStorePixelRatio ||
                    ctx.mozBackingStorePixelRatio || ctx.msBackingStorePixelRatio ||
                    ctx.oBackingStorePixelRatio || ctx.backingStorePixelRatio || 1;

                  return dpr / bsr;
              };
              scope.clearPageAnnotation = function(){
                for (var i in points){
                    if(points[i].page == scope.pageNumber) { 
                        points.splice(i, 1);
                        //break;
                        scope.clearPageAnnotation()
                    }
                }
                scope.resetDrawing();
              };
              scope.updateNotesValues = function(){
                let radius = 48
                let imgLeft = lastX - radius;
                let imgTop = lastY - radius;
                let imgRight = lastX + radius;
                let imgBottom = lastY + radius;
              
                points.push({page:scope.pageNumber,
                  toolName:scope.tool,
                  x:lastX,
                  y:lastY,
                  notes:scope.userNotes,
                  left:imgLeft,
                  top:imgTop,
                  right:imgRight,
                  bottom:imgBottom})

                var notesMarker = new Image();
                notesMarker.src='/icons/ic_notes.png'
                notesMarker.onload = function(){
                  ctx.drawImage(notesMarker, lastX, lastY);
                  //lastX = lastY = ''
                }
                
              };


              scope.resetDrawing = function(){
                ctx.clearRect(0, 0, scope.windowWidth, scope.windowHeight);
                let imgs = [];
                let imgIndex = 0;
                //Load previously Saved Annotation
                 for(var i in points) {
                    ctx.beginPath()
                    if(points[i].page == scope.pageNumber){
                        //ctx.lineWidth = 2;
                        ctx.lineJoin = ctx.lineCap = 'round';
                        if(points[i].toolName == 'Pencil'){
                            ctx.lineWidth = points[i].thickNess;//scope.lineThickness;
                            ctx.strokeStyle = '#000000'
                            ctx.moveTo(points[i].fhPoints[0].x, points[i].fhPoints[0].y);
                            for (var j = 1; j < points[i].fhPoints.length; j++) {
                              ctx.lineTo(points[i].fhPoints[j].x, points[i].fhPoints[j].y);
                            }
                            ctx.stroke();
                        }else if(points[i].toolName == 'Line'){
                            ctx.lineWidth = points[i].thickNess;
                            ctx.strokeStyle = '#000000'
                            ctx.moveTo(points[i].x1, points[i].y1);
                            ctx.lineTo(points[i].x2, points[i].y2);
                            ctx.stroke();
                        }
                        else if(points[i].toolName == 'Rectangle'){
                            ctx.lineWidth = points[i].thickNess;
                            //ctx.strokeStyle = '#F57F17'
                            ctx.strokeStyle = 'rgba(255, 255, 0, 0.4)'
                            ctx.rect(points[i].x,points[i].y,points[i].w,points[i].h);
                            ctx.fillStyle = 'rgba(255, 255, 0, 0.3)';
                            ctx.fill();
                            ctx.stroke();
                           
                        }else if(scope.tool=='Marker'){
                            ctx.lineWidth = points[i].thickNess;
                            ctx.strokeStyle = 'rgba(255, 255, 0, 0.4)'
                            ctx.moveTo(points[i].fhPoints[0].x, points[i].fhPoints[0].y);
                            for (var j = 1; j < points[i].fhPoints.length; j++) {
                              ctx.lineTo(points[i].fhPoints[j].x, points[i].fhPoints[j].y);
                            }
                            ctx.stroke();
                        }
                        else if(scope.tool=='Notes'){
                            /*var notesMarker = new Image();
                            notesMarker.src='/icons/ic_notes.png'
                            notesMarker.onload = function(){
                              ctx.drawImage(notesMarker, points[i].x, points[i].y);
                            }*/

                            imgs[imgIndex] = new Image();
                            imgs[imgIndex].onload = (function () {
                                var thisX = points[i].x
                                var thisY = points[i].y

                                return function () {
                                    ctx.drawImage(this, thisX, thisY)
                                };
                            }());

                            imgs[imgIndex].src = '/icons/ic_notes.png';
                            imgIndex += 1;

                        }
                    }
                 }
              };
              canvas.onmouseover = function(event) {
                if(event.offsetX!==undefined){
                    currentX = event.offsetX;
                    currentY = event.offsetY;
                  } else {
                    currentX = event.layerX - event.currentTarget.offsetLeft;
                    currentY = event.layerY - event.currentTarget.offsetTop;
                  }

                  for(var i in points) {
                    if(points[i].page == scope.pageNumber && points[i].toolName=='Notes'){
                        if (currentX < points[i].right && currentX > points[i].left && currentY > points[i].top && currentY < points[i].bottom) {
                            //alert ('Your Notes : ' + points[i].notes);
                            //isNotesOpen = true
                            console.log('onmouseover'+points[i].notes)
                        }
                    }
                 }
              }
              canvas.onmouseout = function(event) {
                if(event.offsetX!==undefined){
                    currentX = event.offsetX;
                    currentY = event.offsetY;
                  } else {
                    currentX = event.layerX - event.currentTarget.offsetLeft;
                    currentY = event.layerY - event.currentTarget.offsetTop;
                  }

                  for(var i in points) {
                    if(points[i].page == scope.pageNumber && points[i].toolName=='Notes'){
                        if (currentX < points[i].right && currentX > points[i].left && currentY > points[i].top && currentY < points[i].bottom) {
                            //alert ('Your Notes : ' + points[i].notes);
                            //isNotesOpen = true
                            console.log('onmouseout'+points[i].notes)
                        }
                    }
                 }
              }
              canvas.onmousedown = function(event) {
                lastX = lastY = ''
                if(event.offsetX!==undefined){
                  lastX = event.offsetX;
                  lastY = event.offsetY;
                } else {
                  lastX = event.layerX - event.currentTarget.offsetLeft;
                  lastY = event.layerY - event.currentTarget.offsetTop;
                }
                
                // begins new line
                ctx.beginPath();
                fhPoints = []
                drawing = true;
                let isNotesOpen = false

                for(var i in points) {
                    if(points[i].page == scope.pageNumber && points[i].toolName=='Notes'){
                        if (lastX < points[i].right && lastX > points[i].left && lastY > points[i].top && lastY < points[i].bottom) {
                            //alert ('Your Notes : ' + points[i].notes);
                            isNotesOpen = true
                            scope.showUserNotes(event,points[i].notes)
                        }
                    }
                 }
                if(!isNotesOpen){
                    if(scope.tool=='Pencil'){
                    ctx.lineWidth = scope.lineThickness;
                    ctx.strokeStyle = '#000000'
                    ctx.fillStyle = '#000000';
                    ctx.moveTo(lastX, lastY);
                    fhPoints.push({ x: lastX, y: lastY });
                    }else if(scope.tool=='Line'){
                        ctx.lineWidth = scope.lineThickness;
                        ctx.strokeStyle = '#000000'
                        ctx.fillStyle = '#000000';
                        ctx.moveTo(lastX, lastY);
                    }else if(scope.tool=='Marker'){
                        ctx.lineWidth = scope.lineThickness;
                        ctx.strokeStyle = 'rgba(255, 255, 0, 0.1)'
                        //ctx.fillStyle = 'rgba(255, 255, 0, 0.3)';
                        ctx.moveTo(lastX, lastY);
                        fhPoints.push({ x: lastX, y: lastY });
                    }else if(scope.tool=='Rectangle'){
                        ctx.lineWidth = scope.lineThickness;
                        //ctx.strokeStyle = '#F57F17'
                        ctx.strokeStyle = 'rgba(255, 255, 0, 0.4)'
                        ctx.fillStyle = 'rgba(255, 255, 0, 0.3)';

                    }
                    else if(scope.tool=='Notes'){
                       scope.takeNotes()
                    }
                }

              }
              /*element.bind('mousedown', function(event){
                if(event.offsetX!==undefined){
                  lastX = event.offsetX;
                  lastY = event.offsetY;
                } else {
                  lastX = event.layerX - event.currentTarget.offsetLeft;
                  lastY = event.layerY - event.currentTarget.offsetTop;
                }
                
                // begins new line
                ctx.beginPath();
                drawing = true;
                if(scope.toolName=='Pencil'){
                    ctx.moveTo(lastX, lastY);
                }else if(scope.toolName=='Line'){

                }
                
               
              });*/
              canvas.onmousemove = function(event) {
                if(drawing){
                  // get current mouse position
                  if(event.offsetX!==undefined){
                    currentX = event.offsetX;
                    currentY = event.offsetY;
                  } else {
                    currentX = event.layerX - event.currentTarget.offsetLeft;
                    currentY = event.layerY - event.currentTarget.offsetTop;
                  }

                  if(scope.tool=='Pencil'){
                     ctx.lineTo(currentX, currentY);
                     fhPoints.push({ x: currentX, y: currentY });
                     ctx.stroke();
                  }else if(scope.tool=='Line'){
                    //ctx.moveTo(lastX, lastY);
                    //ctx.lineTo(currentX, currentY);
                    //ctx.stroke();
                  }
                  else if(scope.tool=='Rectangle'){
                    /*scope.clearRect(lastX,lastY,currentX-lastX,currentY-lastY);
                    //ctx.rect(lastX,lastY,currentX-lastX,currentY-lastY);
                    scope.drawRect(lastX,lastY,currentX-lastX,currentY-lastY);
                    ctx.fill();
                    ctx.stroke();*/
                  }else if(scope.tool=='Marker'){
                    ctx.lineTo(currentX, currentY);
                    fhPoints.push({ x: currentX, y: currentY });
                    ctx.stroke();
                  }

                }
              }
              scope.clearRect = function(x,y,w,h) {
                    ctx.clearRect(x, y, w, h);
               };

               scope.drawRect = function(x,y,w,h) {
                    //ctx.fillRect(x, y, w, h);
                    ctx.rect(x, y, w, h);
               };
              canvas.onmouseup = function(event) {
                // stop drawing
                drawing = false;
                if(event.offsetX!==undefined){
                    currentX = event.offsetX;
                    currentY = event.offsetY;
                } else {
                    currentX = event.layerX - event.currentTarget.offsetLeft;
                    currentY = event.layerY - event.currentTarget.offsetTop;
                }
                if(scope.tool=='Line'){
                    ctx.lineTo(currentX, currentY);
                    ctx.stroke();
                    points.push({page:scope.pageNumber,toolName:scope.tool,x1:lastX,y1:lastY,x2:currentX,y2:currentY,thickNess:scope.lineThickness})
                }else if(scope.tool=='Pencil'){
                    points.push({page:scope.pageNumber,toolName:scope.tool,fhPoints:fhPoints,thickNess:scope.lineThickness})
                }else if(scope.tool=='Rectangle'){
                    ctx.rect(lastX,lastY,currentX-lastX,currentY-lastY);
                    ctx.fill();
                    ctx.stroke();
                    points.push({page:scope.pageNumber,toolName:scope.tool,x:lastX,y:lastY,w:currentX-lastX,h:currentY-lastY,thickNess:scope.lineThickness})
                }else if(scope.tool=='Marker'){
                    points.push({page:scope.pageNumber,toolName:scope.tool,fhPoints:fhPoints,thickNess:scope.lineThickness})
                }
              } 
        },
        controller: function($scope, $compile, PDFViewerService, $mdSidenav, 
            $mdDialog, $mdMedia, $log, $timeout, $rootScope, 
            $reactive, $state, $timeout, $q, PageTitle, $mdToast) {

            console.log("1");
            $reactive(this).attach($scope);
            let self = this

            $scope.selectedIndex = 0
            $scope.webLinks = []
            $scope.webLinksDiv1 = []
            $scope.webLinksDiv2 = []

            $scope.bookLinks = []
            $scope.videoLinks = []
            $scope.videoUrlLinks = []
            $scope.videoUrlLinksNote = []
            $scope.pdfLinks = []
            $scope.pdfLinksUrl = ""
            $scope.tool = ''
            $scope.pageNumber = 1
            $scope.scale = 1.0
            $scope.lineThickness = 1
            $scope.totalPageNumber = 1
            $scope.userNotes = ''
            $scope.tools=[{tool:'Pencil',expanded:false,icon:'create'},
                            {tool:'Line',expanded:false,icon:'short_text'},
                            {tool:'Rectangle',expanded:false,icon:'crop_5_4'},
                            {tool:'Notes',expanded:false,icon:'speaker_notes'}]

            
             /*{tool:'Marker',expanded:false,icon:'brush'},*/
            self.subscribe('studentData', () => {
                return [] // parameters to be passed to subscribe method
            }, () => {


                studentData11 = Weblinks.find({
                    subject_id: "123",
                    xclass_id: "123",
                    curriculum_id: "123"
                }).fetch()[0]
                console.log(studentData11);
                // $scope.themeData =studentData11.theme;
                console.log(studentData11.theme);
                //console.log(studentDtat);
                angular.forEach(studentData11.theme, function(value, key) {
                    if (value._id == "456") {
                        console.log("theme id same")
                        let webLinks = value.addl.web_link
                        let refferalBooks = value.addl.reference_book;
                        let videoLinks = value.addl.video_link;
                        let pdfLinks = value.pdf;
                        $scope.webLinks = webLinks
                        $scope.bookLinks = refferalBooks
                        $scope.videoLinks = videoLinks
                        $scope.pdfLinks = pdfLinks;
                        ///$state.go('additionalResource',{"fetchedData":webLinks});
                        console.log($scope.videoLinks);
                        } else {
                            console.log("theme id not same")
                        }
                     })
                 $scope.initpdf();
                })
                
                $scope.initpdf = function() {

                    $scope.pdfLinksUrl = $scope.pdfLinks.url
                    console.log('PDF Link : '+$scope.pdfLinksUrl)
                    $scope.instance = PDFViewerService.Instance("viewer");
                    $scope.setTool = function(toolName,data){
                        $scope.tool = toolName
                        for(var i in $scope.tools) {
                           if($scope.tools[i] != data) {
                             $scope.tools[i].expanded = false;   
                           }
                        }
                        data.expanded = !data.expanded;
                        if(toolName == 'Notes'){
                          //$scope.takeNotes()
                        }
                    }
                    
                    $scope.clearPage = function() {
                        this.clearPageAnnotation()
                    };
                    $scope.showUserNotes = function(event,notes){
                      $mdDialog.show(
                        $mdDialog.alert()
                          .parent(angular.element(document.body))
                          .clickOutsideToClose(true)
                          .title('Your Message')
                          .textContent(notes)
                          .ariaLabel('Alert Dialog Demo')
                          .ok('Ok!')
                          .targetEvent(event)
                      );
                    }
                    $scope.takeNotes = function(){

                      $mdDialog.show({
                        controller: CreateNotesController,
                        templateUrl: 'client/pdf/createNotes.html',
                        parent: angular.element(document.body),
                        clickOutsideToClose:true
                      })
                      .then(function(answer) {
                        console.log(answer.msg)
                      },
                       function(result) {
                        $scope.userNotes = PDFViewerService.getUserNotes()
                        PDFViewerService.setUserNotes('')
                        if($scope.userNotes!=undefined && $scope.userNotes!=""){
                          $scope.saveUserNotes()
                        }
                        //self.status = 'You cancelled the dialog.';
                      })

                    };
                    $scope.saveUserNotes = function(){
                      this.updateNotesValues()
                    }
                    $scope.nextPage = function() {
                        $scope.instance.nextPage();
                        if ($scope.pageNumber < $scope.totalPageNumber) {
                            $scope.pageNumber++;
                            this.resetDrawing()
                        }
                    };
                    $scope.prevPage = function() {
                        $scope.instance.prevPage();
                        if ($scope.pageNumber > 1) {
                            $scope.pageNumber--;
                            this.resetDrawing()
                        }
                    };
                    $scope.gotoPage = function(curPage) {
                        $scope.instance.gotoPage(curPage);
                    };
                    $scope.zoomIn = function() {
                        $scope.instance.zoomIn();
                        $timeout(this.getPdfHeight(), 500)
                    };
                    $scope.pageFit = function() {
                        $scope.instance.fitPage();
                        this.getPdfHeight()
                    };
                    $scope.zoomOut = function() {
                        $scope.instance.zoomOut();
                        $timeout(this.getPdfHeight(), 500)
                    };
                    $scope.pageLoaded = function(curPage, maxPage, totalPages) {

                        $scope.scale = PDFViewerService.getScale()
                        $scope.pageNumber = PDFViewerService.getPageNumber()
                        $scope.totalPageNumber = PDFViewerService.getTotalPageNumber()
                        
                        console.log('Scale  = '+PDFViewerService.getScale())
                        console.log('Number = '+PDFViewerService.getPageNumber())
                        console.log('Total Number = '+$scope.totalPageNumber)
                        
                        $timeout(this.getPdfHeight(), 100)
                    };
                    $scope.loadProgress = function(loaded, total, state) {
                        console.log('loaded =', loaded, 'total =', total, 'state =', state);
                    };
                };
                

           

        }
    }
})
function CreateNotesController($scope, $mdDialog,$mdToast,$document, $state,$reactive,PDFViewerService) {

      $reactive(this).attach($scope);
      let self = this
     
      $scope.cancelNotes = function() {
        $mdDialog.cancel();
      };

      $scope.addNotes = function(note) {
        //$mdDialog.cancel({msg:note});
        PDFViewerService.setUserNotes(note)
        $mdDialog.cancel()
      };
  }