angular.module('cordova_lms').directive('teacherAssignmentAnswerView', function(MyResource) {
    return {
        restrict: 'E',
        templateUrl: 'client/teacherAssignmentAnswerView/teacher-assignment-answer-view.html',
       controllerAs: 'studentAssignmentListCtrl',
       controller: function($scope, $reactive, $state,$mdDialog,$mdMedia,$filter, $timeout, $interval, $meteor, $interval, $stateParams) {
             $reactive(this).attach($scope);
             $scope.status = '  ';
  $scope.customFullscreen = $mdMedia('xs') || $mdMedia('sm');

      let self = this
      self.dynamicTheme = "default"

      self.user = {}
      let studentId=Meteor.userId();
 console.log($scope.questions)
             $scope.getAssginmentQuestion = $stateParams.assignment_answer;
             $scope.getAssginment_id = $stateParams.getAssginment_id;
             $scope.getAssginment_title = $stateParams.getAssginment;
             $scope.getAssginmentQuestion_id=$stateParams.question_id;
             $scope.getstudentGroupArray=[];
             $scope.getstudentDetails=[];
             console.log($scope.getAssginmentQuestion)
             // $scope.quesTypes= $scope.getAssginment_id;
             


                              // console.log($scope.quesTypes.group[0]._id)
                            
           $scope.getAllStudentGroup=function(){
        self.subscribe('getStudentGroup',
        () => {
          return [] // parameters to be passed to subscribe method
        },
        () => {
         console.log( StudentGroup.find().fetch())
          $scope.getstudentGroupArray=StudentGroup.find().fetch();
           $scope.getAllStudent();

        }
      )
       }
      $scope.getAllStudent=function(){
            self.subscribe('getStudent',
        () => {
          return [] // parameters to be passed to subscribe method
        },
        () => {
         console.log( StudentDetails.find({"teacherId":Meteor.userId()}).fetch())
          $scope.getstudentDetails=StudentDetails.find({"teacherId":Meteor.userId()}).fetch();
           $scope.quesTypes= $stateParams.getAssginment;

             console.log($scope.quesTypes)
        }
      )
       }                     

     $scope.getstudentGroup=function(group_id){
      console.log(group_id)
      var getstudentGroupArray1=StudentGroup.find({"_id":group_id}).fetch()
       console.log(getstudentGroupArray1[0].students)

       return getstudentGroupArray1[0].groupName
       }

     $scope.collapseAll = function(data,group_id) {
      $scope.getstudentDetails1=[];
      var getstudentGroupArray1=StudentGroup.find({"_id":group_id}).fetch()
                $scope.getstudentDetails=getstudentGroupArray1[0].students
       angular.forEach($scope.getstudentDetails, function(value, key){
         console.log( StudentDetails.find({"_id":value._id}).fetch())
         studentname = StudentDetails.find({"_id":value._id}).fetch()[0].username
        studentObj = {name:studentname,_id:value._id}
          $scope.getstudentDetails1.push(studentObj);
           })
       $scope.studentArray =  $scope.getstudentDetails1
         
      //    for(var i in  $scope.getAssginment_id) {
      //      if( $scope.getAssginment_id[i] != data) {
      //         $scope.getAssginment_id[i].expanded = false;   
      //      }
      //    }

         data.expanded = !data.expanded;
      };
        /* list assignment questions*/
            $scope.getAssginmentQuestions=function(){
let questionsList = $scope.quesTypes[0].question;
console.log($scope.quesTypes[0]._id)

$state.go('teacherAssignmentView',{"getAssginment":questionsList,"assignment_id":assignment_answer,"question_id":$scope.quesTypes[0]._id});

    }

 $scope.viewAssignment=function(student_id,assignment_id){
console.log(student_id+"\t"+assignment_id);

 angular.forEach($scope.quesTypes, function(value, key){
          angular.forEach(value.answer, function(answer, key){
         console.log(answer.studentId)
         if(student_id==answer.studentId&&assignment_id==value._id){
         assignmentObj = {name:value.tittle,assignment_detaiils:answer}
          }
        
           })
        
           })
//$state.go('teacherAssignmentView',{"getAssginment":assignmentObj});

    }
     $scope.showAdvanced = function(ev) {
    var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'))  && $scope.customFullscreen;

    $mdDialog.show({
      locals: {
        "assignment_id":$scope.getAssginment_id,
        "student_id":$stateParams.assignment_answer.studentId
      },
      controller: DialogController,
      templateUrl: 'client/teacherAssignmentAnswerView/teacher-assignment-answer-view-popup.html',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose:true,
      fullscreen: useFullScreen
    })
    .then(function(answer) {
      $scope.status = 'You said the information was "' + answer + '".';
    }, function() {
      $scope.status = 'You cancelled the dialog.';
    });



    $scope.$watch(function() {
      return $mdMedia('xs') || $mdMedia('sm');
    }, function(wantsFullScreen) {
      $scope.customFullscreen = (wantsFullScreen === true);
    });

  };

  }

}
})
function DialogController($scope, $mdDialog,assignment_id, student_id,$state) {
  console.log(student_id)
  $scope.student_id=student_id
  $scope.hide = function() {
   
  };

  $scope.cancel = function() {
    $mdDialog.cancel();
  };

  $scope.answer = function(answer) {
    $mdDialog.hide(answer);
  };
   $scope.updateAssignmentStatus=function(){
            Meteor.call('updateAssignmentStatus',assignment_id,student_id,$scope.textareaMark,$scope.textareaReview,(error, result) => {
                            if (error) {
                                console.log("Error:" + error);
                            } else {
                               $mdDialog.hide();
                            }
                        })

                                $state.go('asigment');

        
       }
}


/**
Copyright 2016 Google Inc. All Rights Reserved. 
Use of this source code is governed by an MIT-style license that can be in foundin the LICENSE file at http://material.angularjs.org/license.
**/