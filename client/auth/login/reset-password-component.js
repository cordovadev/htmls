//directive component for reset password
angular.module('cordova_lms').directive('resetPassword', function() {
  return {
    restrict: 'E',
    templateUrl: 'client/common/auth/login/reset-password.html',
    controllerAs: 'resetPasswordCntl',

    controller: function($scope, $stateParams, $mdToast, $reactive, $state, $mdDialog, $mdMedia, errorValidatorUtil) {

      $reactive(this).attach($scope);
      let self = this;
      let tokenId = $stateParams.token;

      let new_password = null;
      let confirm_password = null;

      //enter presse
      self.checkEnter = (event) => {

        event = event || window.event //For IE
        if (event.keyCode === 13) {
          self.resetPasswordByToken();
        }
      }

      angular.element("#input_175").focus();

      self.resetPasswordByToken = (ev) => {

        $scope.errors = {};

        new_password = self.new_password;
        confirm_password = self.confirm_password;
        //Client calling....
        self.dataArry = {};

        self.dataArry = {
          "new_password": self.new_password,
          "confirm_password": self.confirm_password
        };
        //console.log(self.dataArry)
        ResetPassword.simpleSchema().namedContext("ResetPasswordSchema").validate(self.dataArry, {
          modifier: false
        });


        let context = ResetPassword.simpleSchema().namedContext("ResetPasswordSchema");
        let errorsList = context.invalidKeys();
        let ret_val = context.invalidKeys();
        //console.log(ret_val);
        ret_val = _.map(ret_val, function(res) {
          return _.extend({
            message: context.keyErrorMessage(res.name)
          }, res);
        });
        console.log(ret_val)
        if (ret_val.length > 0) {
          for (let i = 0; i < ret_val.length; i++) {
            let errorMap = ret_val[i]['name'].split('.')
              // assignErrorValue global function written in lib/app.js 
            errorValidatorUtil.assignErrorValue($scope.errors, errorMap, ret_val[i]['message']);
          }
        } else {
          //server calling...
          self.loadingImage = false;
          Meteor.call('resetPasswordByToken', tokenId, new_password, confirm_password, function(error, result) {
            self.loadingImage = false;
            if (error) {
              // show a error message
              getToastbox($mdToast, error.reason, 'error-toast');
              return false;
            }
            getToastbox($mdToast, "Success...You can login with newly created password", 'success-toast');
            $state.go("login");
          });
        }

      }

    }
  }
});

function getToastbox($mdToast, txt, errorType) {
  $mdToast.show($mdToast.simple()
    .textContent(txt)
    .position('top right')
    .theme(errorType));
}
