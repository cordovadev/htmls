//directive component for forgot password
angular.module('cordova_lms').directive('forgotPassword', function() {

  return {
    restrict: 'E',
    templateUrl: 'client/auth/login/forgot-password.html',
    controllerAs: 'forgotPassword',
    controller: function($scope, $reactive, $state, $mdToast, $mdDialog, $mdMedia, errorValidatorUtil) {

      $reactive(this).attach($scope);
      let self = this;
      $scope.$parent.mainCtrl.pageTitle = "Forgot Password";

      self.credentials = {
        email: ''
      };

      angular.element("#input_175").focus();

      //press enter
      self.checkEnter = (event) => {

        event = event || window.event //For IE
        if (event.keyCode === 13) {
          self.getPassword();
        }
      }

      //calling below function when clicking the button
      self.getPassword = (ev) => {
        $scope.errors = {};
        let email = self.credentials.email;
        //calling method to send reset link
        if (email == '') {
          email = null
        }
        self.datas = {
          "email": email
        }

        //validation
        ForgotPassword.simpleSchema().namedContext("ForgotPasswordSchema").validate(self.datas, {
          modifier: false
        });

        let context = ForgotPassword.simpleSchema().namedContext("ForgotPasswordSchema");
        let errorsList = context.invalidKeys();
        let ret_val = context.invalidKeys();
        ret_val = _.map(ret_val, function(res) {
          return _.extend({
            message: context.keyErrorMessage(res.name)
          }, res);
        });

        if (ret_val.length > 0) {
          for (let i = 0; i < ret_val.length; i++) {
            let errorMap = ret_val[i]['name'].split('.')
              // assignErrorValue global function written in lib/app.js 
            errorValidatorUtil.assignErrorValue($scope.errors, errorMap, ret_val[i]['message']);

          }
        } else {
          self.loadingImage = true;
          //server calling.....
          Meteor.call('sendPasswordResetLink', email, function(error, result) {
self.loadingImage = false;
            if (error) { // if it fails, show error message
              getToastbox($mdToast, error.reason, 'error-toast');
              return false;
            }
            //if success, then shows success message
            getToastbox($mdToast, "We have emailed you the instructions to reset the password", 'success-toast');

          });
        }

      }
    }
  }

});

function getToastbox($mdToast, txt, errorType) {
  $mdToast.show($mdToast.simple()
    .textContent(txt)
    .position('top right')
    .theme(errorType));
}
