//directive component for login
angular.module('cordova_lms').directive('login', function() {

  return {

    restrict: 'E',
    templateUrl: 'client/auth/login/login.html',
    controllerAs: 'accountLogin',

    controller: function($scope, $reactive, $state, errorValidatorUtil, $mdToast, $window) {

      $reactive(this).attach($scope);
      let self = this;
      self.buttonStateDisabled = false;

      //it happens when clicking login button, for success login it will direct
      //to dashboard
      self.newUser = {};

      //enter presse
      self.checkEnter = (event) => {

        //if enter login should happen
        event = event || window.event //For IE
        if (event.keyCode === 13) {
          self.loginUser();
        }
      }

      let loginAttempt = 0;

      //when login button clicks
      self.loginUser = () => {

        email = self.newUser.email; //email
        password = self.newUser.password; //password
        if (email == '') {
          email = null
        }
        if (password == '') {
          password = null
        }
        $scope.errors = {};
        self.dataArry = {};
        self.dataArry = {
          "email": email,
          "password": password
        }

        //validation
        Login.simpleSchema().namedContext("LoginSchema").validate(self.dataArry, {
          modifier: false
        });

        let context = Login.simpleSchema().namedContext("LoginSchema");
        let errorsList = context.invalidKeys();
        let ret_val = context.invalidKeys();
        ret_val = _.map(ret_val, function(res) {
          return _.extend({
            message: context.keyErrorMessage(res.name)
          }, res);
        });
        if (ret_val.length > 0) {
          for (let i = 0; i < ret_val.length; i++) {
            let errorMap = ret_val[i]['name'].split('.')
              // assignErrorValue global function written in lib/app.js 
            errorValidatorUtil.assignErrorValue($scope.errors, errorMap, ret_val[i]['message']);
          }
        } else {
          //server calling........

          self.isDisabled = true;

          //enable login button after some seconds
          setTimeout(function() {
            self.isDisabled = false;
          }, 100);

          //call method for authenticating user
          Meteor.call('authenticateUser', email, password, function(error, result) {

            if (error) { // if it fails, show error message
              getToastbox($mdToast, error.reason, "error-toast");
              loginAttempt++;

              if (loginAttempt >= 3) {
                setTimeout(function() {
                  self.showTimer = true
                  self.isDisabled = true;
                }, 100);

                //reload page after 60seconds
                setTimeout(function() {
                  $window.location.reload();
                }, 10000);

              }
              $scope.$apply();
              return false;
            }

            //if success, then login with token
            Meteor.loginWithToken(result.token, (err) => {

              if (err) {
                self.error = err;
              } else {
                //to set header for logged in user
                $scope.$parent.mainCtrl.custUser = true;

                //set theme by customer role
                if (Meteor.user().role === "finance")
                  $scope.$parent.mainCtrl.dynamicTheme = "financeNavToolbarTheme"
                if (Meteor.user().role === "customer") {
                  $scope.$parent.mainCtrl.dynamicTheme = "customerToolBarTheme"
                }
                if (Meteor.user().role === "admin")
                  $scope.$parent.mainCtrl.dynamicTheme = "default"

                //set customer image icon, if have profile image
                if (Meteor.user().image) {
                  $scope.$parent.mainCtrl.displayIcon = Meteor.user().image.icon;
                }

                $scope.$apply();
                $state.go(result.goTo); // goes to curresponding dashboard

              }
            });
          });

        }
      }
    }
  }

  function getToastbox($mdToast, txt, errorType) {
    $mdToast.show($mdToast.simple()
      .textContent(txt)
      .position('top right')
      .theme(errorType));
  }

});
