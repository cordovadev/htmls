//directive component for verifying user
angular.module('cordova_lms').directive('verifyUser', function() {
  return {
    restrict: 'E',
    templateUrl: 'client/auth/register/verify-user.html',
    controllerAs: 'verifyUserCtrl',
    controller: function($scope, $stateParams, $reactive, $state, $mdToast) {
      $reactive(this).attach($scope);
      let self = this;
      tokenId = $stateParams.token;

      $scope.$parent.mainCtrl.pageTitle = "Choose Your Password";

      //calling below function when clicking the button
      this.verifyUserByToken = (ev) => {

        new_password = self.new_password;
        confirm_password = self.confirm_password;

        Meteor.call('verifyUserByTokenId', tokenId, new_password, confirm_password, function(error, result) {

          if (error) {
            // show a error message
            getToastbox($mdToast, "Please try after some time", 'error-toast');
            return false;
          }

          getToastbox($mdToast, "Success...You can login with newly created password", 'success-toast');
          $state.go("login");
        });

      }

    }
  }
});

function getToastbox($mdToast, txt, errorType) {
  $mdToast.show($mdToast.simple()
    .textContent(txt)
    .position('top right')
    .theme(errorType));
}
