angular.module('cordova_lms').directive('chatComponent', function(MyResource,$window) {
    return {
        restrict: 'E',
        templateUrl: 'client/icollaborate/chat/chatComponent.html',
        controllerAs: 'chatCtrl',
        link: function(scope, element, attrs){

            var w = angular.element($window);
            scope.getWindowDimensions = function () {

                return { 'h': w.height(), 'w': w.width() };
            };
            scope.$watch(scope.getWindowDimensions, function (newValue, oldValue) {
               
                let appWidth = newValue.h
                //console.log('org Width'+appWidth)
                scope.windowWidth = (appWidth/100)*77+'px';
                scope.groupWidth = (appWidth/100)*44+'px';
                console.log('Width'+scope.windowWidth)
                /*scope.style = function () {
                    return { 
                        'height': (newValue.h - 100) + 'px',
                        'width': (newValue.w - 100) + 'px' 
                    };
                };*/

            }, true);

            w.bind('resize', function () {
                scope.$apply();
            });

        },
        controller: function($scope, $compile, $filter, $mdSidenav, 
            $mdDialog, $mdMedia, $log, $timeout, $rootScope, 
            $reactive, $state, $q, $mdToast) {

            $reactive(this).attach($scope);
            let self = this
            $scope.messageList = []
            $scope.userName = ''
            $scope.teacherName=''
            $scope.teacherId = ''
            $scope.receiverName = ''
            $scope.receiverId = ''
            $scope.isReceiverSelected = false
            $scope.msgObj = {}
            $scope.userList =[]
            $scope.userGroup =[]
            $scope.userGroupId = []
            $scope.isGroupSelected = false
            $scope.isGroupChat = false
            $scope.groupList =[]
            $scope.group_users=[]
            $scope.userIP=''

            function getGroup(){
                Meteor.call('getUserGroups', Meteor.userId(),(error, result) => {
                        if (error) {
                            console.log("Error:" + error);
                        } else {
                            let groupData = result
                            console.log("group : " + groupData);
                            $scope.groupList = groupData
                            for (var i = 0; i < groupData.length; i++) {
                                 let isExist = false
                                 for(var j in $scope.userList){
                                    if($scope.userList[j].name == groupData[i].group_name){
                                      isExist = true
                                    }
                                 }
                                if(!isExist){
                                  $scope.userList.push({
                                        name:groupData[i].group_name,
                                        userId: groupData[i].students,
                                        selected:false,
                                        group_id:groupData[i]._id,
                                        isGroup:true
                                    });
                                }
                            }
                            $scope.$apply();
                        }
                  })
                /*self.subscribe('getUserGroupList',
                    () => {
                        return [] // parameters to be passed to subscribe method
                    },
                    () => {
                        //let newgroupData = Chat.find({}).fetch();
                        //console.log('New Group : '+newgroupData)
                        this.helpers({
                        getUserGroupList: () => {
                            console.log('New Group Added')
                            return Chat.find({});
                        }
                      })
                      $scope.test = self.getUserGroupList
                    }
                )*/
              }

            self.subscribe('getUserList',
                  () => {
                      return [] // parameters to be passed to subscribe method
                  },
                  () => {
                      let userDetails = StudentDetails.find({}).fetch();
                      for (var i = 0; i < userDetails.length; i++) {
                        if(userDetails[i].userId!=Meteor.userId()){
                           $scope.userList.push({
                              name:userDetails[i].username,
                              userId: userDetails[i].userId,
                              selected:false,
                              isGroup:false
                          });
                        }else{
                          $scope.userName = userDetails[i].username
                          $scope.teacherId = userDetails[i].teacherId
                          Streamy.emit('nick_set', { 'handle': $scope.userName });
                          Meteor.call('getUserName', $scope.teacherId,(error, result) => {
                              if (error) {
                                  console.log("Error:" + error);
                              } else {
                                  $scope.teacherName = result[0].username
                                  console.log("teacherName : " + $scope.teacherName);
                              }
                          })
                        }
                      }
                      Meteor.call('getClientIp', (error, result) => {
                            if (error) {
                                console.log("Error:" + error);
                            } else {
                                $scope.userIP = result
                                console.log("Your IP : " + result);
                            }
                        })
                      getGroup()
                       /*Meteor.call('getUserGroups', Meteor.userId(),(error, result) => {
                            if (error) {
                                console.log("Error:" + error);
                            } else {
                                let groupData = result
                                console.log("group : " + groupData);
                                $scope.groupList = groupData
                                for (var i = 0; i < groupData.length; i++) {
                                    $scope.userList.push({
                                        name:groupData[i].group_name,
                                        userId: groupData[i].students,
                                        selected:false,
                                        group_id:groupData[i]._id,
                                        isGroup:true
                                    });
                                }
                                $scope.$apply();
                            }
                        })*/

                       

                  }
              )
              
              //this.subscribe("getUserGroupList");

              
             
            //$scope.userList = [{name:'Akhil'},{name:'Jeeva'},{name:'Suralal'},{name:'Vijin'}]

            /*Meteor.call('getUserName', Meteor.userId(),(error, result) => {
                if (error) {
                    console.log("Error:" + error);
                } else {
                    $scope.userName = result[0].username
                    console.log("result : " + result);
                }
            })*/

            //Group Chat Start

            // Override Meteor._debug to filter for custom msgs
            /*Meteor._debug = (function (super_meteor_debug) {
              return function (error, info) {
                if (!(info && _.has(info, 'msg')))
                  super_meteor_debug(error, info);
              }
            })(Meteor._debug);*/

            var nick// = new ReactiveVar();
            var room// = new ReactiveVar('lobby');
            // Add a local only collection to manage messages
            Messages = new Mongo.Collection(null);

            /**
             * Try to retrieve a client by its nickname
             * @param  {String} nick Nickname to look for
             * @return {Client}      Client object or null|undefined if not found
             */
            function findClient(nick) {
              return Clients.findOne({ 'nick': nick});
            }
            function addMessage(msgObj){
              
              Meteor.call('setMessage',msgObj, (error, result) => {
                  if (error) {
                      console.log("Message Add Error:" + error);
                  } else {
                      console.log("Message Added succesfully");
                  }
              })
            }
            /**
             * Generic method to insert a message in the chat panel
             * @param  {String} room Room name concerned
             * @param  {String} body Body message
             * @param  {String} from Session id of the sender
             */
            function insertMessage(room, body, from) {
              // Do nothing if not logged in
             /* if(!nick.get())
                return;*/
              let isGroup = false
              var c = from ? Clients.findOne({ 'sid': from }): null;
              
              if(from && !c)
                c = { 'nick': from };

              Messages.insert({
                'room': room,
                'body': body,
                'from': c && c.nick
              });
              if(room!=null){
                isGroup = true
              }

              let msgObj = {'sender':body.sender,
                            'sender_id':body.sender_id,
                            'receiver':body.receiver,
                            'receiver_id':body.receiver_id,
                            'msg':body.msg,
                            'time':body.time,
                            'self':body.self,
                            'group':body.group,
                            'group_name':room,
                            'user_ip':$scope.userIP}
              addMessage(msgObj)
              if(from === Streamy.id())
                return;

              if(isGroup){
                for(var group in $scope.groupList){
                  if(body.receiverID == $scope.groupList[group]._id){
                     $scope.receiverName = body.userlist;//receiverObj.sender
                     $scope.receiverId = body.sender_id
                     $scope.chatName = body.receiver
                     $scope.isReceiverSelected = true;
                     $scope.messageList.push(body)
                  }
                  for(var i in $scope.userList) {
                     if($scope.userList[i].group_id == body.receiver_id) {
                       $scope.userList[i].selected = true;   
                     }else{
                       $scope.userList[i].selected = false;
                     }
                  }
                }
                $scope.isGroupChat = true
                $scope.userMsg = ''
                $scope.$apply();
              }else{
                 if(body.receiver_id == Meteor.userId()){
                  $scope.receiverName = body.sender
                  $scope.chatName = body.sender
                  $scope.receiverId = body.sender_id
                  $scope.isReceiverSelected = true;
                  $scope.messageList.push(body)

                  for(var i in $scope.userList) {
                     if($scope.userList[i].userId == body.sender_id) {
                       $scope.userList[i].selected = true;   
                     }else{
                       $scope.userList[i].selected = false;
                     }
                  }
                  $scope.isGroupChat = false
                  $scope.userMsg = ''
                  $scope.$apply();
                }
              }
              $scope.$apply();
              //$('.chat__messages').scrollTo($('li.chat__messages__item:last'));
            }
            // On connected, subscribe to collections
            Streamy.onConnect(function() {
              Meteor.subscribe('clients');
              Meteor.subscribe('rooms', Streamy.id());
              Streamy.join('lobby');
            });


            this.helpers({
              /*studentData: () => {
                  return Students.findOne({ registerId: $stateParams.userID });
              }

              selectedClass: function(room_name) {
                var current_room = room.get();

                return (current_room === room_name.toLowerCase()) && 'rooms__list__item_active';
              },*/
              messages: function() {
                var current_room = room;//.get();

                return Messages.find({ 
                  $or: [
                    { 'room': current_room },
                    { 'room': null } // Direct messages
                  ]
                });
              },
              rooms: function() {
                return Streamy.rooms();
              }


            });
            // On disconnect, reset nick name
            Streamy.onDisconnect(function() {
              //nick.set('');
              Messages.remove({});
            });

            Streamy.on('nick_ack', function(data) {
              //nick.set(data.nick);
            });

            // On a lobby message, insert the message
            Streamy.on('lobby', function(data) {
              insertMessage('lobby', data.body, data.__from);
            });

            // More generic, when receiving from a room this message, insert it
            Streamy.on('text', function(data) {
              //console(data.__in+" : "+ data.body+" : "+ data.__from)
              insertMessage(data.__in, data.body, data.__from);
              
            });
            // On private message
            Streamy.on('private', function(data) {
              insertMessage(null, data.body, data.__from);
            });

            // Someone has joined
            Streamy.on('__join__', function(data) {
              // Dismiss if self
              if(data.sid === Streamy.id())
                return;

              var c = Clients.findOne({ 'sid': data.sid });
              var msg = ((c && c.nick) || "Someone") + " has joined";

              //insertMessage(data.room, msg);
            });

            // Someone has left
            Streamy.on('__leave__', function(data) {
              var c = Clients.findOne({ 'sid': data.sid });
              var msg = ((c && c.nick) || 'Someone') + " has left";

              //insertMessage(data.room, msg);
            });



            //Group Chat End
            $scope.filterByUser = function(msgObj) {
              if(msgObj.isGroup){
                //return ($scope.selectedGenres.indexOf(movie.genre) !== -1);
                return $scope.userlist;
              }else{
                return $scope.receiverName;
              }
              
            };

            $scope.getDatetime = function() {
              let today = new Date()
              //let currentTime = new Date(today.getFullYear(), today.getMonth(), today.getDate());
              //yyyy-MM-dd HH:mm:ss
              //return $filter('date')(today, 'HH:mm:ss');
              var hours = today.getHours();
              var minutes = today.getMinutes();
              var ampm = hours >= 12 ? 'pm' : 'am';
              hours = hours % 12;
              hours = hours ? hours : 12; // the hour '0' should be '12'
              minutes = minutes < 10 ? '0'+minutes : minutes;
              var strTime = hours + ':' + minutes + ' ' + ampm;
              return strTime;
            };
            $scope.startChat = function(userData){
              $scope.receiverName = userData.name
              $scope.receiverId = userData.userId
              $scope.isReceiverSelected = true;
              for(var i in $scope.userList) {
                 if($scope.userList[i] != userData) {
                   $scope.userList[i].selected = false;   
                 }
               }
               userData.selected = !userData.selected;
               $scope.isGroupChat = userData.isGroup
               if($scope.isGroupChat){
                  $scope.group_users = []
                  for(var i in userData.userId) {
                   $scope.group_users.push(userData.userId[i].name)
                 }
                  //$scope.group_users=userData.userId
                  $scope.group_id=userData.group_id
                  $scope.group_name=userData.name
                  //Join Room with group name
                  Streamy.join(userData.name);
                  // And switch to it
                  room = userData.name;
               }
               $scope.chatName = userData.name
               $scope.userMsg = ''
            }
            $scope.setUserSelction = function(set){
              for(var i in $scope.userList) {
                  $scope.userList[i].selected = set;   
               }
            }
            $scope.enableChatList = function(){
              $scope.setUserSelction(false)
              $scope.isGroupSelected = false
              for(var i in $scope.userList) {
                   if($scope.userList[i].userId != $scope.receiverId) {
                     $scope.userList[i].selected = false;   
                   }else{
                     $scope.userList[i].selected = true;
                     $scope.isReceiverSelected = true;
                   }
                }
            }
            $scope.enableGroupCreation = function(){
              $scope.setUserSelction(false)
              $scope.isGroupSelected = true
              $scope.userGroup =[]
              $scope.userGroupId = []
              $scope.isReceiverSelected = false;
            }
            $scope.createGroup = function(){
              if($scope.studentGroupName!=undefined){
                
                if($scope.userGroup.length == 0)
                {
                  $scope.showToast("Please select atleat one student")
                  return
                }

                let groupDetails = {
                'group_name': $scope.studentGroupName,
                'students': []
                }
                for(var i in $scope.userGroup) {
                    groupDetails.students.push({'name':$scope.userGroup[i].name,
                                'user_id': $scope.userGroup[i].userId,
                                'is_teacher':false})
                }
                //To push creator details
                groupDetails.students.push({'name':$scope.userName,
                                'user_id': Meteor.userId(),
                                'is_teacher':false})
                //To push teacher details
                groupDetails.students.push({'name':$scope.teacherName,
                                'user_id': $scope.teacherId,
                                'is_teacher':true})
                
               
                Meteor.call('updateGroupList', groupDetails, (error, result) => {
                      if (error) {
                          console.log("Error:" + error);
                      } else {
                          console.log(' Group Successfully Added ')
                          getGroup()
                          $scope.studentGroupName = ''
                      }
                  })
                $scope.enableChatList()
              }else{
                $scope.showToast("Please Enter group Name")
              }
              
            }
            $scope.checkUserExist = function(item,isAdd) {
                let isExist = false;
                let idx = $scope.userGroupId.indexOf(item)
                if ( idx > -1) {
                    if(!isAdd){
                       $scope.userGroupId.splice(idx, 1);
                    }
                    return true
                } else {
                    $scope.userGroupId.push(item);
                    return isExist
                }
            };
            $scope.selectUserForGroup = function(userData){
              userData.selected = !userData.selected;

              if(!$scope.checkUserExist(userData.userId,userData.selected)){
                  $scope.userGroup.push(userData)
              }else{
                if(!userData.selected){

                }
              }
              
            }

            $scope.sendMessage = function(sendingMsg){
              if($scope.isGroupChat){

                var current_room = room;//.get();
                // Sends the message, using the broadcast or rooms feature
               /* if(current_room === 'lobby'){
                  Streamy.broadcast('lobby', { 'body': sendingMsg });
                }
                else{*/
                  $scope.groupMsgObj = {sender:$scope.userName,
                                sender_id:Meteor.userId(),
                                receiver:$scope.group_name,
                                receiver_id:$scope.group_id,
                                /*user_list:$scope.group_users,*/
                                msg:sendingMsg,
                                time:$scope.getDatetime(),
                                self:false,
                                group:true}
                  //Streamy.rooms(current_room).emit('text', { 'body': sendingMsg });
                  Streamy.rooms(current_room).emit('text', { 'body': $scope.groupMsgObj});
                  $scope.userMsg = ''
                  $scope.messageList.push({sender:$scope.userName,
                                        receiver:$scope.group_name,
                                        receiver_id:$scope.group_id,
                                        /*user_list:$scope.group_users,*/
                                        msg:sendingMsg,
                                        time:$scope.getDatetime(),
                                        self:true,
                                        group:true})
                //}
              }else{
                $scope.msgObj = {sender:$scope.userName,
                                sender_id:Meteor.userId(),
                                receiver:$scope.receiverName,
                                receiver_id:$scope.receiverId,
                                msg:sendingMsg,
                                time:$scope.getDatetime(),
                                self:false,
                                group:false}

                let to = findClient($scope.receiverName);
                $scope.userMsg = ''
               /* streamer.emit('message', $scope.msgObj);
                $scope.userMsg = ''
                $scope.messageList.push({sender:$scope.userName,
                                        senderID:Meteor.userId(),
                                        receiver:$scope.receiverName,
                                        receiverID:$scope.receiverId,
                                        msg:sendingMsg,
                                        time:$scope.getDatetime(),
                                        isOwn:true})*/
                 if(!to){
                  $scope.messageList.push({sender:$scope.userName,
                                        sender_id:Meteor.userId(),
                                        receiver:$scope.receiverName,
                                        receiver_id:$scope.receiverId,
                                        msg:sendingMsg,
                                        time:$scope.getDatetime(),
                                        self:true,
                                        group:false})
                 }else{
                    Streamy.sessions($scope.receiverName && to.sid).emit('private', {'body': $scope.msgObj});
                    $scope.messageList.push({sender:$scope.userName,
                                        sender_id:Meteor.userId(),
                                        receiver:$scope.receiverName,
                                        receiver_id:$scope.receiverId,
                                        msg:sendingMsg,
                                        time:$scope.getDatetime(),
                                        self:true,
                                        group:false})
                 }
                //Streamy.sessions($scope.receiverName && to.sid).emit('private', {'body': $scope.msgObj});
                
              }
            }

            var last = {
              bottom: false,
              top: true,
              left: false,
              right: true
            };
            $scope.toastPosition = angular.extend({},last);
            $scope.getToastPosition = function() {
              sanitizePosition();
              return Object.keys($scope.toastPosition)
                .filter(function(pos) { return $scope.toastPosition[pos]; })
                .join(' ');
            };
            function sanitizePosition() {
              var current = $scope.toastPosition;
              if ( current.bottom && last.top ) current.top = false;
              if ( current.top && last.bottom ) current.bottom = false;
              if ( current.right && last.left ) current.left = false;
              if ( current.left && last.right ) current.right = false;
              last = angular.extend({},current);
            }

            $scope.showToast = function( data) {
              $mdToast.show(
                $mdToast.simple()
                  .textContent(data)
                  .position($scope.getToastPosition())
                  .hideDelay(3000)
              );
            };
            /*streamer.on('groupmessage', function(receiverObj) {
 

                for(var group in $scope.groupList){
                  if(receiverObj.receiverID == $scope.groupList[group]._id){
                     $scope.receiverName = receiverObj.userlist;//receiverObj.sender
                     $scope.receiverId = receiverObj.senderID
                     $scope.chatName = receiverObj.sender
                     $scope.isReceiverSelected = true;
                     $scope.messageList.push(receiverObj)
                  }
                  for(var i in $scope.userList) {
                     if($scope.userList[i].group_id != receiverObj.receiverID) {
                       $scope.userList[i].selected = false;   
                     }else{
                       $scope.userList[i].selected = true;
                     }
                  }
                }
              
            });*/

            /*streamer.on('message', function(receiverObj) {
              
                if(receiverObj.receiverID == Meteor.userId()){
                  $scope.receiverName = receiverObj.sender
                  $scope.chatName = receiverObj.sender
                  $scope.receiverId = receiverObj.senderID
                  $scope.isReceiverSelected = true;
                  $scope.messageList.push(receiverObj)

                  for(var i in $scope.userList) {
                     if($scope.userList[i].userId != receiverObj.senderID) {
                       $scope.userList[i].selected = false;   
                     }else{
                       $scope.userList[i].selected = true;
                     }
                  }
                }
              
            });*/
        }
    }
})
angular.module('cordova_lms').filter('selectedUserChat', function() {
   return function( items, scope) {
    var filtered = [];
    
    angular.forEach(items, function(item) {
      
       if(scope.isGroupChat){
          if(item.group){
            //
            if(item.receiver == scope.chatName){
                filtered.push(item);
            }
          }

       }else{
         if(!item.group){
             //Own Message
             if(item.sender_id == Meteor.userId() && item.receiver == scope.receiverName){
                filtered.push(item);
             }
             //Incoming Message
             if(item.receiver == scope.userName && item.sender_id == scope.receiverId){
                filtered.push(item);
             }
         }
       }
       /*if(condition === item.condition ||  item.condition === ''){
         filtered.push(item);senderID:Meteor.userId(),
                                        receiver:$scope.receiverName,
                                        receiverID:$scope.receiverId
       }*/
    });
 
    return filtered;
  };
});