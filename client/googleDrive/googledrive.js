angular.module('cordova_lms').directive('googleDrive', function() {
    return {
        restrict: 'E',
        templateUrl: 'client/googleDrive/googledrive.html',
        controllerAs: 'directiveCtrl',
        controller: function($scope, $mdToast, $reactive, $window, $stateParams, $state) {
            // controller code begins
            $reactive(this).attach($scope)
            let self = this
            $scope.studentdetailss = []

            var CLIENT_ID = '688819777685-e5sdb510r6t05tmp1go46fjdbcvfjkov.apps.googleusercontent.com';

            var SCOPES = ['https://www.googleapis.com/auth/admin.directory.user.readonly','https://www.googleapis.com/auth/admin.directory.group'];

            /**
             * Check if current user has authorized this application.
             */
            function checkAuth() {
                gapi.auth.authorize({
                    'client_id': CLIENT_ID,
                    'scope': SCOPES.join(' '),
                    'immediate': true
                }, handleAuthResult);
            }

            /**
             * Handle response from authorization server.
             *
             * @param {Object} authResult Authorization result.
             */
            function handleAuthResult(authResult) {
                var authorizeDiv = document.getElementById('authorize-div');
                if (authResult && !authResult.error) {
                    // Hide auth UI, then load client library.
                    authorizeDiv.style.display = 'none';
                    loadDirectoryApi();
                } else {
                    // Show auth UI, allowing the user to initiate authorization by
                    // clicking authorize button.
                    authorizeDiv.style.display = 'inline';
                }
            }

            /**
             * Initiate auth flow in response to user clicking authorize button.
             *
             * @param {Event} event Button click event.
             */
            $scope.handleAuthClick = function() {
                gapi.auth.authorize({
                        client_id: CLIENT_ID,
                        scope: SCOPES,
                        immediate: false
                    },
                    handleAuthResult);
                return false;
            }

            $scope.createGroup = function( name ) {
              gapi.auth.authorize({
                        client_id: CLIENT_ID,
                        scope: SCOPES,
                        immediate: false
                    },
                    handleAuthResult);
                gapi.client.load('admin', 'directory_v1', listUsers);
                var request = gapi.client.directory.groups.insert({
                    "email": name,
                    "name": "Group Cordova",
                    "description": "this is group"
                });

                request.execute(function(resp) {

                    console.log(resp);


                });
            }

            /**
             * Load Directory API client library. List users once client library
             * is loaded.
             */
            function loadDirectoryApi() {
                gapi.client.load('admin', 'directory_v1', listUsers);
            }

            /**
             * Print the first 10 users in the domain.
             */
            function listUsers() {
                var request = gapi.client.directory.users.list({
                    'customer': 'my_customer',
                    'maxResults': 100,
                    'orderBy': 'email'
                });

                request.execute(function(resp) {
                    var users = resp.users;
                    if (users && users.length > 0) {
                        for (i = 0; i < users.length; i++) {
                            var user = users[i];
                            studentitem = {
                                email: user.primaryEmail,
                                username: user.name.fullName
                            };
                            //   appendPre('-' + user.primaryEmail + ' (' + user.name.fullName + ')');
                            $scope.studentdetailss.push(studentitem);
                        }
                        $scope.$apply()
                    } else {
                        appendPre('No users found.');
                    }
                });
            }

            /**
             * Append a pre element to the body containing the given message
             * as its text node.
             *
             * @param {string} message Text to be placed in pre element.
             */
            function appendPre(message) {
                var pre = document.getElementById('output');
                var textContent = document.createTextNode(message + '\n');
                pre.appendChild(textContent);
            }

            var last = {
                bottom: false,
                top: true,
                left: false,
                right: true
            };
            $scope.toastPosition = angular.extend({}, last);
            $scope.getToastPosition = function() {
                sanitizePosition();
                return Object.keys($scope.toastPosition)
                    .filter(function(pos) {
                        return $scope.toastPosition[pos];
                    })
                    .join(' ');
            };

            function sanitizePosition() {
                var current = $scope.toastPosition;
                if (current.bottom && last.top) current.top = false;
                if (current.top && last.bottom) current.bottom = false;
                if (current.right && last.left) current.left = false;
                if (current.left && last.right) current.right = false;
                last = angular.extend({}, current);
            }

            $scope.showToast = function(data) {
                $mdToast.show(
                    $mdToast.simple()
                    .textContent(data)
                    .position($scope.getToastPosition())
                    .hideDelay(3000)
                );
            };


        }
    }
});