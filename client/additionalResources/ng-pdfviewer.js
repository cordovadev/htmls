
angular.module('cordova_lms').
directive('pdfviewer', [ '$parse', function($parse, PDFViewerService) {
	var canvas = null;
	var instance_id = null;

	return {
		restrict: "E",
		template: '<canvas style="height:100%;width:100%" ></canvas>',
		scope: {
			onPageLoad: '&',
			loadProgress: '&',
			src: '@',
			id: '='
		},

		controllerAs: "pdfUrlCtrller",
		controller: function($scope,PDFViewerService) {
			$scope.pageNum = 1;
			$scope.pdfDoc = null;
			$scope.scale = 1.0;
			$scope.pageFit = true
			// console.log(PDFViewerService.something)
			
		   
			$scope.documentProgress = function(progressData) {
				if ($scope.loadProgress) {
					$scope.loadProgress({state: "loading", loaded: progressData.loaded, total: progressData.total});
				}

			};
			$scope.loadPDF = function(path, initPage) {
				console.log('loadPDF ', path);

				PDFJS.getDocument(path, null, null, $scope.documentProgress).then(function(_pdfDoc) {
					$scope.pdfDoc = _pdfDoc;
					if(initPage){
						$scope.pageNum = initPage
					}
					$scope.renderPage($scope.pageNum, function(success) {
						if ($scope.loadProgress) {
							$scope.loadProgress({state: "finished", loaded: 0, total: 0});
						}
					});
				}, function(message, exception) {
					console.log("PDF load error: " + message);
					if ($scope.loadProgress) {
						$scope.loadProgress({state: "error", loaded: 0, total: 0});
					}
				});
			};
			$scope.backingScale = function() {
		      var ctx = canvas.getContext('2d');
		      var dpr = window.devicePixelRatio || 1;
		      var bsr = ctx.webkitBackingStorePixelRatio ||
		        ctx.mozBackingStorePixelRatio || ctx.msBackingStorePixelRatio ||
		        ctx.oBackingStorePixelRatio || ctx.backingStorePixelRatio || 1;

		      return dpr / bsr;
		    };
			$scope.renderPage = function(num, callback) {
				if($scope.$parent.pageEnd<num){
					return false;
				}
				if($scope.$parent.pageStart>num){
					
					return false;
				}
				console.log('renderPage ', num);
				$scope.pdfDoc.getPage(num).then(function(page) {
					var viewport,pageWidthScale;
					if ($scope.pageFit) {
		              viewport = page.getViewport(1);
		              var clientRect = canvas.getBoundingClientRect();
		              pageWidthScale = clientRect.width / viewport.width;
		              $scope.scale = pageWidthScale;
		            }

					viewport = page.getViewport($scope.scale);
					var ctx = canvas.getContext('2d');
					var ratio = $scope.backingScale()
					//canvas.height = viewport.height;
					//canvas.width = viewport.width;
					var w = viewport.width
					var h = viewport.height
					canvas.width = Math.floor(w * ratio);
			        canvas.height = Math.floor(h * ratio);
			        canvas.style.width = Math.floor(w) + 'px';
			        canvas.style.height = Math.floor(h) + 'px';
			        console.log('PDF -  ratio', ratio);
			        console.log('PDF - widthidth', canvas.width);
			        console.log('PDF - height', canvas.height);
			        console.log('PDF - innerW', canvas.style.width);
			        console.log('PDF - innerH', canvas.style.height);
			        canvas.getContext('2d').setTransform(ratio, 0, 0, ratio, 0, 0);

					page.render({ canvasContext: ctx, viewport: viewport }).promise.then(
						function() { 
							if (callback) {
								callback(true);
							}
							$scope.$apply(function() {
								$scope.onPageLoad({ page: $scope.pageNum, total: $scope.pdfDoc.numPages });
							});
						}, 
						function() {
							if (callback) {
								callback(false);
							}
							console.log('page.render failed');
						}
					);
				});
			};

			$scope.$on('pdfviewer.nextPage', function(evt, id) {
				if (id !== instance_id) {
					return;
				}
				if($scope.$parent.pageEnd < ($scope.pageNum + 1)){
					return false;
				}
				if ($scope.pageNum < $scope.pdfDoc.numPages) {
					$scope.pageNum++;
					$scope.renderPage($scope.pageNum);
				}
			});
			$scope.$on('pdfviewer.prevPage', function(evt, id) {
				if (id !== instance_id) {
					return;
				}
				if($scope.$parent.pageStart > ($scope.pageNum - 1) ){
					return false;
				}
				if ($scope.pageNum > 1) {
					$scope.pageNum--;
					$scope.renderPage($scope.pageNum);
				}
			});
			$scope.$on('pdfviewer.gotoPage', function(evt, id, page) {
				if (id !== instance_id) {
					return;
				}

				if (page >= 1 && page <= $scope.pdfDoc.numPages - 1) {
					$scope.pageNum = page;
					$scope.renderPage($scope.pageNum);
				}
			});
			$scope.$on('pdfviewer.zoomIn', function(evt, id) {
				if (id !== instance_id) {
					return;
				}
				$scope.pageFit = false
				$scope.scale = parseFloat($scope.scale) + 0.2;
          		$scope.renderPage($scope.pageNum);
			});
			$scope.$on('pdfviewer.setFitPage', function(evt, id) {
				if (id !== instance_id) {
					return;
				}
				$scope.pageFit = true
				$scope.scale = 1.0
				$scope.renderPage($scope.pageNum);
			});
			
			$scope.$on('pdfviewer.zoomOut', function(evt, id) {
				if (id !== instance_id) {
					return;
				}
				$scope.pageFit = false
				$scope.scale = parseFloat($scope.scale) - 0.2;
          		$scope.renderPage($scope.pageNum);
			});
			$scope.$on('pdfviewer.getScale', function(evt) {
				
				PDFViewerService.setScale($scope.scale);
			});
			$scope.$on('pdfviewer.getPageNumber', function(evt) {
				
				PDFViewerService.setPageNumber($scope.pageNum);
			});
			$scope.$on('pdfviewer.getTotalPageNumber', function(evt) {
				
				PDFViewerService.setTotalPageNumber($scope.pdfDoc.numPages);
			});
			
		},
		link: function(scope, iElement, iAttr) {
			canvas = iElement.find('canvas')[0];
			instance_id = iAttr.id;

			iAttr.$observe('src', function(v,p) {
				console.log('src attribute changed, new value is', v);
				if (v !== undefined && v !== null && v !== '') {
					scope.pageNum = 1;
					scope.loadPDF(scope.src,scope.$parent.pageStart);
				}
			});
		}
	};
}]).
service("PDFViewerService", [ '$rootScope', function($rootScope) {
	var svc = { };
	var scale;
	var pageNumber;
	var totlaPages,userNotes;
	svc.minPage = function(){
		$rootScope.$broadcast('pdfviewer.minPage')
	};

	svc.nextPage = function() {
		$rootScope.$broadcast('pdfviewer.nextPage');
	};

	svc.prevPage = function() {
		$rootScope.$broadcast('pdfviewer.prevPage');
	};

	svc.getScale = function(){
		$rootScope.$broadcast('pdfviewer.getScale');
		return this.scale;
	};

	svc.getPageNumber = function(){
		$rootScope.$broadcast('pdfviewer.getPageNumber');
		return this.pageNumber;
	};
	svc.getTotalPageNumber = function(){
		$rootScope.$broadcast('pdfviewer.getTotalPageNumber');
		return this.totlaPages;
	};

	svc.setPageNumber = function(pageNum){
		this.pageNumber = pageNum
	};
	svc.setTotalPageNumber = function(totalPageNum){
		this.totlaPages = totalPageNum
	};
	svc.setScale = function(scale){
		this.scale = scale
	};
	svc.setUserNotes = function(note){
		this.userNotes = note;
	};
	svc.getUserNotes = function(){
		return this.userNotes;
	};
	svc.Instance = function(id) {
		var instance_id = id;

		return {
			prevPage: function() {
				$rootScope.$broadcast('pdfviewer.prevPage', instance_id);
			},
			nextPage: function() {
				$rootScope.$broadcast('pdfviewer.nextPage', instance_id);
			},
			gotoPage: function(page) {
				$rootScope.$broadcast('pdfviewer.gotoPage', instance_id, page);
			},
			zoomIn: function() {
				$rootScope.$broadcast('pdfviewer.zoomIn', instance_id);
			},
			zoomOut: function() {
				$rootScope.$broadcast('pdfviewer.zoomOut', instance_id);
			},
			fitPage: function() {
				$rootScope.$broadcast('pdfviewer.setFitPage', instance_id);
			},
		};
	};

	return svc;
}]);
