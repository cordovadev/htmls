angular.module('cordova_lms').directive('additionalResources', function(PDFViewerService) {
    return {
        restrict: 'E',
        templateUrl: 'client/additionalResources/additional-resources.html',
        controllerAs: 'headerCtrl',

        controller: function($scope, $compile, PDFViewerService, $mdSidenav, $mdDialog, $mdMedia, $log, $timeout, $rootScope, $reactive, $state, $timeout, $q, PageTitle, $mdToast) {

            console.log("1");
            $reactive(this).attach($scope);
            let self = this


            $scope.webLinks = []
            $scope.webLinksDiv1 = []
            $scope.webLinksDiv2 = []

            $scope.bookLinks = []
            $scope.videoLinks = []
            $scope.videoUrlLinks = []
            $scope.videoUrlLinksNote = []
            $scope.pdfLinks = []
            $scope.pdfLinksUrl = ""
            self.subscribe('studentData', () => {
                return [] // parameters to be passed to subscribe method
            }, () => {


                studentData11 = Weblinks.find({
                    subject_id: "123",
                    xclass_id: "123",
                    curriculum_id: "123"
                }).fetch()[0]
                console.log(studentData11);
                // $scope.themeData =studentData11.theme;
                console.log(studentData11.theme);
                //console.log(studentDtat);
                angular.forEach(studentData11.theme, function(value, key) {
                    if (value._id == "456") {
                        console.log("theme id same")
                        let webLinks = value.addl.web_link
                        let refferalBooks = value.addl.reference_book;
                        let videoLinks = value.addl.video_link;
                        let pdfLinks = value.pdf;
                        $scope.webLinks = webLinks
                        $scope.bookLinks = refferalBooks
                        $scope.videoLinks = videoLinks
                        $scope.pdfLinks = pdfLinks;
                        ///$state.go('additionalResource',{"fetchedData":webLinks});
                        console.log($scope.videoLinks);
                    } else {
                        console.log("theme id not same")
                    }
                })

                $scope.init = function() {
                    $scope.playVideo($scope.videoLinks[0].url, $scope.videoLinks[0].note)
                    $scope.breakList(3, $(".showBookLink"));

                    console.log("init function invoke");
                    console.log($scope.videoLinks);
                    var html = "";
                    angular.forEach($scope.videoLinks, function(value, key) {
                        if (value.url.search(".flv") === -1) {
                            var urlNote = value.note.toString();
                            console.log(urlNote.search(" "));
                            html += '<a class="videoUrl  "><div  class="userInputs" ng-click=playVideo(videoLinks[' + key + '].url,videoLinks[' + key + '].note);> <iframe src="' + value.url + '" width="200" height="140" controls="controls" autoplay="false" frameborder="1" scrolling="no" ></iframe><br><p>Note : ' + urlNote + '</p></div></a><br>';
                        } else {
                            console.log(" flash video")
                                // $(".player").flowplayer({ swf: value.url});
                                // html+='<md-card  class="userInputs"> <md-card-title-text><a><video id="video1" class="video-js vjs-default-skin" width="420" height="315" controls="true" autoplay ="true" preload = "auto"> <source src="'+value.url+'" type="video/x-flv"> </video> </a><br>Note : '+value.note+'</a></md-card-title-text><md-card-title-media><div class="md-media-md card-media"></div> </md-card-title-media></md-card-title> </md-card>';
                        }
                        //  console.log(html);

                    })
                    console.log(html);
                    var temp = $compile(html)($scope);
                    var myEl = angular.element(document.querySelector('#space-for-video'));
                    myEl.append(temp);

                };
                $scope.initpdf = function() {

                    $scope.pdfLinksUrl = $scope.pdfLinks.url
                    console.log('PDF Link : '+$scope.pdfLinksUrl)
                    $scope.instance = PDFViewerService.Instance("viewer");
                    // $scope.pageStart = parseInt(pageStart);
                    // $scope.pageEnd = parseInt(pageEnd);

                    $scope.closeDialog = function() {
                        $mdDialog.hide();
                    };
                    $scope.nextPage = function() {
                        $scope.instance.nextPage();
                    };
                    $scope.prevPage = function() {
                        $scope.instance.prevPage();
                    };
                    $scope.gotoPage = function(curPage) {
                        $scope.instance.gotoPage(curPage);
                    };
                    $scope.pageLoaded = function(curPage, maxPage, totalPages) {
                        $scope.currentPage = curPage;
                        $scope.maximumPage = maxPage;

                        $scope.maximum = 33;
                        $scope.totalPages = totalPages;
                    };
                    $scope.loadProgress = function(loaded, total, state) {
                        console.log('loaded =', loaded, 'total =', total, 'state =', state);
                    };
                };
                $scope.playVideo = function(url, note) {
                    var playHtml = "";
                    $scope.videoUrlLinks.push(url);
                    $scope.videoUrlLinksNote.push(note);
                    console.log($scope.videoUrlLinks);
                    playHtml = '<iframe src="' + url + '" width="793" height="400" autoplay="true" frameborder="1" ></iframe>';
                    var tempHtml = $compile(playHtml)($scope);
                    var myelHtml = angular.element(document.querySelector('.playVideoFrame'));
                    myelHtml.html(tempHtml);
                    $(".videoNote").text(note);
                    $scope.pervious();
                };
                $scope.breakList = function(arr, size) {

                    var newArr = [];
                    for (var i = 0; i < arr.length; i += size) {
                        newArr.push(arr.slice(i, i + size));
                    }
                    return newArr;
                };
                $scope.initWeblink = function() {
                    $scope.webLinks = $scope.breakList($scope.webLinks, 2);
                    $scope.webLinksDiv1 = $scope.webLinks[0]
                    $scope.webLinksDiv2 = $scope.webLinks[1]
                    console.log($scope.webLinks[0])
                    console.log($scope.webLinks[1])
                };
                $scope.pervious = function() {
                    var playHtml1 = "";
                    $scope.videoUrlLinks.reverse();
                    $scope.videoUrlLinksNote.reverse();
                    for (var i = $scope.videoUrlLinks.length - 1; i >= 1; i--) {
                        console.log($scope.videoUrlLinks[i]);
                        playHtml1 += '<a ><div  ng-click=playVideo(videoUrlLinks[' + i + '],$scope.videoUrlLinksNote[' + i + ']);> <iframe src="' + $scope.videoUrlLinks[i] + '" width="40" height="30" frameborder="1" ></iframe><br>Note : ' + $scope.videoUrlLinksNote[i] + '</div></a>';

                    };
                    console.log(playHtml1);
                    var tempHtml1 = $compile(playHtml1)($scope);
                    var myelHtml1 = angular.element(document.querySelector('#space-for-previous-video'));
                    myelHtml1.append(tempHtml1)
                    console.log(myelHtml1.html(tempHtml1));
                };

            })

        }
    }
})