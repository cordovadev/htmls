angular.module('cordova_lms', ['angular-meteor', 'ui.router', 'ngMaterial','as.sortable','ui.calendar','ui.bootstrap', 'vAccordion', 'ngAnimate'])

angular.module('cordova_lms', ['angular-meteor', 'wsContentScroller', 'ui.router', 'ngMaterial','as.sortable','ui.calendar','ui.bootstrap','googlechart'])

  .config(function($mdThemingProvider) {
    /*This is the theme mainly used in platform admin. ,'googlechart' */

 

    var cordovaBlue = $mdThemingProvider.extendPalette('cyan', {
      /*Extending pallete for those colors not in default.*/
      '50': '#FFFFFF', //Header background color
      'A700': '#18a8a9',
      'A100': '#B2EBF2',
      '100': '#84ffff',
      '200': '#E7E8EA'
    });
    var backgroundBlue = $mdThemingProvider.extendPalette('cyan', {
      /*Extending pallete for those colors not in default.*/
      '50': '#EBEEF3' //Header background color
    });

    var lightPinkMap = $mdThemingProvider.extendPalette('pink', {
      '700': '#26A59A'
    });
    var darkRedMap = $mdThemingProvider.extendPalette('red', {
      '700': '#E35D5A'
    });
    $mdThemingProvider.definePalette('cordovaBlue', cordovaBlue);
    $mdThemingProvider.definePalette('lightPink', lightPinkMap);
    $mdThemingProvider.definePalette('backgroundBlue', backgroundBlue);
    $mdThemingProvider.definePalette('darkRed', darkRedMap);
    $mdThemingProvider.theme('default')
      .primaryPalette('cordovaBlue', {
        'hue-1': '50', // use shade 50 for the <code>md-hue-1</code> class
        'hue-2': 'A700', // use shade A700 for the <code>md-hue-2</code> class
        'hue-3': '200'
      })
      .accentPalette('lightPink', {
        'default': '700'
      })
      .warnPalette('darkRed', {
        'default': '700'
      })
      .backgroundPalette('backgroundBlue', {
        'default': "50"
      });

    $mdThemingProvider.theme('makeMeWhite')
      .backgroundPalette('cordovaBlue', {
        'default': '50' // use shade 50 for the <code>md-hue-1</code> class
      })
      .primaryPalette('cordovaBlue', {
        'hue-1': '200'
      })
  });
//URL of our asset
var assetURL = "https://raw.githubusercontent.com/cfjedimaster/Cordova-Examples/master/readme.md";
var fileName = "video.mp4";
//   if (Meteor.isCordova) {
//   console.log("Printed only in mobile Cordova apps");
  

//   // var ExternaStorge = cordova.file.externalApplicationStorageDirectory
//   // console.log("externalStorage"+JSON.stringify(ExternaStorge))

//   // dataVailable = window.resolveLocalFileSystemURL(ExternaStorge + fileName, appStart, downloadAsset);
//   // console.log("Video"+dataVailable)
//   // window.resolveLocalFileSystemURL(ExternaStorge, function(dir) {
//   //       console.log("got main dir"+JSON.stringify(dir));
//   //       dir.getFile("log.txt", {create:true}, function(file) {
//   //           console.log("got the file"+JSON.stringify(file));
//   //           logOb = file;
//   //           writeLog("App started"+logOb);          
//   //       });
//   // });
//   window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, fsSuccess, fail);
//    window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, onFileSystemSuccess, fail);
//         window.resolveLocalFileSystemURL("file:///example.txt", onResolveSuccess, fail);
 
//   function fsSuccess(fs){
//     console.log("CAME @ success")
//     if (fs.root.fullPath === 'file:///storage/sdcard0'){
//       fs.root.fullPath = 'file:///storage'; // change the path
//     }
//     // create directory reader
//     var directoryReader = fs.root.createReader()
//     // get a list of all entries in the directory
//     directoryReader.readEntries(dirSuccess,fail);
//   }
   
//   function dirSuccess(entries){
//     console.log("Entries"+entries);
//     // will print something like the following to the console
//     // [{"isFile":false,"isDirectory":true,"name":"extSdCard",
//     //    "fullPath":"file:///storage/extSdCard","filesystem":null},
//     //  {"isFile":false,"isDirectory":true,"name":"sdcard0",
//     //    "fullPath":"file:///storage/sdcard0","filesystem":null}
//     // ]
//   }
// }

// function downloadAsset() {
//     var fileTransfer = new FileTransfer();
//     console.log("About to start transfer");
//     fileTransfer.download(assetURL, ExternaStorge + fileName, 
//         function(entry) {
//             console.log("Success!");
//             appStart();
//         }, 
//         function(err) {
//             console.log("Error");
//             console.dir(err);
//         });
// }

// function writeLog(str) {
//     if(!logOb) return;
//     var log = str + " [" + (new Date()) + "]\n";
//     console.log("going to log "+log);
//     logOb.createWriter(function(fileWriter) {
        
//         fileWriter.seek(fileWriter.length);
        
//         var blob = new Blob([log], {type:'text/plain'});
//         fileWriter.write(blob);
//         console.log("ok, in theory i worked");
//     }, fail);
// }

// function justForTesting() {
//     logOb.file(function(file) {
//         var reader = new FileReader();

//         reader.onloadend = function(e) {
//             console.log(this.result);
//         };

//         reader.readAsText(file);
//     }, fail);

// }


//I'm only called when the file exists or has been downloaded.
// function appStart() {
//     $status.innerHTML = "App ready!";
// }

/**
validator functions
keyPath : array of invalid keys
value   : Error message
**/
function assignErrorValue(obj, keyPath, value) {
  lastKeyIndex = keyPath.length - 1;
  for (var i = 0; i < lastKeyIndex; ++i) {
    key = keyPath[i];
    if (!(key in obj))
      obj[key] = {}
    obj = obj[key];
  }
  obj[keyPath[lastKeyIndex]] = value;
}

angular.module('cordova_lms').factory('errorValidatorUtil', function() {

    var errorValidatorUtil = {};

    errorValidatorUtil.assignErrorValue = assignErrorValue;

    return errorValidatorUtil;

  })
  .factory('PageTitle', function() { // factory for set current title for each state
    let title ;
    return {
      
      setHeading: function(newTitle) {
        title = newTitle;
        
      },
       setTitle:function() {
       
        return title;
      }
    };
  })


angular.module('cordova_lms').factory('MyResource',function(){
    return {};
});

/*angular.module('cordova_lms').factory('MyQuestionID',function(){
    return {

    };
});*/

angular.module('cordova_lms').factory('chartFactory', function () {
     
     let graphdata;
    return {

        setgraphArray: function (graphdata) {
             this.graphdata = graphdata
        },
        getGraphArray: function () {
            return this.graphdata 
        },
//        graphdata : this.graphdata? this.graphdata : []
     }
});

angular.module('cordova_lms').factory('QuestionsFactory', function () {
     
  let questionID ;
  let dictation;
    return {

        addDictation:function (dictation) {
             this.dictation = dictation
        },
        getDictation:function()
        {
            return dictation
        },
        addQuestion:function (id) {
             this.questionID = id
        },
        getQuestion:function()
        {
            return questionID
        },

        addArray: function (question) {
             this.questions = question
        },
        getArray: function () {
            return this.questions 
        },
        questions : this.questions? this.questions : []
     }
});
