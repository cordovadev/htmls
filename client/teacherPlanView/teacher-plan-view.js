


angular.module('cordova_lms').directive('teacherPlanView', function() {
  return {
    restrict: 'E',
    templateUrl: 'client/teacherPlanView/teacher-plan-view.html',
    controllerAs: 'teachingPlanCtrl',

     controller: function ($scope, $reactive,$state,$filter,$mdToast, $timeout,$interval,$meteor,$interval,$mdDialog, $stateParams){
      $reactive(this).attach($scope);
      let self = this
      $scope.teachingPlan_id=$stateParams.getTeachingPlan;
         $scope.teachingPlan_=[];
      $scope.teachingPlanData=[]
       $scope.teachingPlanDataUpdate=[]
       $scope.statuscomplete=true;
       $scope.statusNotcomplete=false
       var checkedItems1 = [];
       self.subscribe('getTeachingPlans',
        () => {
          return [] // parameters to be passed to subscribe method
        },
        () => {
          $scope.teachingPlans = TeachingPlans.find({"status":"verified","teacherId": Meteor.userId(),"_id":$scope.teachingPlan_id}).fetch();
          console.log($scope.teachingPlans) 
          $scope.teachingPlan=$scope.teachingPlans[0]
          $scope.teachingPlanData=$scope.teachingPlans[0].teaching_plan
       
                  angular.forEach($scope.teachingPlanData, function(teachingPlan, arrayIndex){
        if(teachingPlan.status=="complete"){
          teachingPlan.status=true

        }else{
          teachingPlan.status=false

        }
          checkedItems1.push(teachingPlan)
        })
        $scope.teachingPlanData=checkedItems1;
      console.log($scope.teachingPlanData)
        }
      )

      
        $scope.backToTeachingPlanList=function(){

   $scope.teachingPlanDataUpdate=$scope.checkedItems ();
   console.log(   $scope.teachingPlanDataUpdate)

Meteor.call('updateTeachingPlan',$scope.teachingPlanDataUpdate,$scope.teachingPlan._id,(error, result) => {
                            if (error) {
                                console.log("Error:" + error);
                            } else {
                               $state.go('teachingPlanList');
                            }
                        })

    }
     $scope.checkedItems = function() {
        var checkedItems = [];
        angular.forEach($scope.teachingPlanData, function(teachingPlan){
        if(teachingPlan.status==true ){
          teachingPlan.status="complete"

        }else{
          teachingPlan.status="not complete"

        }
          checkedItems.push(teachingPlan)
        })
        console.log(checkedItems)
        return checkedItems
    }


    }
  }
})


