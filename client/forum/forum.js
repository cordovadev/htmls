angular.module('cordova_lms').directive('forum', function() {
    return {
        restrict: 'E',
        templateUrl: 'client/forum/forum.html',
        controllerAs: 'forumCtrl',
        controller: function($scope, $mdToast, $reactive, $window, $stateParams, $state) {
            $reactive(this).attach($scope);
            let self = this
            $scope.replyDiv = true
            $scope.replyMessages = {}
            $scope.messageData = {}
            $scope.messageDetails = []
            $scope.replyMessages.faqStatus = false
            $scope.messageData.faq = false
            $scope.messageData.public = false
            var role = ""
            $scope.userName = ""
                // $scope.replyArea  = true
                //  $scope.repliesButton = false
            $scope.replyArea = []
            $scope.repliesButton = []
            self.getUserAcces = function() {
                self.subscribe('usersData', () => {
                    return [] // parameters to be passed to subscribe method
                }, () => {
                    userRole = Meteor.users.find().fetch()
                    if (userRole.length > 0) {
                        role = userRole[0].role
                        userName = userRole[0].userName
                        var studentSubjects = Meteor.users.findOne({
                            _id: Meteor.userId()
                        }).endorse_session_curriculum
                        var classId = studentSubjects[0].class_id
                        var batch = studentSubjects[0].batch_id
                        var subjects = studentSubjects[0].subjects
                        Session.setAuth("role", role)
                        Session.setAuth("subjects", subjects)
                        Session.setAuth("classId", classId)
                        Session.setAuth("batchId", batch)
                        if (role == "teacher") {
                            self.getForumQuestionsTeacher(subjects, classId, batch)
                        } else if (role == "student") {
                            self.getForumQuestionsStudent(subjects, classId, batch)
                        }
                    }
                })
            }
            self.getUserAcces()
            self.getForumQuestionsStudent = function(subjects, classId, batch) {
                self.subscribe('getForumStudent', () => {
                    return [subjects, classId, batch] // parameter s to be passed to subscribe method
                }, () => {

                    // role = userRole[0].role
                    // if(role == "teacher"){
                    self.helpers({
                        getForum: () => {
                            return Forum.find({})
                        }
                    })
                    $scope.messageDetails = self.getForum

                    for (var i = 0; i < $scope.messageDetails.length; i++) {

                        $scope.replyArea[i] = true
                        $scope.repliesButton[i] = false

                        console.log($scope.replyArea[i])
                    }
                    //   $scope.messageDetails = self.getForum
                    // }
                    // else if(role == "student"){
                    //   self.helpers({
                    //     getForum: () => {
                    //       return Forum.find({})
                    //     }
                    //   })


                    // }
                    // self.getForumQuestions()         
                    // }
                })
            }
            self.getForumQuestionsTeacher = function(subject, classId, batch) {
                self.subscribe('getForumTeacher', () => {
                    return [subject, classId, batch] // parameter s to be passed to subscribe method
                }, () => {

                    // role = userRole[0].role
                    // if(role == "teacher"){
                    self.helpers({
                        getForum: () => {
                            return Forum.find({
                                class_id: classId,
                                batch_id: batch,
                                subject_id: {
                                    $in: subject
                                }
                            })
                        }
                    })
                    $scope.messageDetails = self.getForum
                    for (var i = 0; i < $scope.messageDetails.length; i++) {

                        $scope.replyArea[i] = true
                        $scope.repliesButton[i] = false

                        console.log($scope.replyArea[i])
                    }
                    // }
                    // else if(role == "student"){
                    //   self.helpers({
                    //     getForum: () => {
                    //       return Forum.find({})
                    //     }
                    //   })

                    //   $scope.messageDetails = self.getForum
                    // }
                    // self.getForumQuestions()         
                    // }
                })
            }
            $scope.addQuestion = function() {
                MsgObj = {
                    user_id: Meteor.userId(),
                    subject_id: Session.get("subjects")[0],
                    class_id: Session.get("classId"),
                    batch_id: Session.get("batchId"),
                    message: {
                        question: $scope.question,
                        description: $scope.description
                    },
                    faq: false,
                    public: false
                }
                Meteor.call('createQuestion', MsgObj, (error, result) => {
                    if (error) {

                        console.log(error)

                    } else {

                        console.log(result)

                    }
                })
                console.log("Came 2 add Question")
            }

            $scope.forwardFaq = function(forumMessage, faqStatus) {
                Meteor.call('forwardToFaq', forumMessage._id, faqStatus, (error, result) => {
                    if (error) {
                        console.log(error)
                    } else {
                        console.log(result)
                    }
                })
                console.log("Came 2 add Question")
            }
            $scope.assignPublic = function(forumMessage, publicStatus) {
                Meteor.call('showPublic', forumMessage._id, publicStatus, (error, result) => {
                    if (error) {
                        console.log(error)
                    } else {
                        console.log(result)
                    }
                })
                console.log("Came 2 add Question")
            }

            $scope.addToFaq = function(index, messageData, checkstatus) {
                // if(checkstatus = false){
                //   checkstatus = true
                // }
                // else{
                //   checkstatus = true 
                // }
                Meteor.call('faqCorrectAns', messageData._id, index, checkstatus, (error, result) => {
                    if (error) {
                        console.log(error)
                    } else {
                        console.log(result)
                    }
                })
            }

            $scope.showData = function(index) {
                console.log("sdfsd")
             return   $scope.replyArea[index] = true
            }

            $scope.addTextArea = function() {
                console.log("sdfsd")
                $scope.replyDiv = false
            }

            $scope.showReplies = function(index) {
                console.log("sdfsd")
                $scope.replyArea[index] = false
                $scope.repliesButton[index] = true
                    // $scope.
            }

            $scope.openMenu = function($mdOpenMenu, ev) {
                console.log("error.invalidKeys");
                originatorEv = ev;
                $mdOpenMenu(ev);
            };

            $scope.replyMessageButton = function(id, replyMessage, checkStatus,replyMessageData) {
                var rplyMessageObj = {
                    title: replyMessageData,
                    id: Meteor.userId(),
                    faqStatus: false
                }
                Meteor.call('updateDiscussion', id, rplyMessageObj, (error, result) => {
                    if (error) {

                        console.log(error)

                    } else {

                        console.log(result)

                    }
                })
            }
        }
    }
});
