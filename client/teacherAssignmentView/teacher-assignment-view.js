angular.module('cordova_lms').directive('teacherAssignmentView', function(MyResource) {
    return {
        restrict: 'E',
        templateUrl: 'client/teacherAssignmentView/teacher-assignment-view.html',
        controllerAs: 'studentAssignmentListCtrl',
        controller: function($scope, $reactive, $state, $filter, $timeout, $interval, $meteor, $interval, $stateParams) {
            $reactive(this).attach($scope);
            let self = this
            self.dynamicTheme = "default"

            self.user = {}
            let studentId = Meteor.userId();
            console.log($scope.questions)
            $scope.getAssginmentQuestion = $stateParams.getAssginmentQuestion;
            $scope.getAssginment_id = $stateParams.getAssginment;
            $scope.getAssginmentQuestion_id = $stateParams.question_id;
            $scope.getstudentGroupArray = [];
            $scope.getstudentDetails = [];

            // $scope.quesTypes= $scope.getAssginment_id;



            // console.log($scope.quesTypes.group[0]._id)

            $scope.getAllStudentGroup = function() {
                self.subscribe('getStudentGroup', () => {
                    return [] // parameters to be passed to subscribe method
                }, () => {
                    console.log(StudentGroup.find().fetch())
                    $scope.getstudentGroupArray = StudentGroup.find().fetch();
                    $scope.getAllStudent();

                })
            }
            $scope.getAllStudent = function() {
                self.subscribe('getStudent', () => {
                    return [] // parameters to be passed to subscribe method
                }, () => {
                    console.log(StudentDetails.find({
                        "teacherId": Meteor.userId()
                    }).fetch())
                    $scope.getstudentDetails = StudentDetails.find({
                        "teacherId": Meteor.userId()
                    }).fetch();
                    $scope.quesTypes = $stateParams.getAssginment;

                    console.log($scope.quesTypes)
                })
            }

            $scope.getstudentGroup = function(group_id) {
                console.log(group_id)
                var getstudentGroupArray1 = StudentGroup.find({
                    "_id": group_id
                }).fetch()
                console.log(getstudentGroupArray1[0].students)

                return getstudentGroupArray1[0].groupName
            }

            $scope.collapseAll = function(data, group_id) {
                $scope.getstudentDetails1 = [];
                var getstudentGroupArray1 = StudentGroup.find({
                    "_id": group_id
                }).fetch()
                $scope.getstudentDetails = getstudentGroupArray1[0].students
                angular.forEach($scope.getstudentDetails, function(value, key) {
                    console.log(StudentDetails.find({
                        "_id": value._id
                    }).fetch())
                    studentname = StudentDetails.find({
                        "_id": value._id
                    }).fetch()[0].username
                    studentUserId = StudentDetails.find({
                        "_id": value._id
                    }).fetch()[0]._id
                    studentObj = {
                        name: studentname,
                        _id: studentUserId
                    }
                    $scope.getstudentDetails1.push(studentObj);
                })
                $scope.studentArray = $scope.getstudentDetails1

                //    for(var i in  $scope.getAssginment_id) {
                //      if( $scope.getAssginment_id[i] != data) {
                //         $scope.getAssginment_id[i].expanded = false;   
                //      }
                //    }

                data.expanded = !data.expanded;
            };
            /* list assignment questions*/
            $scope.getAssginmentQuestions = function() {
                let questionsList = $scope.quesTypes[0].question;
                console.log($scope.quesTypes[0]._id)

                $state.go('teacherAssignmentView', {
                    "getAssginment": questionsList,
                    "assignment_id": assignment_answer,
                    "question_id": $scope.quesTypes[0]._id
                });

            }

            $scope.viewAssignment = function(student_id, assignment_id) {
                var assignmentName, studentAssignemntData;
                console.log(student_id + "\t" + assignment_id);

                angular.forEach($scope.quesTypes, function(value, key) {
                    angular.forEach(value.answer, function(answer, key) {
                        console.log(student_id + "\t" + answer.studentId);
                        if (student_id == answer.studentId && assignment_id == value._id) {
                            console.log(answer.studentId)
                            assignmentName = value.title;
                            assignment_id = value._id;
                            studentAssignemntData = answer;

                        }

                    })

                })
                console.log(assignmentName + "\t" + studentAssignemntData);
                $state.go('teacherAssignmentAnswerView', {
                    "getAssginment": assignmentName,
                    "getAssginment_id": assignment_id,
                    "assignment_answer": studentAssignemntData
                });

            }
        }

    }
})