/*This is the parent scope controller, where the developers need to define the datasets */
/* Here for the filter Dataset settings colection is used as an example*/
/*The basic structure of settings collection is like this 
{
  "_id": "123",
  "type": "subject",
  //subject package details
  "name": "Main",
  "list": [{
    "_id": "123",
    "name": "Maths"
  }, {
    "_id": "123",
    "name": " Physics"
  }]
}

{
  "_id": "123",
  "type": "exam",
  //exam package details
  "name": "Activities",
  "list": [{
    "_id": "123",
    "name": "Reading"
  }, {
    "_id": "123",
    "name": "Fluency"
  }]
}

*/
/*The actual structure is manipulated inorder to get a generic data structure */
/* sample fucntion - inside this function with some queries a new structure is developed*/
/*so settingsObj object is created like this 
{exam:[{"id":"123","name":"flunecy"},{"id":"123","name":"reading"}],
subject:[{"id":"123","name":"maths"},{"id":"123","name":"physics"}]} */
/* so the developers need to manipulate their collection to this format*/
/*once the new object with settings with settingsObj is created the developer should call the demo
function */
/*intialize filterObj object here */
/* dataSet here contains the data which is displayed in tile format */




angular.module('cordova_lms').directive('searchMain', function() {
    return {
        restrict: 'E',
        templateUrl: 'client/AssignUser/searchMain.html',
        controllerAs: 'searchMainCtrl',
        controller: function($scope, $reactive, $stateParams, $mdDialog, $state, $mdToast, $timeout, $q, $log, errorValidatorUtil) {
            $reactive(this).attach($scope)
            let self = this;
            let currentObject = [];
            $scope.values = []

            self.subscribe('getPerson', () => [], {
                onReady: function() {
                    self.helpers({
                        persons: () => {
                            return Person.find({});
                        }
                    })

                    self.sample();
                    this.autorun(() => {
                        self.demo = function() {
                            let trail = {}
                            _.each(self.settingsObj, function(value, key) {
                                $scope.values.push({
                                    key: key,
                                    value: value
                                });

                            })

                            console.log($scope.values)
                        }

                        self.demo();
                        $scope.dataSet = this.studentList

                        $scope.filterObj = {}
                    })

                }
            });

            




            self.sample = function() {
                self.settingsObj = {}

                _.each(self.persons, function(person) {


                    if (!self.settingsObj[person.type]) {
                        self.settingsObj[person.type] = []
                    }
                    self.settingsObj[person.type] = person
                })

                console.log(self.settingsObj)

            }


            this.studentList = [{
                "name": "Aysha",
                "username": "aysha@cordovacloud.com",
                "class": "1",
                "batch": "A",
                "session": "morning",
                "exam": "fulency",
                "subject": "Physics"

            }, {
                "name": "Arun",
                "username": "arun@cordovacloud.com",
                "class": "2",
                "batch": "B",
                "session": "morning",
                "exam": "fluency",
                "subject": "Physics"
            }, {
                "name": "Sunu",
                "username": "sunu@cordovacloud.com",
                "class": "3",
                "batch": "C",
                "session": "afternoon",
                "exam": "fluency",
                "subject": "Physics"
            }, {
                "name": "Naseer",
                "username": "naseer@cordovacloud.com",
                "class": "1",
                "batch": "A",
                "exam": "fluency",
                "curriculum": "ICSE"
            }, {
                "name": "Jishnu",
                "username": "jishnu@cordovacloud.com",
                "class": "2",
                "batch": "A ",
                "session": "afternoon",
                "exam": "fluency",
                "subject": "maths"
            }, {
                "name": "Varun",
                "username": "varun@cordovacloud.com",
                "class": "3",
                "batch": "B",
                "session": "afternoon",
                "exam": "reading",
                "subject": "maths"
            }, {
                "name": "Babin",
                "username": "babin@cordovacloud.com",
                "class": "3",
                "batch": "C",
                "session": "morning",
                "exam": "reading",
                "subject": "maths"

            }, {
                "name": "Kiran",
                "username": "kiran@cordovacloud.com",
                "class": "1",
                "batch": "B",
                "session": "afternoon",
                "exam": "reading",
                "subject": "maths"
            }]




            $scope.allItemsSelected = false;
            let names = null;
            $scope.selectAll = function(selected) {
                for (var i = 0; i < $scope.dataSet.length; i++) {
                    $scope.dataSet[i].isChecked = selected;
                }
            };

            $scope.notifyMaster = function() {
                $scope.selectedAll = !$scope.dataSet.some(function(names) {
                    return !names.Selected; //check if ANY are unchecked
                });
            }
            $scope.changeCheckAll = function() {
                for (var i = 0; i < $scope.dataSet.length; i++) {
                    // If there is an unchecked input
                    // Change the status and exit the function
                    if (!$scope.dataSet[i].selected) {
                        $scope.allItemsSelected = false;
                        return false;
                    }
                }
                // If all are checked
                $scope.allItemsSelected = true;
            }

        }
    }

})
