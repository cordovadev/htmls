angular.module('cordova_lms').directive('timerDirective', function () {
  return {
    restrict: 'E',
    templateUrl: 'client/Timer/timer.html',
    controllerAs: 'stopWatchDemoCtrl',
    controller :  function($scope,StopwatchFactory,$interval){
    $scope.parentObj = {}
    console.log(StopwatchFactory)
    $scope.stopwatches = [{ log: []}];
    $scope.getTime = function(){

      // alert(JSON.stringify($scope.stopwatches[0].log[0]))
      alert($scope.stopwatches)
    }

    // StopwatchFactory(data).success(function() {
    // //update controller here
    // })
    /*Test Code*/

            // var startTime = 0,
            //     currentTime = null,
            //     offset = 0,
            //     interval = null,
            //     self = this;
            //     options ={}

            // if(!options.interval){
            //     options.interval = 100;
            // }

            // options.elapsedTime = new Date(0);

            // self.running = false;
              
            // self.updateTime = function(){
            //     currentTime = new Date().getTime();
            //     var timeElapsed = offset + (currentTime - startTime);
            //     options.elapsedTime.setTime(timeElapsed);
            // };



            // self.startTimer = function(){
            //     if(self.running === false){
            //         startTime = new Date().getTime();
            //         interval = $interval(self.updateTime,options.interval);
            //         self.running = true;
            //     }
            // };

            // self.stopTimer = function(){
            //     if( self.running === false) {
            //         return;
            //     }
            //     self.updateTime();
            //     offset = offset + currentTime - startTime;
            //     // pushToLog(currentTime - startTime);
            //     // $interval.cancel(interval);  
            //     self.running = false;
            //     self.timeData = currentTime - startTime
            //     console.log(self.timeData)
            // };
    }
   }
})
    angular.module("cordova_lms").filter('stopwatchTime', function () {
      return function (input) {
          if(input){
              
              var elapsed = input.getTime();
              var hours = parseInt(elapsed / 3600000,10);
              elapsed %= 3600000;
              var mins = parseInt(elapsed / 60000,10);
              elapsed %= 60000;
              var secs = parseInt(elapsed / 1000,10);
              var ms = elapsed % 1000;
              
              return hours + ':' + mins + ':' + secs + ':' + ms;
          }
      };
    })
    angular.module("cordova_lms").directive('bbStopwatch', ['StopwatchFactory', function(StopwatchFactory){
      return {
          restrict: 'EA',
          scope: true,
          link: function(scope, elem, attrs){   
              
              scope.stopwatchService = new StopwatchFactory(scope[attrs.options]);
              scope.startTimer = scope.stopwatchService.startTimer; 
              scope.stopTimer = scope.stopwatchService.stopTimer;
              
          }
      };
    }])
    angular.module("cordova_lms").factory('StopwatchFactory', ['$interval',    function($interval){
        return function(options){

            var startTime = 0,
                currentTime = null,
                offset = 0,
                interval = null,
                self = this;
                options = {}
            if(!options.interval){
                options.interval = 100;
            }

            options.elapsedTime = new Date(0);

            self.running = false;
            
            function pushToLog(lap){
                if(options.log !== undefined){
                   options.log.push(lap); 
                }
            }
             
            self.updateTime = function(){
                currentTime = new Date().getTime();
                var timeElapsed = offset + (currentTime - startTime);
                options.elapsedTime.setTime(timeElapsed);
            };

            self.startTimer = function(){
                if(self.running === false){
                    startTime = new Date().getTime();
                    interval = $interval(self.updateTime,options.interval);
                    self.running = true;
                }
            };

            self.stopTimer = function(){
                if( self.running === false) {
                    return;
                }
                self.updateTime();
                offset = offset + currentTime - startTime;
                pushToLog(currentTime - startTime);
                $interval.cancel(interval);  
                self.running = false;
                self.timeData = currentTime - startTime
            };

            self.resetTimer = function(){
              startTime = new Date().getTime();
              options.elapsedTime.setTime(0);
              timeElapsed = offset = 0;
            };

            self.cancelTimer = function(){
              $interval.cancel(interval);
            };

            return self;

        };
    }]);
  
