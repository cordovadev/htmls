angular.module('cordova_lms').directive('listSurvey', function() {
    return {
        restrict: 'E',
        templateUrl: 'client/survey/surveyList.html',
        controllerAs: 'surveyList',
        controller: function($scope, $mdToast, $reactive, $window, $stateParams, $state,$filter) {
            $reactive(this).attach($scope);
            let self = this
            console.log("came 2 survey")

            self.getSurveyList = function(centerArr) {
                self.subscribe('getSurveyList',
                      () => {
                        return [] // parameters to be passed to subscribe method
                      },
                      () => {
                              self.helpers({
                                getSurvey: () => {
                                  return Survey.find()
                                }
                              })
                              $scope.getSurvey = self.getSurvey
                            }
                )
            }
            self.getSurveyList()

            $scope.getDateInFormat = function(date) {
                dateFromat = $filter('date')(date, "dd-MM-yyyy")
                console.log(dateFromat)
                return dateFromat
            }
        }
    }
});