angular.module('cordova_lms').directive('createSurvey', function() {
    return {
        restrict: 'E',
        templateUrl: 'client/survey/createSurvey.html',
        controllerAs: 'faqCtrl',
        controller: function($scope, $mdToast, $reactive, $window, $stateParams, $state) {
            $reactive(this).attach($scope);
            let self = this
            console.log("came 2 survey")
            $scope.options = ["Multiple Choice", "Paragraph", "Check Boxes"]
            $scope.choices = []
            $scope.questionPreview = []
            $scope.question = { }
            $scope.checkboxes = []
            $scope.addOptions = true
            $scope.questions = [
                                  {
                                    title:"",
                                    options : ["Multiple Choice", "Paragraph", "Check Boxes"],
                                  }
                                ]
            $scope.questions[0].selectedOptions = "Multiple Choice"
            let today = new Date()
            let currentDay = new Date(today.getFullYear(),today.getMonth(),today.getDate())
            $scope.getOptions = function(index,selectedOptions) {
                console.log(selectedOptions)
                if (selectedOptions == "Multiple Choice") {
                    $scope.choices = [{
                            type: "radioButton"
                        }, {
                            type: "radioButton"
                        },
                        {
                            type: "radioButton"
                        },
                         {
                            type: "radioButton"
                        }


                    ]
                $scope.questions[index].choices = $scope.choices
                    $scope.addOptions = false
                }
                if (selectedOptions == "Check Boxes") {
                    $scope.choices = [{
                            type: "checkBoxes"
                        }, {
                            type: "checkBoxes"
                        },
                        {
                            type: "checkBoxes"
                        },
                        {
                            type: "checkBoxes"
                        }
                    ]
                    $scope.questions[index].choices = $scope.choices
                    $scope.addOptions = false
                } else if (selectedOptions == "Paragraph") {
                    $scope.choices = [{
                        type: "paragraph",
                        id: "54543"

                    }]
                    $scope.questions[index].choices = $scope.choices
                    $scope.addOptions = true
                }
            }
            $scope.saveQuestions = function() {
                console.log($scope.questionPreview)
                var previewQuestion = []
                angular.copy($scope.questionPreview,previewQuestion);
                var surveyQuestion = {
                    title:$scope.surveyTitle,
                    due_date:"123",
                    status : "Not Conducted",
                    created_user_id : Meteor.userId()
                }
                Meteor.call('addSurveyList', surveyQuestion,currentDay,$scope.questionPreview, function (error, result) {
                  if (error) {
                      console.log(error)
                  }
                  else {
                        Meteor.call('addSurveyQuestions', result,previewQuestion, function (error, result) {
                          if (error) {
                              console.log(error)
                          }
                          else {
                                console.log(result)
                                _.each($scope.questionPreview[0].choices,function(choices,value){
                                    console.log(choices)
                                })
                          }
                        })
                    }
                })
            }

            $scope.resetQuestions = function(){
                $scope.questionPreview = []
            }

            $scope.addOption = function(choices,index) {
                var choiceType = $scope.choices[0].type
                if (choiceType == "radioButton") {
                    typeObj = {
                        type: "radioButton"
                    }
                    // $scope.choices.push(typeObj)
                     $scope.questions[index].choices.push(typeObj)
                     console.log($scope.questions[index].choices)
                } else if (choiceType == "checkBoxes") {
                    typeObj = {
                        type: "checkBoxes"
                    }
                    $scope.choices.push(typeObj)
                }
            }

            $scope.addGridQuestion =  function(){
              $scope.questionArr = []
              angular.copy($scope.questions, $scope.questionArr);
              lastQuestion = $scope.questionArr.length - 1
              // if($scope.questions.length > 0){}
              var questionGrid = $scope.questionArr[lastQuestion]
              // if()
              $scope.questions.push($scope.questionArr[lastQuestion]) 
              // var currentChoice = $scope.choices[0]
              $scope.questionPreview.push($scope.questionArr[lastQuestion])
              $scope.questions.splice($scope.questionArr.length, 1);
            }

            $scope.EditQuestion =  function(question,index){
                $scope.questions.push(question) 
                 $scope.questionPreview.splice(index,1)
            }
            $scope.saveQuestion =  function(question,index){

                $scope.questionArr = []
                angular.copy($scope.questions, $scope.questionArr);
                lastQuestion = $scope.questionArr.length - 2
                $scope.questionPreview.push(question)
                $scope.questions.splice(lastQuestion, 1);
            }
            $scope.removeChoice = function(index){
             var choiceType = $scope.choices[0].type
                if (choiceType == "radioButton") {
                    $scope.choices.splice(index, 1);
                } else if (choiceType == "checkBoxes") {
                    $scope.choices.splice(index, 1);
                }
            }
            $scope.openMenu = function($mdOpenMenu, ev) {
                console.log("error.invalidKeys");
                originatorEv = ev;
                $mdOpenMenu(ev);
            };

        }
    }
});