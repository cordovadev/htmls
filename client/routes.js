// S is Student
// T is Teacher
var viewAuthObj = {
  dashboard : ['S','T'],
  practiceTest : ['S'],
  ibtComponent : ['S'],
  createQuiz : ['T'],
  createGroup : ['T'],
  groupList : ['T'],
  questionList : ['T'],
  chatComponent  : ['S','T'],
  graph : ['S','T'],
  pdfViewer : ['S'],
  newAssignment : ['S'],
  googleDrive: ['T']
}
angular.module('cordova_lms').config(function($urlRouterProvider, $stateProvider, $locationProvider) {

  $locationProvider.html5Mode(true)

  //resolveObj
    resolveObj = {
      authCheck: ['$q', '$rootScope', function($q, $rootScope) {
        var deferred = $q.defer();
        var rndm = Math.floor(Math.random() * 90 + 10)
          //console.log(rndm + ' :: **************************')
          //console.log(rndm + ' :: Entering resolveObj with redirectCheck :: ' + $rootScope.skipRedirectCheck)
        if (Meteor.user() == null) {
          Meteor.autorun(function() {
            //console.log(rndm + ' :: Entering autorun')
            if (!Meteor.loggingIn()) {
              if (Meteor.user() == null) {
                deferred.reject('REDIRECT_TO_LOGIN');
                //console.log(rndm + ' :: resolveObj :: REDIRECT_TO_LOGIN')
              } else {
                if ($rootScope.skipRedirectCheck) {
                  deferred.resolve('AUTHORIZED')
                    //console.log(rndm + ' :: ResolveObj :: AUTHORIZED - ASYNC')
                } else {
                  deferred.reject('AUTHORIZE_CHECK');
                  //console.log(rndm + ' :: ResolveObj :: AUTHORIZE_CHECK - ASYNC')
                }
              }
            }
          })
        } else {
          if ($rootScope.skipRedirectCheck) {
            deferred.resolve('AUTHORIZED')
              console.log(rndm + ' :: ResolveObj :: AUTHORIZED - SYNC')
          } else {
            deferred.reject('AUTHORIZE_CHECK');
            console.log(rndm + ' :: ResolveObj :: AUTHORIZE_CHECK - SYNC')
          }
        }
        //console.log(rndm + ' :: Exiting resolveObj' )
        return deferred.promise;
      }]
    }
    resolveObjLogin = {
      authCheck: ['$q','$state', ($q,$state) => {
        var deferred = $q.defer();
        Meteor.autorun(function() {
          if (!Meteor.loggingIn()) {
            if (Meteor.user() == null) {
              deferred.resolve('NON_LOGGED_IN')
                //console.log('resolveObj :: NON_LOGGED_IN')
            } else {
              if (Meteor.user().role != undefined) {
                deferred.reject('REDIRECT_TO_DASHBOARD')
              }else{
                console.log('session logout')
                /*Accounts.logout(function(error) {
                    if (error) {
                        console.log('error')
                    } else {
                        $state.go('login')
                    }
                })*/
                /*logout: ['$meteor', '$state', ($meteor, $state) => {
                  return $meteor.logout().then(function() {
                    $state.go('login')
                  }, function(err) {
                    //console.log('logout error - ', err)
                  })
                }]*/
              }
              //console.log('resolveObj :: REDIRECT_TO_DASHBOARD')
            }
          }
        });
        return deferred.promise;
      }]
    }

    resolveObjLogout = {
        logout: ['$meteor', '$state', ($meteor, $state) => {
          return $meteor.logout().then(function() {
            $state.go('login')
          }, function(err) {
            //console.log('logout error - ', err)
          })
        }]
      }
      //resolveObj end

  $stateProvider    
  
  // .state('logins', {
  //   url: '/login',
  //   template: '<registration-student/>'

  // })

  .state('dashboard', {
    url: '/dashboard',
     template: '<dashboard layout="column" flex></dashboard>',
     resolve: resolveObj
  })

   .state('courseware', {
    url: '/courseware',
    template: '<courseware-directive layout="column" flex></courseware-directive>',
     resolve: resolveObj
  })


  .state('ibtPage', {
    url: '/ibt',
    template: '<ibt-directive layout="column" flex></ibt-directive>',
     resolve: resolveObj
    })

   .state('viewDictionstatus', {
    url: '/viewDictionstatus',
    template: '<view-dictionstatus layout="column" flex></view-dictionstatus>'

  })

  .state('performance', {
    url: '/performance',
    template: '<performance-directive layout="column" flex></performance-directive>',
     resolve: resolveObj
  })


 .state('viewDictation', {
    url: '/viewDictation',
    template: '<view-dictation layout="column" flex></view-dictation>'
  })

  .state('practiceTest', {
    url: '/practiceTest',
    template: '<practice-test layout="column" flex></practice-test>',
    params: {'isPracticeTest':true},
     resolve: resolveObj

  })

  .state('questionDirective', {
    url: '/questionDirective',
    template: '<question-directive layout="column" flex></question-directive>'

  })

  .state('timerDirective', {
    url: '/timerDirective',
    template: '<timer-directive layout="column" flex></timer-directive>'
  })
  .state('createGroup', {
    url: '/createGroup/:id',
    template: '<create-group layout="column" flex></create-group>',
     resolve: resolveObj

  })
  .state('groupList', {
    url: '/groupList/:id',
    template: '<group-list layout="column" flex></group-list>',
     resolve: resolveObj

  })

  .state('questionList', {
    url: '/questionList',
    template: '<question-list layout="column" flex></question-list>',
     resolve: resolveObj

  })

  .state('createQuiz', {
    url: '/createQuiz',
    template: '<quiz-creation layout="column" flex></quiz-creation>',
     resolve: resolveObj
  })


  .state('previewQuiz', {
    url: '/previewQuiz',
    template: '<preview-quiz layout="column" flex></preview-quiz>'
  })

  .state('login', {
    url: '/login',
    template: '<login layout="column" flex></login>',
     resolve: resolveObjLogin
  })

  .state('register', {
    url: '/register',
    template: '<register layout="column" flex></register>'
  })
  .state('dictation', {
    url: '/dictation',
    template: '<sample-dictation layout="column" flex></sample-dictation>'
  })
  .state('teacherDictation', {
    url: '/teacherDictation',
    template: '<teacher-dictation layout="column" flex></teacher-dictation>'
  })
  // .state('calendar', {
  //   url: '/calendar',
  //   template: '<calendar layout="column" flex></calendar>'
  // })

  .state('calendar', {
    url: '/calendar',
    templateUrl: 'client/calendar/calendar.html',
    controller: 'AddCenterCtrl',
    controllerAs: 'AddCenterCtrl'
  })

  .state('forgotPassword', {
    url: '/forgotPassword',
    template: '<forgot-password layout="column" flex></forgot-password>'
  })

   .state('graph', {
    url: '/graph',
    template: '<student-graph layout="column" flex></student-graph>',
    resolve: resolveObj
  })

  .state('enrollAccount', {
        url: '/cordova/enroll-account/:token',
        template: '<verify-user layout="row" layout-align="center center"></verify-user>'
      })
  .state('additionalResources', {
        url: '/additionalResources',
    template: '<additional-resources layout="column" flex></additional-resources>'
      })


.state('pdfViewer', {
    url: '/pdfViewer',
    template: '<pdf-viewer layout="column" flex></pdf-viewer>',
     resolve: resolveObj

  })

.state('forum', {
    url: '/forum',
    template: '<forum layout="column" flex></forum>'
})
.state('chatComponent', {
    url: '/chatComponent',
    template: '<chat-component layout="column" flex></chat-component>',
     resolve: resolveObj
})
.state('faq', {
    url: '/faq',
    template: '<faq-component layout="column" flex></faq-component>'
})

.state('ibtComponent', {
    url: '/ibtComponent',
    template: '<ibt-test layout="column" flex></ibt-test>',
     resolve: resolveObj
})
.state('ibtQuestion', {
    url: '/ibtQuestion',
    template: '<ibt-question layout="column" flex></ibt-question>'

})

 .state('asigment', {
        url: '/asigment',
template: '<create-asignment layout="column" flex></create-asignment>'
      })
.state('newAssignment', {
        url: '/newAssignment',
    template: '<new-assignment layout="column" flex></new-assignment >'
      })
  .state('assignmentGroup', {
    url: '/assignmentGroup/:id',
    template: '<assignment-group layout="column" flex></assignment-group>'

  })
  .state('createAssignmentGroup', {
    url: '/createAssignmentGroup/:id',
    template: '<create-assignment-group layout="column" flex></create-assignment-group>'

  })
  .state('studentAssignmentList', {
    url: '/studentAssignmentList',
    template: '<student-assignment-list layout="column" flex></student-assignment-list>'

  })
  .state('studentAssignmentView', {
    url: '/studentAssignmentView',
    template: '<student-assignment-view layout="column" flex></student-assignment-view>',
     params: {'getAssginmentQuestion':true,'assignment_id':true,'question_id':true}
  })
  .state('teacherAssignmentView', {
    url: '/teacherAssignmentView',
    template: '<teacher-assignment-view layout="column" flex></teacher-assignment-view>',
     params: {'getAssginment':true,'assignment_answer':true}
  })
  .state('teacherAssignmentAnswerView', {
    url: '/teacherAssignmentAnswerView',
    template: '<teacher-assignment-answer-view layout="column" flex></teacher-assignment-answer-view>',
     params: {'getAssginment':true,'assignment_answer':true,'getAssginment_id':true}
  })
  .state('createSurvey', {
    url: '/createSurvey',
    template: '<create-survey layout="column" flex></create-survey>'
  })
  .state('listSurvey', {
    url: '/listSurvey',
    template: '<list-survey layout="column" flex></list-survey>'
  })

  .state('myPlanerTeachingPlan', {
    url: '/myPlanerTeachingPlan',
   template: '<my-planer-teaching-plan layout="column" flex></my-planer-teaching-plan>'
      
  })
  .state('viewTeachingPlan', {
    url: '/viewTeachingPlan',
   template: '<view-teaching-plan layout="column" flex></view-teaching-plan>',
     params: {'getTeachingPlan':true}
      
  })
   .state('createTeachingPlan', {
    url: '/createTeachingPlan',
   template: '<create-teaching-plan layout="column" flex></create-teaching-plan>'
      
  })
   .state('teachingPlanList', {
    url: '/teachingPlanList',
   template: '<teaching-plan-list layout="column" flex></teaching-plan-list>'
      
  })
   .state('teacherPlanView', {
    url: '/teacherPlanView',
   template: '<teacher-plan-view layout="column" flex></teacher-plan-view>',
     params: {'getTeachingPlan':true}
      
  })
    .state('googleDrive', {
    url: '/googleDrive',
    template: '<google-drive layout="column" flex></google-drive>',
     resolve: resolveObj
  })
   .state('searchFilter', {
                url: '/search-filter',
                template: '<search-main></search-main>'
            })

     $urlRouterProvider.otherwise("login");
  })
.run(["$rootScope", "$state", function($rootScope, $state) {
    $rootScope.skipRedirectCheck = false // initialize
    $rootScope.$on('$stateChangeError', function(ev, toState, toParams, fromState, fromParams, error) {
      var rndm = Math.floor(Math.random() * 90 + 10)
        // since this is a deferred promise after user has been resolved we can do a user role check here
        //console.log(rndm + ' :: Entering statechangeerror')
        //console.log(rndm + ' :: state :: "' + fromState.name + '" ::  to state :: "' + toState.name + '"')
        //console.log(rndm + ' :: redirectFlag :: ' + $rootScope.skipRedirectCheck )
      if (Meteor.user() != null) {
        if (error == "AUTHORIZE_CHECK") {
          //console.log (rndm + ' :: error :: AUTHORIZE_CHECK')

          switch (Meteor.user().role) {
            case "student":
              viewRole = 'S'
              break;
            case "teacher":
              viewRole = 'T'
              break;
            default:
              viewRole = 'T' // general for logged in users
          }

          if (viewAuthObj[toState.name] != null &&
            viewAuthObj[toState.name].indexOf(viewRole) != -1) {
            $rootScope.skipRedirectCheck = true // set flag first and then go to new state
              console.log(rndm + ' :: GOING TO NEW state :: "' + toState.name + '"')
            $state.go(toState.name, toParams)
          } else {
            error = 'REDIRECT_TO_DASHBOARD'
              //console.log (rndm + ' :: REDIRECT_TO_DASHBOARD since "' + toState.name + '" is not there in viewObj')
          }

        }
        if (error == 'REDIRECT_TO_DASHBOARD') {
          //console.log (rndm + ' :: error :: REDIRECT_TO_DASHBOARD "')
          $rootScope.skipRedirectCheck = true // set flag first and then go to new state
          switch (Meteor.user().role) {
            
            case "student":
              //console.log(rndm + ' :: REDIRECT_TO_DASHBOARD >>  directors') 
              $state.go('dashboard')
              break;
            case "teacher":
              //console.log(rndm + ' :: REDIRECT_TO_DASHBOARD >>  directors') 
              $state.go('dashboard')
              break;
            default:
              console.log(Meteor.user().role)
              console.log(rndm + ' :: REDIRECT_TO_DASHBOARD >>  welcome') 
              $state.go('login')
          }
        }
      } else {
        if (error == 'REDIRECT_TO_LOGIN') {
          $state.go('login')
        }
      }
      // once the $state.go is completed, then the below flags reset
      // see the rndm or thread number references to get idea
      $rootScope.skipRedirectCheck = false // reset flag here
        //console.log(rndm + ' :: Exiting statechangeerror')

    })
  }])