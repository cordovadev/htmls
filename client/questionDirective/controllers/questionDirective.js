angular.module('cordova_lms').directive('questionDirective', function () {
  return {
    restrict: 'E',
    templateUrl: 'client/questionDirective/views/questionDirective.html',
    controllerAs: 'directiveCtrl',
    controller: function ($scope, $reactive) {
      // controller code begins
      $reactive(this).attach($scope)
      let self = this

      self.countries = [];
      // self.subscribe('getQuestionBank',
      //   () => {
      //     return [] // parameters to be passed to subscribe method
      //   },
      //   () => {
      //     $scope.questions = QuestionBank.find().fetch();
      //   }
      // )
  
    
  $scope.dragControlListeners = {
          accept: function (sourceItemHandleScope, destSortableScope) {
            return sourceItemHandleScope.itemScope.sortableScope.$id === destSortableScope.$id;
          },
          itemMoved: function (event) {
            event.source.itemScope.modelValue.status = event.dest.sortableScope.$parent.column.name;
          },
          orderChanged: function(event) {
            console.log('orderChanged : ', event.source.index, 'to', event.dest.index);
          },
          dragStart: function () {
            $scope.isOptionSelected = true;
            //console.clear();
            //console.log('dragStart : ', arguments)
          },
          dragEnd: function () {
              //console.log('dragEnd : ', arguments)
          },
          containment: '#board',//optional param.
          clone: false, //optional param for clone feature.
          allowDuplicates: false //optional param allows duplicates to be dropped.
      };
    var i;
    $scope.itemsList = {
        items1: [],
        items2: []
    };

    for (i = 0; i <= 5; i += 1) {
        $scope.itemsList.items1.push({'Id': i, 'Label': 'Item A_' + i});
    }

    for (i = 0; i <= 5; i += 1) {
        $scope.itemsList.items2.push({'Id': i, 'Label': 'Item B_' + i});
    }

    $scope.sortableCloneOptions1 = {
        containment: '#sortable-container',
        allowDuplicates: true,
        ctrlClone: true
    };
    
    $scope.sortableCloneOptions2 = {
        containment: '#sortable-container',
        allowDuplicates: true,
        ctrlClone: true
    };
    
      // controller code ends
    }
  }
});